#include <TimeLib.h>
#include <config.h>
#include <x_data.h>
#include <stdint.h>
#include <MsgPack.h>
#include <DS3232RTC.h>

x_data xd;

#include <station_x.h>

/**
 * Initialises the RTC on first power-up with compile time. This assumes
 * the RTC has not been programmed previously (e.g. power loss)
 * it also  
 * initialises the alarms to known values, clears the alarm flags, clears the alarm interrupt flags
 * and configures the INT/SQW pin for "interrupt" operation (disable square wave output)
*/ 
void displayTime(time_t t)
{
    Serial.print(year(t));
    Serial.print("-");
    Serial.print(month(t));
    Serial.print("-");
    Serial.println(day(t));
    if (hour(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(hour(t));
    Serial.print(":");
    if (minute(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(minute(t));
    Serial.print(":");
    if (second(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.println(second(t));
}

void display(x_config* sx);

void setup() {
  Serial.begin(9600);
    // Start the serial port connected to the satellite modem
  setupIridium();
  // put your setup code here, to run once:
  config xs;
  x_config sx(xs);
  xs.f_version = 3;
  xs.gps = 1;
  xs.iridium  = 3; 
  xs.data_count = 17; 
  xs.en_ptu =1;
  xs.en_sd = true;
  xs.en_gps= true; 
  xs.en_depth = true; 
  xs.en_wind = false;
  xs.en_iridium = true;
  xs.en_magno = false;
  xs.reset = false;
  
  x_config sy(xs);
  sy.updateChecksum();
  display(&sy);

sy.packConfig();
  // serialize class directly
  MsgPack::Packer packer;
  packer.serialize(sy);
  for(unsigned int i = 0 ; i < packer.size() ; i++) {
    Serial.print(packer.data()[i], HEX);
  }
  Serial.println();

   x_config sz(0) ;
  //sendIridium(sy, sz);
  // convert object to class directly
  
  MsgPack::Unpacker unpacker;
  unpacker.feed(packer.data(), packer.size());
  unpacker.deserialize(sz);

  sz.unpackConfig();
  display(&sz);
  time_t t;
  Serial.print(getIridiumTime(t), DEC);
 
}
void loop() {
}
void display(x_config* sx) {
  config xs = (sx->getConfig());

  Serial.println(sx->station_id  ,HEX);
  Serial.println(xs.f_version    ,DEC);
  Serial.println(xs.gps          ,DEC);
  Serial.println(xs.iridium      ,DEC);
  Serial.println(xs.data_count   ,DEC);
  Serial.println(xs.en_ptu       ,DEC);
  Serial.println(xs.en_sd        ,DEC);
  Serial.println(xs.en_gps       ,DEC);
  Serial.println(xs.en_depth     ,DEC);
  Serial.println(xs.en_wind      ,DEC);
  Serial.println(xs.en_iridium   ,DEC);
  Serial.println(xs.en_magno     ,DEC);
  Serial.println(xs.reset        ,DEC);
  Serial.println(sx->checksum    ,HEX);
  Serial.println(sx->verifyChecksum());     
          
}
