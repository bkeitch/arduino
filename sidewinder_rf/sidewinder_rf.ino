/**
 * SideWinder RF test board
 * 
 * Use Arduino to control the I2C to parallel chip, MCP23008
 * 
 * Connections:
 * 
 * A4 (SDA)
 * A5 (SCL)
 * A3 Analogue control of VCO
 * GND
 * 
 */
#include <Wire.h>

#define MCP23008_ADDRESS 0x24 //!< MCP23008 serial address

// registers
#define MCP23008_IODIR 0x00   //!< I/O direction register
#define MCP23008_IPOL 0x01    //!< Input polarity register
#define MCP23008_GPINTEN 0x02 //!< Interrupt-on-change control register
#define MCP23008_DEFVAL                                                        \
  0x03 //!< Default compare register for interrupt-on-change
#define MCP23008_INTCON 0x04 //!< Interrupt control register
#define MCP23008_IOCON 0x05  //!< Configuration register
#define MCP23008_GPPU 0x06   //!< Pull-up resistor configuration register
#define MCP23008_INTF 0x07   //!< Interrupt flag register
#define MCP23008_INTCAP 0x08 //!< Interrupt capture register
#define MCP23008_GPIO 0x09   //!< Port register
#define MCP23008_OLAT 0x0A   //!< Output latch register
  
void setup() {  
  Serial.begin(9600);
  Wire.begin();
  pinMode(3, OUTPUT);  // sets the pin as output 
  Wire.beginTransmission(MCP23008_ADDRESS);
  Wire.write(MCP23008_IODIR); 
  Wire.write(byte(0));        // all outputs
  Wire.endTransmission();     // stop transmitting
  
  Wire.beginTransmission(MCP23008_ADDRESS);
  Wire.write(MCP23008_GPPU);
  Wire.write(byte(0xFF));     // all pullups
  Wire.endTransmission();     // stop transmitting
     
}

/*
Table 6. D5 to D0 Truth Table
Digital Control Input1 Attenuation
D5 D4 D3 D2 D1 D0 State (dB)
High High High High High High 0 (reference)
High High High High High Low  0.5
High High High High Low  High 1.0
High High High Low  High High 2.0
High High Low  High High High 4.0
High Low  High High High High 8.0
Low  High High High High High 16.0
Low  Low  Low  Low  Low  Low  31.5
*/
 byte attn ;
/*
Latched Parallel Mode
The LE pin must be kept low when changing the control voltage
inputs (D0 to D5) to set the attenuation state. When the desired
state is set, LE must be toggled high to transfer the 6-bit data to
the bypass switches of the attenuator array, and then toggled
low to latch the change into the device until the next desired
attenuation change (see Figure 32 and Table 2)
*/
void setAttn(byte att) {
  
  Wire.beginTransmission(MCP23008_ADDRESS); // transmit to device #44 (0x2c)
  Wire.write(MCP23008_GPIO);            // sends instruction byte
  Wire.write(byte(0b00000000)|byte(att));             // LE off
  Wire.endTransmission();  

  Wire.beginTransmission(MCP23008_ADDRESS); // transmit to device #44 (0x2c)
  Wire.write(MCP23008_GPIO);            // sends instruction byte
  Wire.write(byte(att));             // set attn
  Wire.endTransmission(); 
   
  Wire.beginTransmission(MCP23008_ADDRESS); // transmit to device #44 (0x2c)
  Wire.write(MCP23008_GPIO);            // sends instruction byte
  Wire.write(byte(0b10000000)|byte(att) );             // LE on
  Wire.endTransmission(); 
  attn = 0;
}
void loop() {
  while (Serial.available() > 0) {
    int attn = Serial.parseInt();
    int freq = Serial.parseInt();
    if (Serial.read() == '\n') {
      attn = constrain(attn, 0, 64);      
      setAttn(attn);  
      freq = constrain(freq, 0, 255);      
      analogWrite(3, freq);    
      Serial.println("Attenuation (0-63) \t Frequency (0-255) ");
      Serial.print(attn);
      Serial.print("\t \t \t");
      Serial.println(freq);
    }
  }
  
}
