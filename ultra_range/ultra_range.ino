/*
 * created by Rui Santos, https://randomnerdtutorials.com
 * 
 * Complete Guide for Ultrasonic Sensor HC-SR04
 *
    Ultrasonic sensor Pins:
        VCC: +5VDC
        Trig : Trigger (INPUT) - Pin11
        Echo: Echo (OUTPUT) - Pin 12
        GND: GND
 */
 
const static int trigPin = 4;    // Trigger
const static int echoPin = 5;    // Echo
const static int power = 31;    // Echo

void setup() {
  //Serial Port begin
  Serial.begin (9600);
  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(power, OUTPUT);
  digitalWrite(power, LOW);
}
unsigned long time_d() 
{
  unsigned long d=0;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  noInterrupts();
  d = pulseIn(echoPin, HIGH, 12000);
  interrupts();
  return d;
}
void loop() {
  unsigned long total_time = 0;
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
 for (int i=0 ; i<10 ; i++) {
  total_time += time_t();
 }
 //total_time = 10 * us => t = tt/10,000,000 (s) 
  float dist = total_time/145.0;
  Serial.print(dist);
  Serial.print("cm");
  Serial.println();
  delay(250);
}
