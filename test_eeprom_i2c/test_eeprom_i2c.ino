#include <Wire.h>
/*
 * For Microchip 241025 :
 *  Upon receiving a ‘1010’ code and appropriate device select bits:
 *  1010 B0 A1 A0
 *  if A1 A0 are 0 this is 80 in decimal
 */
const int theDeviceAddress = 80;
const static int EN_EEPROM = 40;
void setup(void) {
  pinMode (EN_EEPROM, OUTPUT);
  digitalWrite(EN_EEPROM, 0); //0 to enable
  Serial.begin(9600);
  Wire.begin();
  
}

void loop(void) {
  for (unsigned int theMemoryAddress = 0; theMemoryAddress < 256; theMemoryAddress++) {
    WireEepromWriteByte(theDeviceAddress, theMemoryAddress, (byte)theMemoryAddress);
  }
  for (unsigned int theMemoryAddress = 0; theMemoryAddress < 256; theMemoryAddress++) {
    Serial.print("Byte: ");
    Serial.print(theMemoryAddress);
    Serial.print(", ");
    Serial.print((int)WireEepromReadByte(theDeviceAddress, theMemoryAddress));
    Serial.println(".");
  }
  for (unsigned int theMemoryAddress = 0; theMemoryAddress < 1024; theMemoryAddress++) {
   WireEepromWriteInt(theDeviceAddress, theMemoryAddress * 2, (int)theMemoryAddress);
   }
   for (unsigned int theMemoryAddress = 0; theMemoryAddress < 1024; theMemoryAddress++) {
   Serial.print("Int: ");
   Serial.print(theMemoryAddress);
   Serial.print(", ");
   Serial.print(WireEepromReadInt(theDeviceAddress, theMemoryAddress * 2));
   Serial.println(".");
   }
   delay(5000);
}

void WireEepromRead(int theDeviceAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(theDeviceAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.endTransmission();
    delay(5);
    Wire.requestFrom(theDeviceAddress, sizeof(byte));
    theByteArray[theByteIndex] = Wire.read();
  }
}

byte WireEepromReadByte(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(byte)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(byte), theByteArray);
  return (byte)(((theByteArray[0] << 0)));
}

int WireEepromReadInt(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(int)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(int), theByteArray);
  return (int)(((theByteArray[0] << 8)) | (int)((theByteArray[1] << 0)));
}

void WireEepromWrite(int theDeviceAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(theDeviceAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.write(theByteArray[theByteIndex]);
    Wire.endTransmission();
    delay(5);
  }
}

void WireEepromWriteByte(int theDeviceAddress, unsigned int theMemoryAddress, byte theByte) {
  byte theByteArray[sizeof(byte)] = {(byte)(theByte >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(byte), theByteArray);
}

void WireEepromWriteInt(int theDeviceAddress, unsigned int theMemoryAddress, int theInt) {
  byte theByteArray[sizeof(int)] = {(byte)(theInt >> 8), (byte)(theInt >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(int), theByteArray);
}
