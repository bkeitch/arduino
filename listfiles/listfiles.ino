/*
  Listfiles

  This example shows how print out the files in a
  directory on a SD card

  The circuit:
   SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)

  created   Nov 2010
  by David A. Mellis
  modified 9 Apr 2012
  by Tom Igoe
  modified 2 Feb 2014
  by Scott Fitzgerald

  This example code is in the public domain.

*/
#include <SPI.h>
#include <SD.h>

File root;

#define EN_SD1 36
#define EN_SD2 38
#define EN_EEPROM 40
#define CS1 48
#define CS2 49

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  pinMode(EN_SD1, OUTPUT);
  pinMode(EN_SD2, OUTPUT);
  digitalWrite(EN_SD2, HIGH);
  digitalWrite(EN_SD1, LOW);
  pinMode(EN_EEPROM, OUTPUT);
  digitalWrite(EN_EEPROM, LOW);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
delay(500);
  Serial.print("Initializing SD card...");

  if (!SD.begin(CS1)) {
    Serial.println("initialization failed!");
    
  } else {
    Serial.println("initialization done.");
  
    root = SD.open("/");
  
    printDirectory(root, 0);
  
    Serial.println("done!");
  
    SD.end();
  }
  digitalWrite(EN_SD2, LOW);
  digitalWrite(EN_SD1, HIGH);
  delay(500);
  if (!SD.begin(CS2)) {
    Serial.println("initialization failed!");
    
  } else {
    Serial.println("initialization done.");
  
    root = SD.open("/");
  
    printDirectory(root, 0);
  
    Serial.println("done!");
  
    SD.end();
  }
  digitalWrite(EN_SD1, HIGH);
  digitalWrite(EN_SD2, HIGH);
  digitalWrite(EN_EEPROM, HIGH);
  
}

void loop() {
  // nothing happens after setup finishes.
}

void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}
