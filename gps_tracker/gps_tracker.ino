/*
 * Uses Maplin GPS to acquire fix on press of a button
 * Then transmits over Iridium
 */
#include "TinyGPS++.h"
#include <SPI.h>
#include <SD.h>
//#include <IridiumSBD.h>
File logFile;

//#include <SoftwareSerial.h>
// configure software serial port for GPS (Serial3 Mega)
//SoftwareSerial nss(2,3); 
//configure software serial for Iridium (Serial1 Mega)
//SoftwareSerial inss(4, 5); 
//IridiumSBD isbd(Serial1, 7);
int btnVal;
bool gpsPower = false;

static const int LED_RED = 7;
static const int LED_GREEN = 8;
static const int LED_YELLOW = 9;
static const int btnPIN = 5;
static const int gpsCTRL = 6;
static const char defaultMessage[] = "Position update.";
static const int maxChars = 62; // limit of Iridium/Rock block
char message[maxChars+1]; // must be big enough for digits and terminating null
int index = 0;         // the index into the array storing the received digits


// Create an instance of the TinyGPS object
TinyGPSPlus gps;
unsigned int messageCount;
void setup()
{
  messageCount = 0;
  pinMode(btnPIN, INPUT); 
  pinMode(gpsCTRL, OUTPUT);
//  swOffGPS(); // GPS is active low, so as soon as pin is set, switch it off.
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_YELLOW, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  SD.begin(19);//10 UNO 19 MEGA
//  isbd.attachConsole(Serial);
//  isbd.setPowerProfile(1);
  Serial.println(F("Type your messsage now (max 62 characters) and press return :"));
  //Flash LEDs so we know they work
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_YELLOW, LOW);
  digitalWrite(LED_GREEN, LOW);
  delay(500);
  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_YELLOW, HIGH);
  digitalWrite(LED_GREEN, HIGH);
  swOnGPS();
  delay(1000); // warm up GPS
setFile();
}

bool sendMessage(char* message) {
  char userMsg[250];
  //if(strlen(message) > 3) {
   //if (gps.time.isUpdated())
  {
    digitalWrite(LED_GREEN, HIGH);
    sprintf(userMsg, "{\"d\":\"%04u-%02u-%02u\",\"t\":\"%02u:%02u:%02u.%02u\",\"p\":[%02u.%09lu,%03u.%09lu],\"m\":\"%s\"}", 
    gps.date.year() , gps.date.month() , gps.date.day() , gps.time.hour() , gps.time.minute() , gps.time.second(),
    gps.time.centisecond(), gps.location.rawLat().deg, gps.location.rawLat().billionths, 
    gps.location.rawLng().deg, gps.location.rawLng().billionths, message);
    logFile.println(userMsg);
    Serial.println("Writing: ");
  //} else  {
    //sprintf(userMsg, "%s\n" , "No data");
  } 
  digitalWrite(LED_GREEN, LOW);    // green on = GPS success
  messageCount++;

  Serial.println(userMsg); 
  return true;

}

typedef enum {
    BTN_STATE_RELEASED,
    BTN_STATE_PRESSED
} ButtonState;

ButtonState btnStatus = BTN_STATE_RELEASED;


void loop()
{

  while (Serial3.available() > 0)
  {
    int c = Serial3.read();
    Serial.write(c);
    if (gps.encode(c)) {
      // process new gps info here
      if (gps.time.isUpdated())
      {
        Serial.print("\nLAT="); Serial.print(gps.location.lat(), 6);
        Serial.print("\tLNG="); Serial.println(gps.location.lng(), 6);
        if(messageCount < 100) sendMessage(message);
      }
//      //  success so switch off GPS
//        swOffGPS();
    }
  }

 while( Serial.available())
  {
    char ch = Serial.read();
    if(index <  maxChars && ch != '\r' && ch != '\n'){
      message[index++] = ch; // add the ASCII character to the string;
      
    }
    else
    {
      // here when buffer full or on the first non digit
      message[index] = 0;        // terminate the string with a 0
      //toggleGPS(); //fire sending straight away.
    }
  }
  btnVal = digitalRead(btnPIN);
//    delay(5000);

if(messageCount == 100) {
  swOffGPS();
  unsetFile();
  Serial.println("Finished");
}

  switch(btnStatus)
  {
  case BTN_STATE_PRESSED:
      // Handle button release
      if(btnVal == HIGH)
      {
          btnStatus = BTN_STATE_RELEASED;
      }

      break;

  case BTN_STATE_RELEASED:
      // Handle button press
      if(btnVal == LOW)
      {
          //toggleGPS();
//          sendMessage();
          btnStatus = BTN_STATE_PRESSED;
      }
      break;
  }
}
void toggleGPS() {
  if(gpsPower == false)
    swOnGPS();
  else
    swOffGPS();
}
/**
 * GPS module has active LOW - see datasheet.
 */
void swOnGPS()
{
  digitalWrite(LED_YELLOW, LOW);    // green on = GPS on
  Serial3.begin(9600);
  digitalWrite(gpsCTRL, LOW);
  gpsPower = true;
}
void swOffGPS()
{
  digitalWrite(LED_YELLOW, HIGH);    // green off = GPS off
  Serial3.end();
  digitalWrite(gpsCTRL, HIGH);
  gpsPower = false;
}

/*
void sendIridium(char* msg) {
  int signalQuality = -1;

  Serial1.begin(19200);
  
  isbd.begin();
  
  int err = isbd.getSignalQuality(signalQuality);
  if (err != 0)
  {
    Serial.print("SignalQuality failed: error ");
    digitalWrite(LED_GREEN, HIGH);    
    digitalWrite(LED_RED, LOW);    // show fail with red LED
    Serial.println(err);
    return;
  }

  Serial.print(F("Signal quality is "));
  Serial.println(signalQuality);
  Serial.print(F("Sending message: "));
  Serial.println(msg);
  err = isbd.sendSBDText(msg);
  if (err != 0)
  {
    Serial.print(F("sendSBDText failed: error "));
    digitalWrite(LED_GREEN, HIGH);    
    digitalWrite(LED_RED, LOW);    // show fail with red LED
    Serial.println(err);
    return;
  }

  Serial.println(F("Message Sent"));
  digitalWrite(LED_RED, LOW);    
  digitalWrite(LED_GREEN, HIGH);    // show success with green LED
  //Serial.print("Messages left: ");
  //Serial.println(isbd.getWaitingMessageCount());   
  Serial1.end();
  isbd.sleep();
}


bool ISBDCallback()
{
   digitalWrite(ledPin, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
   return true;
}
*/
void setFile () {
  if (SD.exists("test.txt")) {
    Serial.println("test.txt exists.");
  } else {
    Serial.println("test.txt doesn't exist.");
  }

  // open a new file and immediately close it:
  Serial.println("Creating test.txt...");


  
  logFile = SD.open("test.txt", FILE_WRITE);

  // if the file opened okay, write to it:
  if (logFile) {
    Serial.print("Writing to test.txt...");
    
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}
void unsetFile() {
 if (logFile) {
    // close the file:
    logFile.close();
    Serial.println("done.");
  } else {
    // if the file didn't open, print an error:
    Serial.println("error opening test.txt");
  }
}
