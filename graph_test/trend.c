#include <stdio.h>
#include <math.h>
const int NUM_BARS= 32;
const char* getTrend () ;
int main() {
double pressures[NUM_BARS];
for(int i = 0; i < NUM_BARS; i++) {
	pressures[i] = 975-(i*0.1); //48/NUM_BARS); //start with very low pressure, to see range
printf(": %f ", pressures[i]);

}
printf("\n");
const char* t = getTrend(pressures);
printf("Trend: %s \n", t);
}
const char* getTrend (double* pressures) {
 double totalDiff = 0;
 for (int i = NUM_BARS-1; i >= NUM_BARS - 12; i--) {
 totalDiff += pow((pressures[i] - pressures [i-1]),2); //difference between this pressure and previous pressure
 }
 double rmsDiff = sqrt(totalDiff/12);
 if(rmsDiff <0.025) {
 return "T";
 } else if(rmsDiff <0.375) {
 return "S";
 } else if(rmsDiff <0.875) {
 return "N";
 } else if(rmsDiff <1.5) {
 return "Q";
 } else {
 return "R";
 }
}
