//#include <RtcDateTime.h>
//#include <RtcDS1307.h>

#include <openGLCD.h>
#include <Wire.h>

const int PERIOD = 6;//00;
const short WIDTH = 4;
const int theDeviceAddress = 80;
const int barometerInPin = A0;  // Analog input pin that the potentiometer is attached to
const int tempInPin = A1;  // Analog input pin that the LM10 temp sensor is attached to
//const int tempAddr = 0x48; //1001000
//Display is 128 x 64 pixels
// Top two rows are 8 + 8 of text. 
// Bargraph is 48 x 128 px
// bars are 1 mbar over 48 mbar scale
// width is 4 px per bar
// time is 32 bins of 15 mins = 8 hours
// 6 mbar in 3 hours = 16 mbar in 8 hours
// +/- 24 mbar in 8 hours = 3 mbar / hour or 9mb in 3 hours! (88mb in 12 hours world record)
const int NUM_BARS = 32; // 32 x 4 = 128 system 5x7 = 128/(5+1) = 22 chars
    
int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value converted to mV
byte value;
int addr = 180; 
int secondsRun = 0;
double pressures[NUM_BARS];



// initialize the RTC
//RtcDS1307<TwoWire> rtc(Wire);

void setup() {
  //Wire.begin();
  Serial.begin(115200);
  // set up the LCD's number of columns and rows: 
  // Initialize the GLCD 
  GLCD.Init();

 // Select the font for the default text area
  GLCD.SelectFont(System5x7);


  GLCD.print("Pressure Time Temp");
  //rtc.begin();
  //if (! rtc.GetIsRunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    //rtc.SetIsRunning(true);
    
  //} 
  //rtc.SetSquareWavePin(DS1307SquareWaveOut_Low);
  //Serial.print("#Current date:  ");
  //printDate(rtc.GetDateTime());
  //printTime(rtc.GetDateTime());
  //Serial.println();       
  for(int i = 0; i < NUM_BARS; i++) {
    pressures[i] = 975+(i*48/NUM_BARS); //start with very low pressure, to see range
  }
}
/*
void printDate(RtcDateTime now) {
  Serial.print(now.Year(), DEC);
  Serial.print('/');
  Serial.print(now.Month(), DEC);
  Serial.print('/');
  Serial.print(now.Day(), DEC);
  Serial.print(' ');
}
void printTime(RtcDateTime now) {
  Serial.print(now.Hour(), DEC);
  Serial.print(':');
  if(now.Minute() < 10)
     Serial.print('0');
  Serial.print(now.Minute(), DEC);
  Serial.print(':');
  if(now.Second() < 10)
     Serial.print('0');
  Serial.print(now.Second(), DEC);
}
void showTime(RtcDateTime now) {
  if(now.Hour() < 10)
     GLCD.print(' ');
  GLCD.print(now.Hour());
  GLCD.print(':');
  if(now.Minute() < 10)
     GLCD.print('0');
  GLCD.print(now.Minute());
  GLCD.print(':');
  if(now.Second() < 10)
     GLCD.print('0');
  GLCD.print(now.Second());
}*/
unsigned long TotalPressure, AveragePressure = 0;
short barNumber=0;
/**
 * LM35 temperature sensor outputs 10mV per C
 */
float getTemp() {
  unsigned long sensorValue = 0;
  for (int i = 0; i < 100; i++) {
    sensorValue += analogRead(tempInPin);
  }
  // map it to mV
  return map(sensorValue/100, 0, 1023, 0, 500);//using C scale
  //Wire.beginTransmission(tempAddr);
  //Wire.write((byte)(tempAddr));
  //Wire.endTransmission();
  //Wire.requestFrom(tempAddr, 2);
  //while(Wire.available())    // slave may send less than requested
  //{
    //temp = (uint8_t) Wire.read();    // receive a byte as character    
  //}
  //return temp;
}
/**
 * Retruns pressure in hPa (mBar) from a
 * Freescale MPX4115A pressure sensor
 * From the datasheet we use the following formula:
 * Vout = V x (.009 x P-.095) ± Error
 * VSs = 5.1 Vdc
 * TEMP = 0 to 85°C
 * P(kPa) = ( Vout/V + 0.095 ) / 0.009
 * Using 0-1000 as voltage scale => V =1000 
 * Multiply through by V
 * P(mBar) = Vout + (0.096*1000) 
 */
double getPressure() {
  // read the analog in value:
  unsigned long sensorValue = 0;
  for (int i = 0; i < 100; i++) {
    sensorValue += analogRead(barometerInPin);
  }
  // map it to mV
  double outputValue = map(sensorValue/100, 0, 1023, 0, 1000);//using arbitrary voltage scale
  // convert to nearest hPa as per data sheet
  return  ((outputValue + 95)/0.9);
  
}
void loop() {
  double pressure = getPressure();
  //TotalPressure += pressure;
  //RtcDateTime now = rtc.GetDateTime();

  //store datestamp in EEPROM every 10 mins
//  if(secondsRun%PERIOD==0) {
//    WireEepromWriteLong(theDeviceAddress, addr, now.TotalSeconds64());//outputValue
//    //Increment EEPROM address
//    addr += sizeof(long);
//    //store value in EEPROM
//    AveragePressure=round(TotalPressure/(float)PERIOD);
//    WireEepromWriteInt(theDeviceAddress, addr, AveragePressure);
//    addr+= sizeof(int);
//    //plot last 30 points
//    for (unsigned int num = 0, theMemoryAddress = 0, i=0; i<NUM_BARS; i++) {
//      if(addr < NUM_BARS*6)
//        break;
//      theMemoryAddress = addr + (i - NUM_BARS*6);
//      //long timestamp = WireEepromReadLong(theDeviceAddress, theMemoryAddress);
//      theMemoryAddress +=4;     
//      num = WireEepromReadInt(theDeviceAddress, theMemoryAddress);
////      if (num > MINVAL) 
////       num =- MINVAL;
////     else
////       num = 0;
////      GLCD.CursorTo(0,i%5+3);
////      showTime(DateTime(timestamp));
////      GLCD.print(' ');
////      GLCD.print(theMemoryAddress);
////      GLCD.print(' ');
////      GLCD.print(num);
  if (barNumber == NUM_BARS) 
    barNumber = 0;
  pressures[NUM_BARS-1] = pressure; //new pressure in right-most box

//  secondsRun=0; //reset hour count
//    TotalPressure = 0;
  //int curval = 5;
  for (unsigned short i=0; i<NUM_BARS; i++) {
    if(pressures[i] > pressure + 24) {
      GLCD.DrawVBarGraph(GLCD.Left+(i*WIDTH), GLCD.Bottom, WIDTH -1 , -(GLCD.Height-16), 1, (int) (pressure - 24), (int) (pressure + 24), (int) pressure + 24);
    } else if (pressures[i] < pressure - 24) {
      GLCD.DrawVBarGraph(GLCD.Left+(i*WIDTH), GLCD.Bottom, WIDTH, -(GLCD.Height-16), 0, (int) (pressure - 24), (int) (pressure + 24), (int) pressure -24);
    } else {
      GLCD.DrawVBarGraph(GLCD.Left+(i*WIDTH), GLCD.Bottom, WIDTH, -(GLCD.Height-16), 0, (int) (pressure - 24), (int) (pressure + 24), (int) pressures[i]);
    }
    //shift all points to the left, ready for next datapoint
    if(i < NUM_BARS - 1) 
      pressures[i] = pressures[i+1];
  }
//  }
  //Display current time and pressure
  GLCD.CursorTo(0, 1);
  if(pressure < 1000) //correct formatting
    GLCD.print(" ");
  GLCD.print(pressure);
  //GLCD.print("hPa");
  GLCD.print(getTrend());
  //GLCD.CursorTo(0, 2);
  
  //showTime(now);
  GLCD.print(" ");
  GLCD.print(secondsRun);
  //GLCD.print(" mins");
  //24 readings is 4 hours every 10 mins
  //144 Bytes of memory = 24 readings (6 bytes per measurement)  
//  if(addr >= 65535 ) //24A512 is 512Kibits = 64 Kibytes
//    addr = 0;
  GLCD.CursorTo(14, 1);
  GLCD.print(getTemp());
  GLCD.print("C");
  Serial.println(pressure);
  
  //Serial.println(" hPa");
  // send data if amount is requested (amount must be bytes, %6)
  /*
  String txtMsg = "";  
  char s;
  int retrieve = 0;
  while (Serial.available() > 0) {
    s=(char)Serial.read();
    if (s == '\n') {
      if(txtMsg[0] == 'T') {
        uint32_t timeFrom2000 = atol(txtMsg.substring(1).c_str());
        rtc.SetDateTime(timeFrom2000);
        printDate(rtc.GetDateTime());
        printTime(rtc.GetDateTime());
      } else if(txtMsg[0] == 'R') {
         for (unsigned int theMemoryAddress = 0; theMemoryAddress < 65535; ) {
            WireEepromWriteLong(theDeviceAddress, theMemoryAddress, 0L);//outputValue
            //Increment EEPROM address
            theMemoryAddress += sizeof(long);
            WireEepromWriteInt(theDeviceAddress, theMemoryAddress, 0);
            theMemoryAddress+= sizeof(int);
          }   
      } else if (txtMsg[0] == 'A') {
        addr = atoi(txtMsg.substring(1).c_str());
      } else {
        retrieve = atoi(txtMsg.c_str());
      }
      txtMsg = "";  
    } else {  
      txtMsg +=s; 
    }
  }
  
  
  if (retrieve > 0) {
    GLCD.CursorTo(0, 1);
    GLCD.print(F(" *Sending Data* "));
    for (unsigned int theMemoryAddress = 0; theMemoryAddress < retrieve; ) {
      long timestamp = WireEepromReadLong(theDeviceAddress, theMemoryAddress);
      Serial.print(timestamp);
      theMemoryAddress +=4;     
      Serial.print(", ");
      Serial.print(WireEepromReadInt(theDeviceAddress, theMemoryAddress));
      theMemoryAddress +=2;
      Serial.println();
    }   
  }*/
  ++secondsRun;   
  delay(900000); //delay 15 minutes (900 000) before redoing loop.
}
/* Pressure Trend Over 3 Hours
[T] Steady  Less than 0.1 mb
[S] Rising or Falling Slowly  0.1 to 1.5 mb
[N] Rising or Falling  1.6 to 3.5 mb
[Q] Rising or Falling Quickly  3.6 to 6.0 mb
[R] Rising or Falling Very Rapidly  More than 6.0 mb
 */
const char* getTrend () {
  double totalDiff = 0;
  for (int i = NUM_BARS-1; i >= NUM_BARS - 12; i--) {
    totalDiff += pow((pressures[i] - pressures [i-1]),2); //difference between this pressure and previous pressure
  }
  double rmsDiff = sqrt(totalDiff/12);
  if(rmsDiff <0.025) {
    return "T";
  } else if(rmsDiff <0.375) {
    return "S";
  } else if(rmsDiff <0.875) {
    return "N";
  } else if(rmsDiff <1.5) {
    return "Q";
  } else {
    return "R";
  }
}
/*


void WireEepromRead(int theDeviceAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(theDeviceAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.endTransmission();
    Wire.requestFrom(theDeviceAddress, 1);
    while(Wire.available()) {
      theByteArray[theByteIndex] = Wire.read();
    }
//    delay(5);
//    Wire.requestFrom(theDeviceAddress, sizeof(byte));
//    theByteArray[theByteIndex] = Wire.read();
  }
}

byte WireEepromReadByte(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(byte)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(byte), theByteArray);
  return (byte)(((theByteArray[0] << 0)));
}

int WireEepromReadInt(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(int)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(int), theByteArray);
  return ((((int)theByteArray[0] << 8)) | (((int)theByteArray[1] << 0)));
}
long WireEepromReadLong(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(long)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(long), theByteArray);  
  return ((((long)theByteArray[0] << 24) |(((long)theByteArray[1] << 16) | ((long)theByteArray[2] << 8))) | ((long)theByteArray[3] << 0));
}
void WireEepromWrite(int theDeviceAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(theDeviceAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.write(theByteArray[theByteIndex]);
    Wire.endTransmission();
    delay(5);
  }
}

void WireEepromWriteByte(int theDeviceAddress, unsigned int theMemoryAddress, byte theByte) {
  byte theByteArray[sizeof(byte)] = {(byte)(theByte >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(byte), theByteArray);
}

void WireEepromWriteInt(int theDeviceAddress, unsigned int theMemoryAddress, int theInt) {
  byte theByteArray[sizeof(int)] = {(byte)(theInt >> 8), (byte)(theInt >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(int), theByteArray);
}

void WireEepromWriteLong(int theDeviceAddress, unsigned int theMemoryAddress, long theLong) {
  byte theByteArray[sizeof(long)] = {(byte)(theLong >> 24), (byte)(theLong >> 16), (byte)(theLong >> 8), (byte)(theLong >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(long), theByteArray);
}
*/
