typedef struct PhaseFreqDet {
		unsigned char qsig: 1;
		unsigned char qref: 1;
		unsigned char lsig: 1;
		unsigned char lref: 1;
		unsigned char direction: 1;
		unsigned char sig: 1;
		unsigned char ref: 1;
		unsigned char rst: 1;
		};