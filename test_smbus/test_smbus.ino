#include <Wire.h>
#include <stdint.h>

#define ADDR_Ax 0b000 //A2, A1, A0
#define ADDR (0b1101 << 3) + ADDR_Ax

void setup() {
  //pinMode(A4, INPUT);
  //pinMode(A5, INPUT);
  Serial.begin(9600);
  Wire.begin();
  //Wire.setClock(31000L); //31 kHz
  Wire.beginTransmission(ADDR);
  Wire.write(0x00); //Sets the start address for use in the upcoming reads
  Wire.endTransmission();
  for(uint8_t i=0;i<8;++i){ //cycle through enough times to capture entire chip
    Wire.requestFrom(ADDR,32,1); //read 32 bytes at a time
    while (Wire.available()){
      uint8_t c = Wire.read();
      Serial.write(c); //Send raw data over serial to computer
    }
  }

}

void loop() {
  
}
