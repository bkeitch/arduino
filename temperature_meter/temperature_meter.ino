#include <LiquidCrystal.h>


int lcdRS = 10;                               // Define output pins for LCD
int lcdE = 11;
int lcdD4 = 6;
int lcdD5 = 7;
int lcdD6 = 8;
int lcdD7 = 9;

const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)
int samples = 0;
float average = 0;
  

LiquidCrystal lcd(lcdRS, lcdE, lcdD4, lcdD5, lcdD6, lcdD7); 
void setup() {
  // set up the LCD memory locations
  lcd.begin(16,2);
  
  // clear the screen
  lcd.clear();
  //set left hand screen
  lcd.setCursor(0,0);
  //print headers
  lcd.print("Temp:");
}

void loop() {
    // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1024, 0, 5000);
  delay(100);
  if (samples < 10) {
    average +=outputValue;
    ++samples;
  } else {
    //set right hand screen
    lcd.setCursor(0,1);
    lcd.print(average/100.0);
    lcd.setCursor(7,1);
    lcd.print('C');
    samples = 0;
    average = 0;
  }
  
}
