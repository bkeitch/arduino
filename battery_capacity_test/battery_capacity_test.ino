/*
  Battery Capacity Tester
  
  Works with a MOS FET and load resistor module
  Pin 12        FET Gate
  Pin Analog 0  FET Drain
  Pin Analog 1  Battery +
  GND           GND
 
  Handles batteries from 1.2--8.5 Volts. But MUST change load resistor! See below! 
  
  A ATmega320 has 2 kB of SRAM. For each measurement 2 voltages of 2 bytes and a time of 2 bytes (seconds)
  will be stored = total of 6 bytes. This gives a maximum of about 250 measurements. 
  Only every 10 measurement will be stored.
  
  Every measurement is printed to the serial monitor - after the testing is finished the measurements stored
  in memory will also be printed out. This reduced set of measurements is better suited to do the calculations 
  on I think - but you might easily use the full set also or store some other ratio - as long as the memory 
  is not owerflowed. Or skip the memory storing alltogether - this gives you the ability to do as many 
  measurements as you wish!  :-)
  
  UPDATE NOTE: Best to replace the printed "," with tab chars instead - makes it REALLY easy to copy and 
  paste into Excel!
  
  Resistor network used in module is 33 kohm and 47 kohm giving ratio of 1:1.702
  
  By Photoman at more4u2c.wordpress.com 2011 April
 
*/

int GatePin=12;
int LEDPin=13;
int analogStart;
int breakLevel;
int nowHigh;
unsigned long onTime;
unsigned long offTime;
unsigned long startTime;
int tvval[3][200];
 

void setup() {
  int i;
  
  pinMode(GatePin, OUTPUT);  
  pinMode(LEDPin, OUTPUT);  
  Serial.begin(9600);
  Serial.println("Battery Capacity Tester");
  
  // Initial readings
  analogStart=analogRead(1);

  
  // Decide which battery is connected 1.2V NiMh, 3.6V Li-ion or 7.2 Li-ion
  // and set upp break off voltages, timing etc
  
  // 1.1 Volts to 1.6 Volt - assume a 1.2 V NiMh battery >120
  if ((analogStart>120) and (analogStart<200))
  {
    breakLevel=121;   // Safe level for NiMh is around 1 Volt
    onTime=20000;
    offTime=20000;
    Serial.println("Battery 1.2 V - Use 1 Ohm resistor!");
  }
  // 3.35 Volts to 4.5 Volts assume a one-cell Li-ion battery 540
  else if ((analogStart>400) and (analogStart<540))
  {
    breakLevel=361;   // Safe level for Li-ion one-cell is around 3 Volt
    onTime=16000;      // Should be set to 16000
    offTime=16000;     // Should be set to 16000
    Serial.println("Battery 3.6 V - Use 5.6 Ohm resistor!");
  }
  // 7 Volts to 8.5 Volts assume a two-cell Li-ion battery
  else if ((analogStart>842) and (analogStart<1023))
  {
    breakLevel=622;   // Safe level for Li-ion two-cell is around 6 Volt
    onTime=5000;
    offTime=5000;
    Serial.println("Battery 7.4 V - Use 11.2 Ohm resistor! (2x5.6)");
  }
  else
  {
    Serial.print(analogStart);
    Serial.println("  Can not identify battery!");
    Serial.println("No test will start!");
    
    while(1==1)
    {
      digitalWrite(LEDPin,HIGH);
      delay(50);
      digitalWrite(LEDPin,LOW);
      delay(200);
      digitalWrite(LEDPin,HIGH);
      delay(50);
      digitalWrite(LEDPin,LOW);
      delay(500);
    }
  }
  
  nowHigh=analogStart;

  Serial.println("Testing will start in 20 seconds!");
  Serial.println("");

  // Flash LED to indicate waiting
  for (i=0;i<40;i++)
  {
    digitalWrite(LEDPin,HIGH);
    delay(50);
    digitalWrite(LEDPin,LOW);
    delay(450);
  }
  
}

void loop() {
  int i;
  int j;
  int nowSec;
  int nowLow;
  
  // Print header for file 
  Serial.print("BaT01 ");
  Serial.println(breakLevel);
  Serial.print("0,0,");
  Serial.print(analogStart);
  Serial.print(",");
  Serial.println(analogStart);

  startTime=millis();
  j=0;
  i=0;
  
  // While voltage > breakVoltage then do testing
  while (nowHigh>breakLevel)
  {
    // Switch on FET/load
    digitalWrite(GatePin,HIGH);
    digitalWrite(LEDPin,HIGH);
    
    // Wait n seconds
    delay(onTime);
    
    // Measure values under load
    nowSec=(millis()-startTime)/1000;   // Get time in secs
    nowLow=analogRead(0);               // Get lower resistance voltage
    nowHigh=analogRead(1);              // Get battery voltage
      
    // Store voltages and times fore every 10th loop
    if (0==(j % 10))
    {
      tvval[0][i]=nowSec;               // Store time in secs
      tvval[1][i]=nowLow;               // Store lower resistance voltage
      tvval[2][i]=nowHigh;              // Store battery voltage

      i++;
    }
    
    // Switch off FET/LOad
    digitalWrite(GatePin,LOW);
    digitalWrite(LEDPin,LOW);

    // Print values for EACH loop - nice to see that something goeas on...
    Serial.print(j);
    Serial.print(",");
    Serial.print(nowSec);
    Serial.print(",");
    Serial.print(nowLow);
    Serial.print(",");
    Serial.println(nowHigh);

    
    // Wait m seconds
    delay(offTime);
    
    j++;
  }
 
  // Print header for file 
  Serial.println("");
  Serial.print("BaT01-10th ");
  Serial.println(breakLevel);
  Serial.print("0,");
  Serial.print(analogStart);
  Serial.print(",");
  Serial.println(analogStart);

 
  // Transfer results to PC
  for (j=0;j<i;j++)
  {
    Serial.print(tvval[0][j]);
    Serial.print(",");
    Serial.print(tvval[1][j]);
    Serial.print(",");
    Serial.println(tvval[2][j]);
    
  }
  Serial.println("");
  Serial.println("Test is finished!");
 
  // DoubleFlash LED13 "forever" to indicate that test is complete
  while(1==1)
  {
    digitalWrite(LEDPin,HIGH);
    delay(50);
    digitalWrite(LEDPin,LOW);
    delay(200);
    digitalWrite(LEDPin,HIGH);
    delay(50);
    digitalWrite(LEDPin,LOW);
    delay(500);
  }

  
}

