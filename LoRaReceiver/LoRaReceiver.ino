#include <SPI.h>
#include <LoRa.h>
#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    868E6


#include <WiFi.h>
#include <WiFiUdp.h>


//IP address to send UDP data to:
// either use the ip address of the server or 
// a network broadcast address
const char* ssid = "VM0850127";
const char* pwd = "h4zQwFvhqrrv";


const char * udpAddress = "192.168.0.210";
const int udpPort = 3333;

//Are we currently connected?
boolean connected = false;

//The udp library class
WiFiUDP udp;





void connectToWiFi(){
  Serial.println("Connecting to WiFi network: " + String(ssid));

  // delete old config
  WiFi.disconnect(true);
  //register event handler
  WiFi.onEvent(WiFiEvent);
  
  //Initiate connection
  WiFi.begin(ssid, pwd);

  Serial.println("Waiting for WIFI connection...");
}

//wifi event handler
void WiFiEvent(WiFiEvent_t event){
    switch(event) {
      case SYSTEM_EVENT_STA_GOT_IP:
          //When connected set 
          Serial.print("WiFi connected! IP address: ");
          Serial.println(WiFi.localIP());  
          //initializes the UDP state
          //This initializes the transfer buffer
          udp.begin(WiFi.localIP(),udpPort);
          connected = true;
          break;
      case SYSTEM_EVENT_STA_DISCONNECTED:
          Serial.println("WiFi lost connection");
          connected = false;
          break;
      default: break;
    }
}



long int lastSentTime;
static const long int keepAlive = 600000; //10 minutes
void setup(void) {
  Serial.begin(9600);
  while (!Serial);

  
  //Connect to the WiFi network
  while(!connected) {
    connectToWiFi();
    delay(2000);
  }

  

  Serial.println("LoRa Receiver");
  SPI.begin(SCK,MISO,MOSI,SS);
  LoRa.setPins(SS,RST,DI0);  
  if (!LoRa.begin(BAND)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

    // register the receive callback
  LoRa.onReceive(onReceive);

  // put the radio into receive mode
  LoRa.receive();
  lastSentTime = millis();
}
void loop() {
  if (connected && (keepAlive < millis() - lastSentTime)) {
    udp.beginPacket(udpAddress,udpPort);
    udp.write( (uint8_t*) "KEEPALIVE" ,10);
    udp.write( (uint8_t*) (&lastSentTime) ,4);
    udp.endPacket();
    lastSentTime = millis();
  }
}
/**
 * send a packet on WiFi using UDP. Do nothing if WiFi is not up.
 */
void sendWiFi(uint8_t * buffer, int packetSize) {  
  //only send data when connected
  if(connected){
    //Send a packet
    udp.beginPacket(udpAddress,udpPort);
    udp.write( buffer , packetSize);
    udp.endPacket();
    lastSentTime = millis();
  } 
  while(!connected) {
    connectToWiFi();
    delay(20000);
  }
}
/**
 * callback on receiving a LoRa packet. Collect the packet and then call
 * @sendWiFi to retransmit on WiFi
 */
void onReceive(int packetSize) {
  boolean newPacket = false;
  uint8_t buffer[packetSize+1];
  // received a packet
  Serial.print("Received packet: ");
  Serial.println(packetSize);
  
  
  // read packet
  for (unsigned int  i = 0; i < packetSize; i++) {
    buffer [i] = ((uint8_t)LoRa.read());
  }
  buffer[packetSize] = '\0';
  Serial.println((char*)buffer);
  sendWiFi(buffer, packetSize);
  // print RSSI of packet
  Serial.print(" with RSSI ");
  Serial.println(LoRa.packetRssi());
}
