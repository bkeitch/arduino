#include <SPI.h>
#include <LoRa.h>

#define SCK     5    // GPIO5  -- SX1278's SCK
#define MISO    19   // GPIO19 -- SX1278's MISO
#define MOSI    27   // GPIO27 -- SX1278's MOSI
#define SS      18   // GPIO18 -- SX1278's CS
#define RST     14   // GPIO14 -- SX1278's RESET
#define DI0     26   // GPIO26 -- SX1278's IRQ(Interrupt Request)
#define BAND    868E6
#define BUTTON_PIN_BITMASK 0x1 // 2^0 in hex

RTC_DATA_ATTR int bootCount = 0;
RTC_DATA_ATTR int counter = 0;
#define DEBUG    false


void setup(){
  Serial.begin(9600);
  while (!Serial);
  Serial2.begin(9600);
  while (!Serial2);
  pinMode(GPIO_NUM_0, INPUT_PULLUP);
  //Increment boot number and print it every reboot
  ++bootCount;
  if(DEBUG) Serial.println("\nBoot number: " + String(bootCount));
  if(DEBUG) Serial.println("LoRa Sender");
  SPI.begin(SCK,MISO,MOSI,SS);
  LoRa.setPins(SS,RST,DI0);  
  if (!LoRa.begin(BAND)) {
    if(DEBUG) Serial.println("Starting LoRa failed!");
    while (1);
  }
  if(DEBUG) Serial.println("Were up!");
}
static const int timeout = 10000;
void loop() {
  boolean packetNotSent = true;
  uint8_t buffer [200];
  if(DEBUG) Serial.println("waiting...");
  delay(500);
  if(DEBUG) Serial.println("sending c ...");
  Serial2.print('c');
  unsigned int i = 0;
  // if there's any serial available, read it:
  unsigned long last = millis();
  while (packetNotSent && (timeout > millis() - last)) {
    while (Serial2.available() > 0) { 
      // look for the next valid integer in the incoming serial stream:
      buffer[i] = (uint8_t) Serial2.read();
      // look for the newline. That's the end of your sentence:
      if (buffer[i] == '\n' || i > 199) {
        //break;
   
        if(DEBUG) Serial.print("Sending packet: ");
        if(DEBUG) Serial.println(counter);
        if(DEBUG) Serial.print("Size: ");
        if(DEBUG) Serial.println(i);
        if(DEBUG) buffer[i+1] = '\0';
        if(DEBUG) Serial.println((char*)(buffer));
        // send packet
        LoRa.beginPacket();
        // read the value from the sensor:
        
        LoRa.write(buffer, i);
        
        //LoRa.println(counter);
        LoRa.endPacket();
        
        counter++;
        Serial2.print('x');
        packetNotSent = false;
        
      }
      ++i;
    }
  }
  if(packetNotSent) {
    if(DEBUG) Serial.print("Timed Out after ");
    if(DEBUG) Serial.println((char*)buffer);
    if(DEBUG) Serial.println(i);
  }
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_0,0); //1 = High, 0 = Low

  //If you were to use ext1, you would use it like
  //esp_sleep_enable_ext1_wakeup(BUTTON_PIN_BITMASK,ESP_EXT1_WAKEUP_ANY_HIGH);

  //Go to sleep now
  if(DEBUG) Serial.println("Going to sleep now");
  //delay(1000);
  if(DEBUG)Serial.flush();
  esp_deep_sleep_start();

}
