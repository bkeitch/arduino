/*
  Test the LED decoder on an FPGA via SPI
  * CS - to digital pin 10  (SS pin)
  * MOSI - to digital pin 11 (MOSI pin)
  * MISO - to digital pin 12 (MISO pin)
  * CLK - to digital pin 13 (SCK pin)

 

*/


// inslude the SPI library:
#include <SPI.h>


// set pin 10 as the slave select for the digital pot:
const int slaveSelectPin = 10;

void setup() {
  // set the slaveSelectPin as an output:
  pinMode(slaveSelectPin, OUTPUT);  
  // initialize SPI:
 // SPI.setDataMode(SPI_MODE1);
//
//    SPI_MODE0
//    SPI_MODE1
//    SPI_MODE2
//    SPI_MODE3 


  SPI.begin();
}

void loop() {
    // change the value this channel from min to max:
    for (int level = 1; level < 256; level++ ) { // = level<<1) {
      fpga_write(level);
      
      delay(10000);
    }
//    // wait a second at the top:
//    delay(100);
//    // change the value on this channel from max to min:
//    for (int level = 0; level <= 255; level = pow(2,level)) {
//      fpga_write(0, level);
//      delay(1000);
//    }
 
}

void fpga_write(int value) {
  // take the SS pin low to select the chip:
  SPI.beginTransaction(SPISettings(400000, MSBFIRST, SPI_MODE1));
  digitalWrite(slaveSelectPin, LOW);
  delay(100);
  //  send in the address and value via SPI:
//  SPI.transfer(address);
  SPI.transfer(value);
  delay(100);
  // take the SS pin high to de-select the chip:
  digitalWrite(slaveSelectPin, HIGH);
  SPI.endTransaction();
}
