/*
   The circuit:
   * analog evaluation board AD9858 Rev(D)
   * External control pins of board attached to Arduino as follows:
   ** RIGHT 
   * 
   * RDB (CS)-------pin D10 22 green
   * WRB (SCLK)-----pin D13 19 blue 
   * A0 (SDIO)------pin D11 18 red
   * A1 (SDO)-------pin D12 17 white
   * A2 (IORESET)---pin D8  16 black
   * 
   * BOTTOM
   * RESET ---------pin D6 92 black btm
   * FUD -----------pin D7 99 blue
   * SPSELECT -------pin D5 91 red btm
   * PS0 ------------pin D4 97 white
   * PS1 ------------pin D3 98 red
   * SYNCLOCK NC green btm
  */
   
  #include <SPI.h>

/* UNO:
    (SS) : pin 10
    (MOSI) : pin 11
    (MISO) : pin 12
    (SCK) : pin 13
*/
  


  
  const int IORESET = 8;
  const int FUD = 7;
  const int RESET = 6;
  const int SPSELECT=5; //91 red btm
  const int PS0=4; //97 white
  const int PS1=3; //98 red
  
  const int timer = 100;
  int iB = 0;
  const byte READ = 0x80;
  const byte WRITE = 0x00;
  char buffer[10];
  byte startPosition=0;
  
 //Output frequency
  byte byte1=0xf6; //LSB
  byte byte2=0x28;
  byte byte3=0x5c;
  byte byte4=0x0f;//MSB

  
  //---------Functions-----------
  void FUD_Toggle()
    {
        digitalWrite (FUD, HIGH);
        delay(timer); // Delay in millisecond
        digitalWrite (FUD, LOW);
    }
 
  void Reset_Com()
    {
        digitalWrite (IORESET, HIGH);
        delay(timer);
        digitalWrite (IORESET, LOW);
    }
  void Reset_DDS()
    {
        digitalWrite (RESET, HIGH);
        delay(timer);
        digitalWrite (RESET, LOW);
    }
  void setup()
    {
        // Open serial communications and wait for port to open
        SPI.begin();
        Serial.begin(9600);
        // Set pin 10 to output, even if not in use
        pinMode(SS, OUTPUT);
        pinMode(RESET, OUTPUT);
        pinMode(FUD, OUTPUT);
        pinMode(SS, OUTPUT);
        pinMode(IORESET,OUTPUT);

        pinMode(SPSELECT, OUTPUT);
        pinMode(PS0, OUTPUT);
        pinMode(PS1, OUTPUT);
       
        digitalWrite (SPSELECT, HIGH);
        digitalWrite (PS0, LOW);
        digitalWrite (PS1, LOW);
        digitalWrite (FUD, LOW);
        
        digitalWrite (SS, HIGH);
        Reset_DDS();
        Reset_Com();
        digitalWrite (SS, LOW);
   
      }    
  
  //--------Main Program------------------------ 
  void loop()
    
           { 
        SPI.setDataMode(SPI_MODE0); //Rising edge - added as falling edge added later
        SPI.transfer (WRITE); //CFR AddreIORESET
        delay(timer);
        FUD_Toggle();
        
         
        //CFR: 4 bytes
        // single mode 0x00 00 00 7A
        SPI.transfer (0x00);
        delay(timer);
        SPI.transfer (0x00);//CFR[23]=1 to enable auto clear accumulator
        delay(timer);
        SPI.transfer (0x00); //CFR[15]=1 sweep mode enabled
        delay(timer);
        SPI.transfer (0x7A); //0x7A with CFR[6]=1 or 0x3A with CFR[6]=0
        delay(timer);
        
        FUD_Toggle();
        
        //Frequency Tuning Word
        SPI.transfer (0x03); //FTW AddreIORESET
        delay(timer);
         
        FUD_Toggle();
                         
        //FTW: 4 bytes
        SPI.transfer (byte4);
        delay(timer);
        SPI.transfer (byte3);
        delay(timer);
        SPI.transfer (byte2);
        delay(timer);
        SPI.transfer (byte1);
        delay(timer);
        FUD_Toggle();
  
       }
