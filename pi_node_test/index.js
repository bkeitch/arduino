/*
	SerialToJson.js
	a node.js app to read serial strings, convert them to
	JSON objects, and send them to webSocket clients
	requires:
		* node.js (http://nodejs.org/)
		* express.js (http://expressjs.com/)
		* socket.io (http://socket.io/#how-to-use)
		* serialport.js (https://github.com/voodootikigod/node-serialport)
		
	To call it type:
		node SerialToJSON.js portname

	where portname is the path to the serial port you want to open.
		
	created 1 Nov 2012
	modified 7 Nov 2012
	by Tom Igoe
	
*/

var SerialPort = require("serialport");				// include the serialport library
const express = require('express');
const	app = express();
const  	server = require('http').createServer(app);		// start an HTTP server
const  	io = require('socket.io').listen(server);		// filter the server using socket.io
const Readline = require('@serialport/parser-readline');
var portName = process.argv[2];						// third word of the command line should be serial port name
console.log("opening serial port: " + portName);	// print out the port you're listening on

server.listen(8080);								// listen for incoming requests on the server
console.log("Listening for new clients on port 8080");
var connected = false;

// open the serial port. Change the name to the name of your port, just like in Processing and Arduino:
var myPort = new SerialPort(portName);
// look for return and newline at the end of each data packet:

const parser = myPort.pipe(new Readline({ delimiter: '\r\n' }));
const base64url = require("base64-url");
const msgpack = require("msgpack");

  
// respond to web GET requests with the index.html page:
app.get('/', function (request, response) {
  response.sendfile(__dirname + '/index.html');
});


// listen for new socket.io connections:
io.sockets.on('connection', function (socket) {
	// if the client connects:
	if (!connected) {
		// clear out any old data from the serial bufffer:
		myPort.flush();
		// send a byte to the serial port to ask for data:
		myPort.write('c');
    	console.log('user connected');
    	connected = true;
    }

	// if the client disconnects:
	socket.on('disconnect', function () {
		myPort.write('x');
    	console.log('user disconnected');
    	connected = false;
  	});

	// listen for new serial data:  
	parser.on('data', function (data) {
		// Convert the string into a JSON object:
		//var serialData = JSON.parse(data);
		// for debugging, you should see this in the terminal window:
//var out = 	 base64url.decode(data)	;
const out = Buffer.from(data, 'base64');
		console.log(data);
		// send a serial event to the web client with the data:

/*
var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://192.168.0.210')
client.on('connect', function () {
  client.subscribe('gateway/fcf5c4ffff0cd4e0/event/up', function (err) {
    if (!err) {
      client.publish('gateway', 'Hello mqtt')
    }
  })
})
client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())

//-----------------
// packet decoding
const obj = JSON.parse(message.toString());
// decode a packet
const packet = lora_packet.fromWire(Buffer.from(obj["phyPayload"], "base64"));
 const lora_packet = require("lora-packet");
const packet = lora_packet.fromWire(data.toString(), "base64");

// debug: prints out contents
// - contents depend on packet type
// - contents are named based on LoRa spec
console.log("packet.toString()=\n" + packet.toString());

// e.g. retrieve payload elements
console.log("packet MIC=" + packet.MIC.toString("hex"));
console.log("FRMPayload=" + packet.FRMPayload.toString("hex"));

// check MIC
const NwkSKey = Buffer.from("00000000000000000000000000000000", "hex");
console.log("MIC check=" + (lora_packet.verifyMIC(packet, NwkSKey) ? "OK" : "fail"));

// calculate MIC based on contents
console.log("calculated MIC=" + lora_packet.calculateMIC(packet, NwkSKey).toString("hex"));

// decrypt payload
const AppSKey = Buffer.from("00000000000000000000000000000000", "hex");
var packetD = lora_packet.decrypt(packet, AppSKey, NwkSKey).toString() ;
console.log("Decrypted (ASCII)='" + packetD + "'");
console.log("Decrypted (hex)='0x" + lora_packet.decrypt(packet, AppSKey, NwkSKey).toString("hex") + "'");

//-----------------

//  client.end()
//  */
		var buf = new Buffer.from(out);
	console.log(buf.toJSON());	
		var oo =  msgpack.unpack(buf);
	console.log(oo.toString());	
		console.log(JSON.stringify(msgpack.unpack(buf)));
		socket.emit('serialEvent', JSON.stringify(oo));
})

});

