int time_h;
int time_l;
int photon = 13;
void setup(){
  //Serial.begin(9600);
  pinMode(photon, OUTPUT);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
}

void loop() {

  
  time_h = floor(random(3, 5));//number of photons arriving
  time_l = floor(random(5, 50)); //time between photons

  digitalWrite(photon, HIGH);
  delayMicroseconds(1);
  digitalWrite(photon, LOW);
  delayMicroseconds(1);
  
}
