// Arduino 7 segment display example software
// http://www.hacktronics.com/Tutorials/arduino-and-7-segment-led.html
// License: http://www.opensource.org/licenses/mit-license.php (Go crazy)

 

// Define the LED digit patters, from 0 - 9
// Note that these patterns are for common cathode displays
// For common anode displays, change the 1's to 0's and 0's to 1's
// 1 = LED on, 0 = LED off, in this order:

//                                    Arduino pin: 2,3,4,5,6,7,8
byte seven_seg_digits[10][7] = { { 1,1,1,1,1,1,0 },  // = 0
                                 { 0,1,1,0,0,0,0 },  // = 1
                                 { 1,1,0,1,1,0,1 },  // = 2
                                 { 1,1,1,1,0,0,1 },  // = 3
                                 { 0,1,1,0,0,1,1 },  // = 4
                                 { 1,0,1,1,0,1,1 },  // = 5
                                 { 1,0,1,1,1,1,1 },  // = 6
                                 { 1,1,1,0,0,0,0 },  // = 7
                                 { 1,1,1,1,1,1,1 },  // = 8
                                 { 1,1,1,0,0,1,1 }   // = 9
                                 };
  int timer =0;
  int i =0;
  byte pass=0;
void setup() {               
  pinMode(2, OUTPUT);  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);

  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);

//writeDot(0);  // start with the "dot" off
}

void showdigits (int number)
{

  // e.g. we have "1234"
  sevenSegWrite(number/1000);  // segments are set to display "1"
  digitalWrite(9, HIGH); // first digit on,
  digitalWrite(10, LOW); // other off
  digitalWrite(11, LOW);
  digitalWrite(12, LOW);
  
  delay (1);
  
  number = number%1000;  // remainder of 1234/1000 is 234
  digitalWrite(9, LOW); // first digit is off
  sevenSegWrite(number/100); //// segments are set to display "2"
  digitalWrite(10, HIGH); // second digit is on
  delay (1); // and so on....
  
  number =number%100;    
  digitalWrite(10, LOW);
  sevenSegWrite(number/10);
  digitalWrite(11, HIGH);
  delay (1);
  
  number =number%10; 
  digitalWrite(11, LOW);
  sevenSegWrite(number); 
  digitalWrite(12, HIGH);
  delay (1);

}

void loop ()

{ 
  timer++;
  
  showdigits (i);
  
  if (timer==10) {
  
    timer=0;
      if(floor(analogRead(0)) > 3 ){
        pass = 1;
      }
      if(floor(analogRead(0)) < 2 && pass ==1 ){
        pass = 0;
      
        i++;
      }
//    if (i>10000) {
//      i=0;
//    }
  
  
  
  } 

}

//void writeDot(byte dot) {
//  digitalWrite(9, dot);
//}
//void flashDot() {
//  byte STATE = 0;
//  for (byte count = 10; count > 0; --count) {
//   delay(400);
//   writeDot(STATE);
//   STATE = count % 2;
//  }
//}
  
void sevenSegWrite(byte digit) {
  byte pin = 2;
  for (byte segCount = 0; segCount < 7; ++segCount) {
    digitalWrite(pin, seven_seg_digits[digit][segCount]);
    ++pin;
  }
}

//void loop() {
//  for (byte count = 10; count > 0; --count) {
//   delay(1000);
//   sevenSegWrite(count - 1);
//  }
//  delay(1000);
//  flashDot();
//}
