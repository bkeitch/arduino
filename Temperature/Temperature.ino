/* Measuring temperature with Arduino using LM34/35 Temperature Sensor

Measuring Temperature using the LM34Z Temp Sensor with Arduino.

    Arduino Temp Sensor

for more information see http://www.ladyada.net/make/logshield/lighttemp.html
*/
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
#define aref_voltage 3.3         // we tie 3.3V to ARef and measure it with a multimeter!

//LM34Z Pin Variables
int tempPin = 1;        //the analog pin the LM34Z’s Vout (sense) pin is connected to
//the resolution is 10 mV / degree centigrade with a
//500 mV offset to allow for negative temperatures
int tempReading;        // the analog reading from the sensor

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
int curPos = 0;

void setup(void) {
// set up the LCD’s number of rows and columns:
lcd.begin(16, 2);

// If you want to set the aref to something other than 5v
analogReference(EXTERNAL);
}

void loop(void) {
lcd.noCursor();
delay(10);
// Turn on the cursor:
lcd.cursor();
delay(10);

tempReading = analogRead(tempPin);

lcd.clear();
lcd.setCursor(0, 1);
lcd.print(“Temp reading = “);
lcd.print(tempReading);

// converting that reading to voltage, which is based off the reference voltage
float voltage = tempReading * aref_voltage;
voltage /= 1024.0;

lcd.setCursor(0, 2);

// now print out the temperature
float temperatureC = (voltage – 0.5) * 100 ;  //converting from 10 mv per degree wit 500 mV offset
//to degrees ((volatge – 500mV) times 100)

lcd.print(temperatureC);
lcd.print(” degrees C”);

delay(1000);
}