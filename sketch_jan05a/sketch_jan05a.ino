#include <TimeLib.h>
#include <x_data.h>
#include <stdint.h>
#include <MsgPack.h>
//#include <DS3232RTC.h>

x_data xd;

//#include <station_x.h>

/**
 * Initialises the RTC on first power-up with compile time. This assumes
 * the RTC has not been programmed previously (e.g. power loss)
 * it also  
 * initialises the alarms to known values, clears the alarm flags, clears the alarm interrupt flags
 * and configures the INT/SQW pin for "interrupt" operation (disable square wave output)
*/ 
void displayTime(time_t t)
{
    Serial.print(year(t));
    Serial.print("-");
    Serial.print(month(t));
    Serial.print("-");
    Serial.println(day(t));
    if (hour(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(hour(t));
    Serial.print(":");
    if (minute(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(minute(t));
    Serial.print(":");
    if (second(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.println(second(t));
}

void display(x_config* sx);

void setup() {
  Serial.begin(9600);
    // Start the serial port connected to the satellite modem
  //setupIridium();
  char buffer[200];
  x_data xd;
  unsigned int mem = 64;
  float P2, T2, U;
  P2 = 1099.8;
  T2 = -62.123;
  U = 95.49;
  time_t t = millis();
 
  xd.update(T2, P2, U, t, mem);
  xd.updateGPSOffset(t, t+3);
  xd.updateIridiumOffset(t, t-2);
  xd.getJSONData(buffer);  
  //xd.getCSVData(buffer);  
  Serial.print( "{\"before\":");
  Serial.print(buffer);
  Serial.println(",");
  
 
  // serialize class directly
  MsgPack::Packer packer;
  packer.serialize(xd);
  
  Serial.print("\"tx_data\":\"") ;
  for(unsigned int i = 0 ; i < packer.size() ; i++) {
    if(packer.data()[i] < 10)
      Serial.print(0, HEX);
    Serial.print(packer.data()[i], HEX);
  }
  
  //sendIridium(sy, sz);

  Serial.println("\",");

  x_data xd2;
  MsgPack::Unpacker unpacker;
  unpacker.feed(packer.data(), packer.size());
  unpacker.deserialize(xd2);
 

  Serial.print("\"valid\": ");
  if(xd2 != xd)
    Serial.println("false,");
  else 
    Serial.println("true,");
  
  xd2.getJSONData(buffer);
  //xd2.getCSVData(buffer);  
  Serial.print("\"after\":");
  Serial.print(buffer);
  Serial.println("}");


  
}
void loop() {
}
