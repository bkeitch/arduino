/*
Weather station. Plot barometric trend as bar graph. Stores data in array
on Mega and uses RTC. 
BCK 2019

Hardware: I2C bus on pins 20/21
RTC: DS3231 RTC and 
Sensor: BMP280 on I2C 5V 

Analogue:
Sensor: MPX4115 at 5V on A?

Graphics:K108 64 x 128 on pins 24-34

GPS: Serial3 on Mega 14/15. PWR on D7

SD Card : SPI on pins 50-53 (Purple on 13)

Iridium: Modem on Serial1 18/19

openGLCD installed from Zip.

 */
#include "TinyGPS++.h"
#include <SPI.h>
#include <SD.h>
//#include <IridiumSBD.h>
#include <avr/sleep.h>//this AVR library contains the methods that controls the sleep modes
#define interruptPin 2 //Pin we are going to use to wake up the Arduino
#include <DS3232RTC.h>  //RTC Library https://github.com/JChristensen/DS3232RTC

#include <Adafruit_BMP280.h>
#include <openGLCD.h>
#include <Wire.h>

#define DS3231_I2C_ADDRESS 0x68

Adafruit_BMP280 bmp; // I2C
File logFile;
const int time_interval = 5;

const int NUM_BARS = 34;
const bool DEBUG = true;
double pressures[NUM_BARS]; //array to store pressures
int addr = 0; //array index
byte oldMinute; //initiated in startup()
//byte count = 0;
//double TotalPressure =0.0;
//double AveragePressure = 0.0;

//IridiumSBD isbd(Serial1, 7);

int btnVal;
bool gpsPower = false;

static const int LED_RED = 7;
static const int LED_GREEN = 8;
static const int LED_YELLOW = 9;
static const int btnPIN = 5;
static const int gpsCTRL = 6;
static const char defaultMessage[] = "Position update.";
static const int maxChars = 62; // limit of Iridium/Rock block


// Create an instance of the TinyGPS object
TinyGPSPlus gps;
unsigned int messageCount;


/*
 * Plot histogram of data in bottom part of screen
 * 
 */
void plotDataTest() { 
  unsigned int currentValue = 1013;
  unsigned int minval = currentValue -25;
  unsigned int maxval = currentValue +25;
  unsigned int startVal = 1000;
  for (byte i=0; i<NUM_BARS; i++) {
    unsigned int outputValue = startVal++; //pressure stored in hPa as double
    GLCD.DrawVBarGraph(GLCD.Left+1+(i*4), GLCD.Bottom-1, 5, -(GLCD.Height-2*10), 0, minval,maxval, outputValue);
  }
}
void shiftPressures() {
  for (unsigned int i =1 ; i < NUM_BARS; i++) {
   pressures[i-1] = pressures[i]; //array to store pressures
  }

}
/*
 * Display time via serial port
 */
void displayTime()
{
  time_t t;
  t=RTC.get();
  
  Serial.print(hour(t), DEC);
 
  Serial.print(":");
  if (minute(t)<10)
  {
    Serial.print("0");
  }
  Serial.print(minute(t), DEC);
  Serial.print(":");
  if (second(t)<10)
  {
    Serial.print("0");
  }
  Serial.print(second(t), DEC);
  Serial.print("T");
  Serial.print(day(t), DEC);
  Serial.print("/");
  Serial.print(month(t), DEC);
  Serial.print("/");
  Serial.println(year(t), DEC);
}



/*
 * Format time for displaying on LCD screen.
 */
void showTime() {
  time_t t;
  t=RTC.get();
  
  if(hour(t) < 10)
     GLCD.print(' ');
  GLCD.print(hour(t));
  GLCD.print(':');
  if(minute(t) < 10)
     GLCD.print('0');
  GLCD.print(minute(t));
  GLCD.print(':');
  if(second(t) < 10)
     GLCD.print('0');
  GLCD.print(second(t));
}
/*
 * Plot histogram of data in bottom part of screen
 * 
 */
void plotData(unsigned int currentIndex) { 
  unsigned int currentValue = int(2.0*pressures[currentIndex]); //2* gives accuracy to 0.5hPa
  unsigned int minval = currentValue -25;
  unsigned int maxval = currentValue +25; // 64 screen - 14 text = 50 pixels = 1 / hPa
  if(DEBUG) Serial.print(currentValue);  Serial.println("**");
  
  for (byte i=0; i<NUM_BARS; i++) {
    unsigned int outputValue = int(2.0*pressures[i]); //pressure stored in hPa as double 2* gives accuracy to 0.5hPa
    if(outputValue > maxval) outputValue = maxval;
    if(outputValue < minval) outputValue = minval;
    GLCD.DrawVBarGraph(GLCD.Left+1+(i*4), GLCD.Bottom-1, 5, -(GLCD.Height-2*10), 0, minval,maxval, outputValue);


  }
}
void outputArray() {
  for (byte i=0; i<NUM_BARS; i++) {
    unsigned int outputValue = int(2*pressures[i]); //pressure stored in hPa as double 2* gives accuracy to 0.5hPa
  
    Serial.print(outputValue); Serial.print(";"); 
  }
  Serial.println();
  
}
void toggleGPS() {
  if(gpsPower == false)
    swOnGPS();
  else
    swOffGPS();
}
/**
 * GPS module has active LOW - see datasheet.
 */
void swOnGPS()
{
  digitalWrite(LED_YELLOW, LOW);    // green on = GPS on
  Serial3.begin(9600);
  digitalWrite(gpsCTRL, LOW);
  gpsPower = true;
}
void swOffGPS()
{
  digitalWrite(LED_YELLOW, HIGH);    // green off = GPS off
  Serial3.end();
  digitalWrite(gpsCTRL, HIGH);
  gpsPower = false;
}

/*
 * Sends a message over Iridium 
 */

/*
void sendIridium(char* msg) {
  int signalQuality = -1;

  Serial1.begin(19200);
  
  isbd.begin();
  
  int err = isbd.getSignalQuality(signalQuality);
  if (err != 0)
  {
    Serial.print("SignalQuality failed: error ");
    digitalWrite(LED_GREEN, HIGH);    
    digitalWrite(LED_RED, LOW);    // show fail with red LED
    Serial.println(err);
    return;
  }

  Serial.print(F("Signal quality is "));
  Serial.println(signalQuality);
  Serial.print(F("Sending message: "));
  Serial.println(msg);
  err = isbd.sendSBDText(msg);
  if (err != 0)
  {
    Serial.print(F("sendSBDText failed: error "));
    digitalWrite(LED_GREEN, HIGH);    
    digitalWrite(LED_RED, LOW);    // show fail with red LED
    Serial.println(err);
    return;
  }

  Serial.println(F("Message Sent"));
  digitalWrite(LED_RED, LOW);    
  digitalWrite(LED_GREEN, HIGH);    // show success with green LED
  //Serial.print("Messages left: ");
  //Serial.println(isbd.getWaitingMessageCount());   
  Serial1.end();
  isbd.sleep();
}


bool ISBDCallback()
{
   digitalWrite(ledPin, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
   return true;
}
*/

void getGPSTime() {
  bool timeNeeded = true;
  swOnGPS();
  delay(1000); // warm up GPS
  unsigned long start_time = millis();
  while (timeNeeded) {
    if(millis() - start_time > 10000) timeNeeded = false; 
    while (Serial3.available() > 0)
    {
      int c = Serial3.read();
      if(DEBUG) Serial.write(c);
      if (gps.encode(c)) {
        // process new gps info here
        if (gps.time.isUpdated())
        {
          tmElements_t tm;
          tm.Hour = gps.time.hour();
          tm.Minute = gps.time.minute(),
          tm.Second = gps.time.second();
          tm.Day = gps.date.day();
          tm.Month = gps.date.month();
          tm.Year = (gps.date.year()-1970);      // tmElements_t.Year is the offset from 1970
          if(DEBUG) Serial.print(gps.time.value());
          RTC.write(tm);              // set the RTC from the tm structure
          timeNeeded = false;
        }
      }
    }
  }
  swOffGPS();

}
void getPosition()
{
  swOnGPS();
  delay(1000); // warm up GPS
  
  bool posNeeded = true;
  unsigned long start_time = millis();
  while(posNeeded) {
    if(millis() - start_time > 10000) posNeeded = false; 
    while (Serial3.available() > 0)
    {
      int c = Serial3.read();
      if(DEBUG) Serial.write(c);
      if (gps.encode(c)) {
        // process new gps info here
        if (gps.time.isUpdated())
        {
          tmElements_t tm;
          tm.Hour = gps.time.hour();
          tm.Minute = gps.time.minute(),
          tm.Second = gps.time.second();
          tm.Day = gps.date.day();
          tm.Month = gps.date.month();
          tm.Year = (gps.date.year()-1970);      // tmElements_t.Year is the offset from 1970
          RTC.write(tm);              // set the RTC from the tm structure
        }
        if (gps.location.isUpdated())
        {
          if(DEBUG) {Serial.print("\nLAT="); Serial.print(gps.location.lat(), 6);
          Serial.print("\tLNG="); Serial.println(gps.location.lng(), 6);}
             posNeeded = false;
        }
  
      }
    }
  }

  //      //  success so switch off GPS
  swOffGPS();

}
/*
 * Log position in JSON format on SD card
 */
void writeLog() {
  char userMsg[250];
  getPosition();
  
  sprintf(userMsg, "{\"T\":%lu, \"P\":%lu, \"d\":[%04u,%02u,%02u],\"t\":[%02u,%02u,%02u,%02u],\"p\":[%02u.%09lu,%03u.%09lu]}", 
    (long) bmp.readTemperature(), (long) bmp.readPressure(), gps.date.year() , gps.date.month() , gps.date.day() , gps.time.hour() , gps.time.minute() , gps.time.second(),
    gps.time.centisecond(), gps.location.rawLat().deg, gps.location.rawLat().billionths, 
    gps.location.rawLng().deg, gps.location.rawLng().billionths);
   if(DEBUG) {
    Serial.println("Writing: ");
    Serial.println(userMsg);
  }
 
  
  char userFile[20] = "example.txt";
  sprintf(userFile, "log%02u%02u.txt", gps.date.month() , gps.date.day());
  
  if (SD.exists(userFile)) {
    if(DEBUG) Serial.println("file exists.");
  } else {
    if(DEBUG) Serial.println("file doesn't exist.");
  }

  // open a new file and immediately close it:
  if(DEBUG) { Serial.print("Creating file...");
    Serial.println(userFile); }

  
  logFile = SD.open(userFile, FILE_WRITE);

  // if the file opened okay, write to it:
  if (logFile) {
    if(DEBUG) Serial.print("Opened test.txt...");
  logFile.println(userMsg);
    
  } else {
    // if the file didn't open, print an error:
    if(DEBUG) Serial.println("error opening test.txt");
  }
 if (logFile) {
    // close the file:
    logFile.close();
    if(DEBUG) Serial.println("file closed.");
  } else {
    // if the file didn't open, print an error:
    if(DEBUG) Serial.println("error file already closed");
  }
  
  
  
}
void setup()
{
  Wire.begin();
  pinMode(interruptPin,INPUT_PULLUP);//Set pin d2 to input using the buildin pullup resistor
  
  // initialize the alarms to known values, clear the alarm flags, clear the alarm interrupt flags
  RTC.setAlarm(ALM1_MATCH_DATE, 0, 0, 0, 1);
  RTC.setAlarm(ALM2_MATCH_DATE, 0, 0, 0, 1);
  RTC.alarm(ALARM_1);
  RTC.alarm(ALARM_2);
  RTC.alarmInterrupt(ALARM_1, false);
  RTC.alarmInterrupt(ALARM_2, false);
  RTC.squareWave(SQWAVE_NONE);
   // Initialize the GLCD 
  GLCD.Init();

  // Select the font for the default text area
  GLCD.SelectFont(System5x7);
  
  if(DEBUG) {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  }
  GLCD.CursorTo(2,2);
  GLCD.print(F("Sailor's"));
  GLCD.CursorTo(2,3);
  GLCD.print(F("Friend"));

  if (!bmp.begin()) {
    if(DEBUG) Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    else GLCD.print(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */ 

  tmElements_t tm;
  tm.Hour = 0;               // set the RTC to an arbitrary time
  tm.Minute = 0;
  tm.Second = 0;
  tm.Day = 4;
  tm.Month = 9;
  tm.Year = 2020 - 1970;      // tmElements_t.Year is the offset from 1970
  RTC.write(tm);              // set the RTC from the tm structure

  
  pinMode(btnPIN, INPUT); 
  pinMode(gpsCTRL, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
   pinMode(LED_YELLOW, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  
  
  SD.begin(53);//10 UNO 53 MEGA
//  isbd.attachConsole(Serial);
//  isbd.setPowerProfile(1);
  //Serial.println(F("Type your messsage now (max 62 characters) and press return :"));
  //Flash LEDs so we know they work
  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_YELLOW, HIGH);
  digitalWrite(LED_GREEN, HIGH);
  delay(500);
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_YELLOW, LOW);
  digitalWrite(LED_GREEN, LOW);
  delay(500);

//  getGPSTime();
//  GLCD.CursorTo(0,0);
//  GLCD.print(F("Pressure  Time"));

  if(DEBUG) displayTime();
  //initalise array at low pressure
  for (byte i=0; i<NUM_BARS; i++) {
    pressures[i] = 1000.0;
  }
  time_t t;
  //plotData(34);
  //plotDataTest();
  t=RTC.get();//Gets the current time of the RTC
  RTC.setAlarm(ALM1_MATCH_MINUTES , 0, (minute(t)+time_interval)%60, 0, 0);// Setting alarm 1 every 20 minutes
  // clear the alarm flag
  RTC.alarm(ALARM_1);
  // configure the INT/SQW pin for "interrupt" operation (disable square wave output)
  RTC.squareWave(SQWAVE_NONE);
  // enable interrupt output for Alarm 1
  RTC.alarmInterrupt(ALARM_1, true);

  
}

void showData() {
  double pressure = bmp.readPressure();
  //TotalPressure += pressure/100.0;
  /*
  //store data in array every 10 mins and replot graph
  if((minute*60+second - oldMinute*60)%600==0) {
    
    if(DEBUG) outputArray();
    //AveragePressure= TotalPressure/count;
    if(addr == 34) {
      shiftPressures();
    }
    pressures[addr] =  pressure/100.0 ; //AveragePressure;
    //plot last points
    plotData(addr);
    //wrap when array full 
    if(addr < 34 ) //34 doubles in array
      addr++;
    else
      addr = 0;
    displayTime();
    if(DEBUG)  Serial.print(pressure/100.0);    Serial.println(F(" hPa"));    Serial.print(bmp.readTemperature());    Serial.println(F(" C"));
    //TotalPressure = 0; //reset average
    if(DEBUG) Serial.print(oldMinute); Serial.print(" : "); Serial.println(minute);
    oldMinute = minute;
  }
    */
  //Display current time, temp and pressure

  GLCD.print(F("Temperature: "));
  //GLCD.CursorTo(15,0);
  GLCD.print(bmp.readTemperature());
  //GLCD.print((char)223); //degree symbol, not supported
  GLCD.print(F("C"));
  
  GLCD.CursorTo(0, 1);
  GLCD.print(F("Pressure  : "));
  if(pressure < 100000) //in Pa
    GLCD.print(F(" "));
  GLCD.print(pressure/100.0);
  GLCD.print(F("hPa"));
  
  GLCD.CursorTo(0, 2);
  
  GLCD.print(F("Longtitude: "));
  GLCD.print(gps.location.lng());
  GLCD.CursorTo(0, 3);
  GLCD.print(F("Lattitude : "));
  GLCD.print(gps.location.lat());
  GLCD.CursorTo(0, 4);
  GLCD.print(F("GPS Date  : "));
  GLCD.print(gps.date.value());
  GLCD.CursorTo(0, 5);
  GLCD.print(F("GPS Time  : "));
  GLCD.print(gps.time.value());
  GLCD.CursorTo(0, 6);
  GLCD.print(F("Clock Time: "));
  showTime();

}
void loop() {
 delay(1000);//wait 0.5 seconds before going to sleep. In real senairio keep this as small as posible
 gotoSleep();
// showData();
// writeLog();
   
}

void gotoSleep(){
    sleep_enable();//Enabling sleep mode
    attachInterrupt(0, wakeUp, LOW);//attaching a interrupt to pin d2
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);//Setting the sleep mode, in our case full sleep
//    digitalWrite(LED_BUILTIN,LOW);//turning LED off
    time_t t;// creates temp time variable
    t=RTC.get(); //gets current time from rtc
    if(DEBUG) Serial.println("Sleep  Time: "+String(hour(t))+":"+String(minute(t))+":"+String(second(t)));//prints time stamp on serial monitor
 //   delay(1000); //wait a second to allow the led to be turned off before going to sleep
    sleep_cpu();//activating sleep mode
    SD.begin(53);//10 UNO 53 MEGA
    t=RTC.get();
    if(DEBUG) Serial.println("WakeUp Time: "+String(hour(t))+":"+String(minute(t))+":"+String(second(t)));//Prints time stamp 
    showData();
    writeLog();
    delay(2000);
    //plotData(10);
    //Set New Alarm
    RTC.setAlarm(ALM1_MATCH_MINUTES , 0, (minute(t)+time_interval)%60, 0, 0);
  
  // clear the alarm flag
    RTC.alarm(ALARM_1);
  }

void wakeUp(){
  if(DEBUG) Serial.println("Interrrupt Fired");//Print message to serial monitor
   sleep_disable();//Disable sleep mode
  detachInterrupt(0); //Removes the interrupt from pin 2;
 
}
