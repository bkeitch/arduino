# Converts JSON format GPS data into GPX format data
# BCK 2020-10-05
import gpxpy
import gpxpy.gpx
import datetime
from datetime import datetime
import json

with open('log.json') as f:
  data = json.load(f)


gpx = gpxpy.gpx.GPX()

gpx.name = 'Hebredies'
gpx.description = 'Trip'

# Create first track in our GPX:
gpx_track = gpxpy.gpx.GPXTrack()
gpx.tracks.append(gpx_track)

# Create first segment in our GPX track:
gpx_segment = gpxpy.gpx.GPXTrackSegment()
gpx_track.segments.append(gpx_segment)

# Create points:

for d in data: 
  da = d['d'].split("-")
  ta = d['t'].split(":")
  sa = ta[2].split(".")
  x = datetime(int(da[0]),int(da[1]),int(da[2]),int(ta[0]),int(ta[1]),int(sa[0]), int(sa[1]))
  gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(d['p'][0],(d['p'][1]), elevation=1, time=x))

#Write out file

with  open ("log.gpx","w") as ff:
  ff.write(gpx.to_xml())

