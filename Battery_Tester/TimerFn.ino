// This portion of the code is written in AVR style
void init_timer1(void)
{
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;               // disabling PWM functionality form timer
  TCCR1B = 0;               // reseting before configuration
  TCNT1  = 0;               //

  OCR1A = 62500;            // compare match register 16MHz/256/1Hz
  TCCR1B |= (1 << WGM12);   // CTC mode
  TCCR1B |= (1 << CS12);    // 256 prescaler 
  TIMSK1 |= (1 << OCIE1A);  // enable timer compare interrupt
  interrupts();             // enable all interrupts
}

void update_time(void)
{
 
   noInterrupts();
   oldsec=sec; 
 
    if (sec>=60)
    {
      mint=mint+1; // after every 60 sec, minute is incremented by 1
      sec=0;
    }
    if (mint>=60)
    {
      hr=hr+1;// after every 60 min, hour is incremented by 1
      mint=0;
      if((hr==C)||(stopflag==1))
      { TIMSK1 &= ~(1 << OCIE1A); } // freeze runtime by disabling interrupt when test ends
    }
    interrupts();
 
}
