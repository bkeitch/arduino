// Loops the Display Screen after Test Stopped
void test_stopped(void)
{
   u8g.firstPage();  
  do {
    draw12();
  } while( u8g.nextPage() );
  
  
}

// Loops the Display Screen for manual stop input form user
void manual_stop(void)
{
   u8g.firstPage();  
  do {
    draw11();
  } while( u8g.nextPage() );
  
}

// Loops the Display Screen wheather to enter EEPROM for 
// Old test Data or Run a New Test
void fetch_or_test(void)
{
    u8g.firstPage();  
  do {
    draw0();
  } while( u8g.nextPage() );
 
}

// Loops the Display Screen during Start Up

void init_screen(void)
{
    u8g.firstPage();  
  do {
    draw1();
  } while( u8g.nextPage() );
 
}

// Loops the Display Screen with Update V,I,Time value
void update_disp(void)
{
    u8g.firstPage();  
  do {
    draw2();
  } while( u8g.nextPage() );
  
}

// Loops the Display Screen for User (input) Setup for current value
void set_batt_current(void)
{
    u8g.firstPage();  
  do {
    draw3();
  } while( u8g.nextPage() );
  
}

// Loops the Display Screen for User (input) Setup for c-rate value
void set_c_rate(void)
{
   u8g.firstPage();  
  do {
    draw4();
  } while( u8g.nextPage() );
  
  
}

// Loops the Display Screen with instructions before starting test
void begin_test(void)
{
   u8g.firstPage();  
  do {
    draw5();
  } while( u8g.nextPage() );
  
  
}

// Loops the Display Screen druing data Copying from EEPROM-Serial-USB(PC) connection
void eeprom_to_serial(void)
{
   u8g.firstPage();  
  do {
    draw7();
  } while( u8g.nextPage() );
  
  
}


void fetch_screen(void)
{
   u8g.firstPage();  
  do {
    draw10();
  } while( u8g.nextPage() );
  
  
}


// The follwing Draw functions are U8GLIB functions for displaying 
// Text/Data on the 1306 OLED Display, which are Sub-Function of the above Functions
// Details can be found : https://code.google.com/p/u8glib/wiki/u8glib

void draw0(void)
{
  u8g.setFont(u8g_font_5x8);
  u8g.drawStr( 0, 10, "MENU :NEW TEST / OLD DATA");
  u8g.setFont(u8g_font_6x10);
  u8g.drawStr( 0, 24, "PRESS S1 to COPY LAST");
  u8g.drawStr( 0, 36, "TEST'S DATA(EEPROM)");
  u8g.drawStr( 0, 50, "PRESS S2 for SETTING");
  u8g.drawStr( 0, 62, "NEW TEST PARAMETERS");
 
  
}


void draw1(void)
{
  u8g.setFont(u8g_font_6x12);
  u8g.drawStr( 0, 10, "DON'T CONNECT BATTERY");
  u8g.setFont(u8g_font_7x14B);
  u8g.drawStr( 0, 28, "LEAD ACID BATTERY");
  u8g.drawStr( 0, 46, "  CAPACITY TESTER ");
  u8g.drawStr( 0, 64, " Initializing....");
  
}

void draw2(void) {
  // graphic commands to redraw the complete screen should be placed here  
  u8g.setFont(u8g_font_6x10);
 //u8g.drawStr( x upto 128, y upto 64, "String")
 // u8g.drawStr( 0, 44, "Current (est):"); u8g.setPrintPos(90, 44);u8g.print(Ic);
  u8g.drawStr( 0, 24, "Current(Load):"); u8g.setPrintPos(90, 24);u8g.print(Im);u8g.drawStr( 120, 24, "A");
  u8g.drawStr( 0, 12, "Volt (V_Batt):"); u8g.setPrintPos(90,12);u8g.print(Vb);u8g.drawStr( 120, 12, "V");
 u8g.drawFrame(0,28,128,28);
  u8g.drawStr(3,39,"To STOP this TEST");
  u8g.drawStr(3, 51,"Manually, PRESS: S1");
  u8g.drawStr( 0, 64, "Runtime");
  u8g.setPrintPos(48,64);u8g.print(hr);
  u8g.drawStr( 60, 64, "h: ");
  u8g.setPrintPos(78,64);u8g.print(mint);
  u8g.drawStr( 90, 64, "m: ");
  u8g.setPrintPos(108,64);u8g.print(sec);
  u8g.drawStr( 120, 64, "s");

}


void draw3(void)
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawStr( 0, 10, "Set Current: PRESS S1" );
  u8g.drawStr( 0, 24, "Discharge");
  u8g.drawStr( 0, 34, "Current = ");
  
  u8g.setPrintPos(60,34);u8g.print(disc_I);
  u8g.drawStr( 84, 34, "mA");
  u8g.drawStr( 0, 40, "_____________________");
  u8g.drawStr( 0, 50, "After Setting Current");
  u8g.drawStr( 0, 60, "PRESS S2 to Proceed");
  
}


void draw4(void)
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawStr( 0, 10, "Set C-Rate: PRESS S1" );
  u8g.drawStr( 0, 28, "C-Rate: C/");
  u8g.setPrintPos(60,28);u8g.print(C);
  u8g.drawStr( 0, 60, "PRESS S2 to Proceed");
  
}


void draw5(void)
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawFrame(0,0,128,14);
  u8g.drawStr( 1, 11, "!NOW CONNECT BATTERY!" );
//  u8g.drawStr( 0, 16, "....................." );
  u8g.drawStr( 0, 26, "V_Batt(+) & V_Batt(-)");
  u8g.drawStr( 0, 38, "After Connecting Batt");
  u8g.drawStr( 0, 50, "PRESS S2 to Start");
  u8g.drawStr( 0, 62, "Discharging Test");
  
  
}

void draw7(void)
{
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawFrame(0,0,128,14);
  u8g.drawStr( 1, 11, " CONNECT RX/TX/Gnd to" );
//  u8g.drawStr( 0, 16, "....................." );
  u8g.drawStr( 0, 24, "Computer using a USB");
  u8g.drawStr( 0, 34, "to Serial Converter.");
  u8g.drawStr( 0, 44, "Start a Terminal");
  u8g.drawStr( 0, 54, "Programm,then PRESS");
  u8g.drawStr( 0, 64, "S2 to Copy EEPROM.");
  
  
}
  
}

void draw10(void)
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawFrame(0,0,128,14);
  u8g.drawStr( 1, 12, "Hopefully Previous" );
//
  u8g.drawStr( 1, 24, "TEST DATA COPYING" );
//  u8g.drawStr( 0, 16, "....................." );
  u8g.drawStr( 0, 36, "From EEPROM to Serial");
  u8g.drawStr( 0, 48, "In Progress.....");
  
  
}


void draw11(void)
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawStr( 0, 10, "STOP TEST/CONTINUE");
  u8g.drawStr( 0, 24, "PRESS S2 to STOP TEST");
 // u8g.drawStr( 0, 36, "LAST TEST'S DATA (EEPROM)");
  u8g.drawStr( 0, 44, "PRESS S1 to CONTINUE");
  u8g.drawStr( 0, 56, "RUNNING this TEST");
 
  
}



void draw12(void)
{
  u8g.setFont(u8g_font_6x10);
  u8g.drawFrame(0,0,128,14);
  u8g.drawStr( 18, 11, "! TEST STOPPED !" );
//
  u8g.drawStr( 18, 24, "( OR COMPLETED )" );
//  u8g.drawStr( 0, 16, "....................." );
  u8g.drawStr( 0, 36, "Disconnect Battery,");
  u8g.drawStr( 0, 48, "Then Restart Tester");
  u8g.drawStr( 0, 64, "Duration:");
  u8g.setPrintPos(60,64);u8g.print(hr);
  u8g.drawStr( 72, 64, "h: ");
  u8g.setPrintPos(90,64);u8g.print(mint);
  u8g.drawStr( 102, 64, "m ");
  
}

