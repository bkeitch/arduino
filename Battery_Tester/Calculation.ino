

// Measures ADC & Calculates Discharge Current : Range: 0-13.9 A in resolution of 25 mA

void Calc_I(void)
{int I_temp=0;
 for (int i=5;i>0;i--)
 { I_temp=I_temp+analogRead(A1);}
 int I_avg= I_temp/5;


  Im=(I_avg-I_base)*13.91/(1023-I_base);
  delay(1);
 if (Im<0.03){Im=0.0;}
}

 // Measures ADC and Calculates Battery Voltage
 // NOTE : Battery Voltage Measureing Range :10.10 V to 14.3 V 
 // Below or Above this range result is not accurate !

void Calc_V(void)
{

 int V_temp=0;
 for (int i=5;i>0;i--)
 { V_temp=V_temp+analogRead(A0);}
 int V_avg=V_temp/5;
 Vb=(V_avg-V_base)*0.004767+10.50;
  delay(1); 
}


