// Follwoing Function are for Controlling MCP4131 10K Diigital Potentiometer 
// over SPI bus

void init_SPI_digipot(void)
{

  pinMode (slaveSelectPin, OUTPUT);
  // initialize SPI:
  SPI.begin(); 
  for(int i=0;i<=65;i++)
  {
    pinMode(4,1);
    digitalPot_dec(); // wrt PB <> PW
    pinMode(4,0);  
    delay(10);
      
  }
  pot_pos=0;
}

// Resistence Increase Function (1 Step = about 75 Ohms)

void digitalPot_inc(void)
{
 digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x08);// the increment command
  digitalWrite(slaveSelectPin,HIGH); 
  pot_pos++;
}


// Resistence Decrease Function (1 Step = about 75 Ohms)

void digitalPot_dec(void)
{
 digitalWrite(slaveSelectPin,LOW);
  SPI.transfer(0x04);// the decrement command
  digitalWrite(slaveSelectPin,HIGH); 
  pot_pos--;
}

