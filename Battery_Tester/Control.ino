/*
Note :
Im is the measured current
disc_I is the expected discharge current set by user

*/

// Adjust Current function Adjusts the Discharge current equals to Set Value

void adjust_current(void)
{
  Calc_I();
  // set the current with in +/- 50 mA of expected
  if (Im<((disc_I+50.0)/1000.00))
  {
  digitalPot_inc();
  delay(50);
  }
  else if ((Im+0.05)>(disc_I/1000))
  {
  digitalPot_dec();
  delay(50);
  }
  
} 


// Sets current for the first time
void set_current(void)
{
  for(int k=0;k<=(disc_I/100);k++)
  {
    adjust_current();
    delay(5);
    
  }
}
// This Function takes necessary actions for stopping the test
// by Ramping Down the discharge current to 0A, stopping the Timer,


void stop_test(void)
{
    while(pot_pos>0)
    {
    digitalPot_dec();// slowly reduce the discharge currnet 
    digitalWrite(LED,1);
    delay(100);   
    digitalWrite(LED,0);
    }

   stopflag=1;
   update_time();
 
    while(1)
    {
    
    test_stopped();
    delay(50);
    }
}

// Confirms user request for manual stop test

void stop_check(void)
{
  S2=1; 
  S1=1;
  while(S1)
  {
  digitalWrite(LED,1);  
  manual_stop();
  if(S2==0)
  {
    stop_test();
    
  }
  }
  digitalWrite(LED,0);
   S1=1; 
}

// Updates Display during test with Battery Voltage, 
// Discharge Current, Test Run Time Info periodically 

void update_ui(void)
{
  
   if(sec!=oldsec)
 {
   update_time();
   update_disp();
 }
}
