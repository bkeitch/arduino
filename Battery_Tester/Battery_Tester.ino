/* Open Source Design
* Code written by Shahariar Hossain, IE DU, Batch 3
** This firmware is for Lead Acid Battery Capacity 
*** Development & Testing Device Hardware & Farmware Date : 2015,Sep-Dec 
------------------------------------------------------*/

#include "U8glib.h"
#include <SPI.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>

#define LED 4
// cheap chinese OLED(scl,sda,nothing,d/c,rst)
U8GLIB_SSD1306_128X64 u8g(8, 7, 9, 5,6); //real H/W connection 
//U8GLIB_SSD1306_128X64 u8g(5, 6, 9, 8,7);

// Float to hold Voltage and Current readings
float Vb=0.0;
//int Vb=0;
//float Ic=0.0;
float Im=0.0;

int I_base=462; // ADC value @ 0A current
int V_base=229;// @ 10.5 Volt

const uint8_t slaveSelectPin=10; // SPI bus chip select for mcp4131
uint8_t pot_pos =0; //digital potentiometer trimmer position

// Interrupt variables for Time keeping and User Input from press switch
volatile uint8_t sec=0;
volatile uint8_t S1=1;
volatile uint8_t S2=1;

// Timer variables 
uint8_t oldsec=0;
uint8_t mint=0;
uint8_t hr=0;
uint8_t j=0;

int disc_I=0;// initial discharge current value (upto 10000 mA or 10.0A in step of 100 mA or 0.1 A)
uint8_t C=05;// initial C rate

uint8_t eeprom_nth_data=0; // data index for EEPROM

// stopflag 1 means any stop condition occured : Vb<10.5 or Test runtime = C hrs or User Stopped
uint8_t stopflag =0; 


void setup(void) {
// u8g.setRot180();// flip display 
// 1.114 volt Internal Analog Ref on Atmega328p
// provides 10 bit ADC Resolution of 1.075 mV 
  analogReference(INTERNAL);
  pinMode(LED,OUTPUT); // Green LED for debugging Interrupts and EEPROM 

  pinMode(2, INPUT_PULLUP);
  attachInterrupt(0, SW1, FALLING);// Interrupt for Swithc 1

  pinMode(3, INPUT_PULLUP);
  attachInterrupt(1, SW2, FALLING);// Interrupt for Swithc 2
// Avr internal timer for timebase  
  init_timer1();    
// Sets Potentiometer to 0 ohms
  init_SPI_digipot();
  delay(100);
  init_screen();delay(5000); // Shows Initializarion screen for 5 sec      
  eeprom_or_test();
  user_input(); // take test configuration form user
  set_current();
}


void loop(void) 
{

// Calculates Current in Amps and Voltage in Volts 

Calc_I();
Calc_V();

// updates the screen with time,measured V & I
update_ui();
 
// adjusts current if changed on every 5 th minute
  if ((mint%5==0)&&(sec>56))
  {
    adjust_current(); // enable this function when everything is OK
    update_ui();
  }
 



//Stops test in one of the following 3 situations 
// Abort by user / Battery Voltage below 10.50 Volt / Test Runtime exceeds C hrs 

if((Vb<=10.5)||(hr==C))
{
  // make Vb 10.5 later
 stop_test();
}

// User Stop Request
if (S1==0)
{
 stop_check();
}

    
// save eeprom every 5 min
if((mint%5==0)&&(sec==0))
{
 save_eeprom();
 update_ui();
}


}

