// Saves to EEPROM

void save_eeprom(void)
{
  noInterrupts();
  digitalWrite(LED,HIGH); // turns on Green LED to indicate eeprom write begin
// size of Float variable is 4 byte 
//  int size_of_data=sizeof(Vb);
//  eeprom_write_block((const void*)&DATA,(void*)EEPROM_LOCATION,DATASIZE_in_Bytes);
//  eeprom_write_block((const void*)&Vb,(void*)(eeprom_nth_data*size_of_data),size_of_data);
    eeprom_write_block((const void*)&Vb,(void*)(eeprom_nth_data*4),4);
  
  eeprom_nth_data++;
  
  delay(240);
  digitalWrite(LED,LOW);
  delay(240);
  digitalWrite(LED,HIGH); 
  delay(240);
  digitalWrite(LED,LOW);
  delay(240);
  digitalWrite(LED,HIGH);
  sec++;
  interrupts();
  
}

// Read from EEPROM and export over serial<>USB to computer terminal program

void fetch_eeprom(void)
{
  Serial.begin(9600);
  Serial.println("");
  Serial.println("Battery Voltage form last test (Each reading with 5 minutes interval)");
 // int size_of_data=sizeof(Vb);

  for(int i=0;i<256;i++)
  {
  digitalWrite(LED,HIGH); // turns on Green LED to indicate eeprom write begin
   eeprom_nth_data=i;
  eeprom_read_block((void*)&Vb,(void*)(eeprom_nth_data*4),4);
 // eeprom_read_block((void*)&Vb,(void*)(eeprom_nth_data*size_of_data),size_of_data);
 // size_of_data=sizeof(Vb);
 // eeprom_nth_data=i;
  Serial.println(Vb);
  digitalWrite(LED,LOW); // turns on Green LED to indicate eeprom write begin
  delay(10);
  }
    Serial.println("Battery Voltage DATA form Previous Test, Transfer Completed !!!");
 Serial.println("-- Thank You --");
 
  S1=1;S2=1; // exit loop to Menu
}

