//int int_array[54] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int int_array[11] = {3,3,3,3,3,3,3,3,3,3,3};

String pin_str = "";

void setup() {
  Serial.begin(9600);
  for (int ii = 3;ii<11;ii++){
    pinMode(ii,INPUT);
  }
  Serial.print("Welcome\n\rPins are:\n\r");
}

void loop() {
  for (int ii = 3;ii<=10;ii++){
      int current = digitalRead(ii);
      
      if (int_array[ii]!=current) {
        Serial.print("Pin # ");
        Serial.print(ii-3);
        if(current==0) {
          Serial.println(" OFF");
        } else {
          Serial.println(" ON");
        }
      } 
      int_array[ii] = current;
  }
  delay(100);
}
