#include <WiFi.h>
#include <ESPmDNS.h>
#include <WebServer.h>

const char * ssid = "ONESTREAM-1325";
const char * password = "TADV715R";

WebServer server(8040);


const char* www_username = "admin";
const char* www_password = "benben257";
bool HTstatus = LOW;
bool HWstatus = LOW;
const int HWled = 12;
const int HTled = 13;
const int HWrelay = 16;
const int HTrelay = 17;
void WiFiEvent(WiFiEvent_t event)
{
    Serial.printf("[WiFi-event] event: %d\n", event);

    switch (event) {
        case ARDUINO_EVENT_WIFI_READY: 
            Serial.println("WiFi interface ready");
            break;
        case ARDUINO_EVENT_WIFI_SCAN_DONE:
            Serial.println("Completed scan for access points");
            break;
        case ARDUINO_EVENT_WIFI_STA_START:
            Serial.println("WiFi client started");
            break;
        case ARDUINO_EVENT_WIFI_STA_STOP:
            Serial.println("WiFi clients stopped");
            break;
        case ARDUINO_EVENT_WIFI_STA_CONNECTED:
            Serial.println("Connected to access point");
            break;
        case ARDUINO_EVENT_WIFI_STA_DISCONNECTED:
            Serial.println("Disconnected from WiFi access point");
            break;
        case ARDUINO_EVENT_WIFI_STA_AUTHMODE_CHANGE:
            Serial.println("Authentication mode of access point has changed");
            break;
        case ARDUINO_EVENT_WIFI_STA_GOT_IP:
            Serial.print("Obtained IP address: ");
            Serial.println(WiFi.localIP());
            break;
        case ARDUINO_EVENT_WIFI_STA_LOST_IP:
            Serial.println("Lost IP address and IP address is reset to 0");
            break;
        case ARDUINO_EVENT_WPS_ER_SUCCESS:
            Serial.println("WiFi Protected Setup (WPS): succeeded in enrollee mode");
            break;
        case ARDUINO_EVENT_WPS_ER_FAILED:
            Serial.println("WiFi Protected Setup (WPS): failed in enrollee mode");
            break;
        case ARDUINO_EVENT_WPS_ER_TIMEOUT:
            Serial.println("WiFi Protected Setup (WPS): timeout in enrollee mode");
            break;
        case ARDUINO_EVENT_WPS_ER_PIN:
            Serial.println("WiFi Protected Setup (WPS): pin code in enrollee mode");
            break;
        case ARDUINO_EVENT_WIFI_AP_START:
            Serial.println("WiFi access point started");
            break;
        case ARDUINO_EVENT_WIFI_AP_STOP:
            Serial.println("WiFi access point  stopped");
            break;
        case ARDUINO_EVENT_WIFI_AP_STACONNECTED:
            Serial.println("Client connected");
            break;
        case ARDUINO_EVENT_WIFI_AP_STADISCONNECTED:
            Serial.println("Client disconnected");
            break;
        case ARDUINO_EVENT_WIFI_AP_STAIPASSIGNED:
            Serial.println("Assigned IP address to client");
            break;
        case ARDUINO_EVENT_WIFI_AP_PROBEREQRECVED:
            Serial.println("Received probe request");
            break;
        case ARDUINO_EVENT_WIFI_AP_GOT_IP6:
            Serial.println("AP IPv6 is preferred");
            break;
        case ARDUINO_EVENT_WIFI_STA_GOT_IP6:
            Serial.println("STA IPv6 is preferred");
            break;
        case ARDUINO_EVENT_ETH_GOT_IP6:
            Serial.println("Ethernet IPv6 is preferred");
            break;
        case ARDUINO_EVENT_ETH_START:
            Serial.println("Ethernet started");
            break;
        case ARDUINO_EVENT_ETH_STOP:
            Serial.println("Ethernet stopped");
            break;
        case ARDUINO_EVENT_ETH_CONNECTED:
            Serial.println("Ethernet connected");
            break;
        case ARDUINO_EVENT_ETH_DISCONNECTED:
            Serial.println("Ethernet disconnected");
            break;
        case ARDUINO_EVENT_ETH_GOT_IP:
            Serial.println("Obtained IP address");
            break;
        default: break;
    }}

// WARNING: This function is called from a separate FreeRTOS task (thread)!
void WiFiGotIP(WiFiEvent_t event, WiFiEventInfo_t info)
{
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(IPAddress(info.got_ip.ip_info.ip.addr));
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }

  server.send(404, "text/plain", message);
}
void setup() {
  Serial.begin(115200);
  pinMode(HWled   , OUTPUT);
  pinMode(HTled   , OUTPUT);
  pinMode(HWrelay , OUTPUT);
  pinMode(HTrelay , OUTPUT);
  
//  pinMode(14, OUTPUT); // to boot with battery...
//  digitalWrite(14,HIGH);  // and/or power from 5v rail instead of USB

  WiFi.onEvent(WiFiEvent);
  WiFi.onEvent(WiFiGotIP, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_GOT_IP);
  WiFiEventId_t eventID = WiFi.onEvent([](WiFiEvent_t event, WiFiEventInfo_t info){
      Serial.print("WiFi lost connection. Reason: ");
      Serial.println(info.wifi_sta_disconnected.reason);
  }, WiFiEvent_t::ARDUINO_EVENT_WIFI_STA_DISCONNECTED);
  Serial.print("WiFi Event ID: ");
  Serial.println(eventID);  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi Connect Failed! Rebooting...");
    delay(1000);
    ESP.restart();
  }



  Serial.print("Open http://");
  Serial.print(WiFi.localIP());
  Serial.println(":8040/ in your browser to see it working");


  server.on("/", handle_OnConnect);
  server.on("/hton", handle_HTon);
  server.on("/htoff", handle_HToff);
  server.on("/hwon", handle_HWon);
  server.on("/hwoff", handle_HWoff);
  server.onNotFound(handleNotFound);

  server.begin();

}


void handle_OnConnect() {
    if (!server.authenticate(www_username, www_password)) {
      return server.requestAuthentication();
    }
  Serial.println("Connected");
  server.send(200, "text/html", SendHTML(HTstatus,HWstatus)); 
}

void handle_HTon() {
    if (!server.authenticate(www_username, www_password)) {
      return server.requestAuthentication();
    }
  HTstatus = HIGH;
  Serial.println("HT Status: ON");
  server.send(200, "text/html", SendHTML(true,HWstatus)); 
}

void handle_HToff() {
    if (!server.authenticate(www_username, www_password)) {
      return server.requestAuthentication();
    }
  HTstatus = LOW;
  Serial.println("HT Status: OFF");
  server.send(200, "text/html", SendHTML(false,HWstatus)); 
}

void handle_HWon() {
    if (!server.authenticate(www_username, www_password)) {
      return server.requestAuthentication();
    }
  HWstatus = HIGH;
  Serial.println("HW Status: ON");
  server.send(200, "text/html", SendHTML(HTstatus,true)); 
}

void handle_HWoff() {
    if (!server.authenticate(www_username, www_password)) {
      return server.requestAuthentication();
    }
  HWstatus = LOW;
  Serial.println("HW Status: OFF");
  server.send(200, "text/html", SendHTML(HTstatus,false)); 
}


String SendHTML(uint8_t HTstat,uint8_t HWstat){
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr +="<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr +="<title>Heating Control</title>\n";
  ptr +="<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: center;}\n";
  ptr +="body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 50px;}\n";
  ptr +=".button {display: block;width: 80px;background-color: #3498db;border: none;color: white;padding: 13px 30px;text-decoration: none;font-size: 25px;margin: 0px auto 35px;cursor: pointer;border-radius: 4px;}\n";
  ptr +=".button-on {background-color: #3498db;}\n";
  ptr +=".button-on:active {background-color: #2980b9;}\n";
  ptr +=".button-off {background-color: #34495e;}\n";
  ptr +=".button-off:active {background-color: #2c3e50;}\n";
  ptr +="p {font-size: 14px;color: #888;margin-bottom: 10px;}\n";
  ptr +="</style>\n";
  ptr +="</head>\n";
  ptr +="<body>\n";
  ptr +="<h1>BCK Heating System</h1>\n";
    ptr +="<h3>WiFi Station(STA) Mode</h3>\n";
  
   if(HTstat)
  {ptr +="<p>Heating Status: ON</p><a class=\"button button-off\" href=\"/htoff\">OFF</a>\n";}
  else
  {ptr +="<p>Heating Status: OFF</p><a class=\"button button-on\" href=\"/hton\">ON</a>\n";}

  if(HWstat)
  {ptr +="<p>Hotwater Status: ON</p><a class=\"button button-off\" href=\"/hwoff\">OFF</a>\n";}
  else
  {ptr +="<p>Hotwater Status: OFF</p><a class=\"button button-on\" href=\"/hwon\">ON</a>\n";}

  ptr +="</body>\n";
  ptr +="</html>\n";
  return ptr;
}

void loop() {


  server.handleClient();
  if(HTstatus)
  {digitalWrite(HTled, HIGH);digitalWrite(HTrelay, HIGH);}
  else
  {digitalWrite(HTled, LOW);digitalWrite(HTrelay, LOW);}
  
  if(HWstatus)
  {digitalWrite(HWled, HIGH);digitalWrite(HWrelay, HIGH);}
  else
  {digitalWrite(HWled, LOW);digitalWrite(HWrelay, LOW);}
  delay(200);//allow the cpu to switch to other tasks
  if (WiFi.status() != WL_CONNECTED) {
    delay(1000);//allow the cpu to switch to other tasks
    Serial.println("Restarting WiFi");
    delay(500);//allow the cpu to switch to other tasks
      
    WiFi.reconnect();
  }
}
