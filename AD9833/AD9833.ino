#include <MD_AD9833.h>    
#include "Vrekrer_scpi_parser.h"
#define FNC_PIN 10

//--------------- Control AD9833 DDS ----------------
// SCK and MOSI must be connected to CLK and DAT pins on the AD9833 using SPI defaults
// System is 5V on Uno. 
// Defaults to 25MHz internal reference frequency
MD_AD9833 gen(FNC_PIN); 
SCPI_Parser dds;

void setup() {
	
	dds.RegisterCommand(F("*IDN?"), &Identify);
	dds.RegisterCommand(F("*RST"), &Reset);
	dds.SetCommandTreeBase(F("DDS"));
	dds.RegisterCommand(F(":FREQuency"), &SetFrequency);
	dds.RegisterCommand(F(":FREQuency?"), &GetFrequency);
	dds.RegisterCommand(F(":PHase"), &SetPhase);
	dds.RegisterCommand(F(":PHase?"), &GetPhase);
	dds.RegisterCommand(F(":FSelect"), &FreqSwitch);
	dds.RegisterCommand(F(":PSelect"), &PhaseSwitch);
	dds.RegisterCommand(F(":WAVEtype"), &SetWaveType);
	dds.RegisterCommand(F(":WAVEtype?"), &GetWaveType);
	dds.RegisterCommand(F(":OUTPut"), &SetOutputSource);
	dds.RegisterCommand(F(":OUTPut?"), &GetOutputSource);
	
	Serial.begin(9600);
	Serial.print("\n\n**AD9833**\n");
	gen.begin();
	gen.setMode(MD_AD9833::MODE_OFF);
}

void loop() {
	dds.ProcessInput(Serial, "\n");
}
/* 
 * *IDN? Suggested return string should be in the following format:
 * "<vendor>,<model>,<serial number>,<firmware>"
*/
void Identify(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	interface.println(F("RedWaveLabs,DDS,AD9833,#00," VREKRER_SCPI_VERSION));
}
void FreqSwitch(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	if (parameters.Size() > 0) {
		int channel = constrain(String(parameters[0]).toInt(), 0, 1);
		if(channel == 0) gen.setActiveFrequency(MD_AD9833::CHAN_0);
		if(channel == 1) gen.setActiveFrequency(MD_AD9833::CHAN_1);
	}
}
void PhaseSwitch(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	if (parameters.Size() > 0) {
		int channel = constrain(String(parameters[0]).toInt(), 0, 1);
		if(channel == 0) gen.setActivePhase(MD_AD9833::CHAN_0);
		if(channel == 1) gen.setActivePhase(MD_AD9833::CHAN_1);
	}
}
/*
 * Frequency - 0 to 12.5 MHz
*/
void SetFrequency(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	if (parameters.Size() > 1) {
		int channel = constrain(String(parameters[0]).toInt(), 0, 1);
		int frequency = String(parameters[1]).toInt();
		if (frequency < 0 or frequency > 12500000L) frequency =0;
		if(channel == 0)
		gen.setFrequency(MD_AD9833::CHAN_0,frequency);
		if(channel == 1)
		gen.setFrequency(MD_AD9833::CHAN_1,frequency);
	}
}

void GetFrequency(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	int channel = 0;
	long frequency = 0;
	if (parameters.Size() > 0) {
		channel = constrain(String(parameters[0]).toInt(), 0, 1);
		if(channel == 0) frequency = gen.getFrequency(MD_AD9833::CHAN_0);
		if(channel == 1) frequency = gen.getFrequency(MD_AD9833::CHAN_1);
	}
	interface.println(String(frequency, DEC));
}

/* Phase - 0 to 360 degress (this is only useful if it is 'relative' to
 * some other signal
 * such as the phase difference between REG0 and REG1).
*/
void SetPhase(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	
	if (parameters.Size() > 1) {
		int channel = constrain(String(parameters[0]).toInt(), 0, 1);
		int phase = String(parameters[1]).toInt();
		if (phase < 0 or phase > 12500000L) phase = 0;
		if(channel == 0) gen.setPhase(MD_AD9833::CHAN_0, phase);
		if(channel == 1) gen.setPhase(MD_AD9833::CHAN_1, phase);
	}
	
}

void GetPhase(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	int channel = 0;
	int phase = 0;
	if (parameters.Size() > 0) {
		channel = constrain(String(parameters[0]).toInt(), 0, 1);
		if(channel == 0) phase = gen.getPhase(MD_AD9833::CHAN_0);
		if(channel == 1) phase = gen.getPhase(MD_AD9833::CHAN_1);
	}
	interface.println(String(phase, DEC));
}
void Reset(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	gen.reset();
}
/*
 * Signal type - SINE_WAVE, TRIANGLE_WAVE, SQUARE_WAVE, and HALF_SQUARE_WAVE
*/
void SetWaveType(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	MD_AD9833::mode_t mode = gen.getMode();
	
	if(String(parameters[0]).equalsIgnoreCase("OFF"))
	mode = MD_AD9833::MODE_OFF;
	if(String(parameters[0]).equalsIgnoreCase("SQUARE"))
	mode = MD_AD9833::MODE_SQUARE1;
	if(String(parameters[0]).equalsIgnoreCase("HALF"))
	mode = MD_AD9833::MODE_SQUARE2;
	if(String(parameters[0]).equalsIgnoreCase("TRIANGLE"))
	mode = MD_AD9833::MODE_TRIANGLE;
	if(String(parameters[0]).equalsIgnoreCase("SIN"))
	mode = MD_AD9833::MODE_SINE;
	
	gen.setMode(mode);
	
}
void GetWaveType(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	MD_AD9833::mode_t mode = gen.getMode();
	switch (mode) {
		case MD_AD9833::MODE_OFF:
		interface.println("OFF");
		break;
		case MD_AD9833::MODE_SQUARE1:
		interface.println("SQUARE");
		break;
		case MD_AD9833::MODE_SQUARE2:
		interface.println("HALF");
		break;
		case MD_AD9833::MODE_TRIANGLE:
		interface.println("TRIANGLE");
		break;
		case MD_AD9833::MODE_SINE:
		interface.println("SINE");
		break;
	}
}
// There are two register sets, CHAN_0 and CHAN_1.

void SetOutputSource(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	int channel = 0;
	if (parameters.Size() > 0) {
		channel = constrain(String(parameters[0]).toInt(), 0, 1);
		if(channel == 0) gen.setActiveFrequency(MD_AD9833::CHAN_0);
		if(channel == 1) gen.setActiveFrequency(MD_AD9833::CHAN_1);
	}
}
void GetOutputSource(SCPI_C commands, SCPI_P parameters, Stream& interface) {
	MD_AD9833::channel_t channel = gen.getActiveFrequency();
	interface.println(String(channel, DEC));
}

