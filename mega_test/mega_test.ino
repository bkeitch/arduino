/**
   MEGA Power On Self Test

  BCK 2024

*/

#include "x_data.h"
#include <DS3232RTC.h>  //RTC Library https://github.com/JChristensen/DS3232RTC
static constexpr int timeout = 2000;
static constexpr int time_interval = 5;
static constexpr int months[] = {
  281,
  269,
  288,
  291,
  295,
  301,
  299,
  285,
  296,
  294,
  307,
  268
};

//Global objects:

x_data data;

//BME280I2C bme; // Default : forced mode, standby time = 1000 ms
// Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,

//ExternalEEPROM EEPROM_1;
DS3232RTC RTC;

void displayTime(time_t t) {
  char cbuffer[20];
  sprintf(cbuffer, "%04u-%02u-%02u %02u:%02u:%02u",
          year(t), month(t),  day(t),  hour(t),  minute(t),  second(t)
         );
  Serial.println(cbuffer);
}
/**
   Set RTC from individual components
*/
void setRTCTime(int h, int m, int s, int d, int mn, int y, bool first)	{
  tmElements_t new_time;
  new_time.Year   = y;
  new_time.Month  = mn;
  new_time.Day    = d;
  new_time.Hour   = h;
  new_time.Minute = m;
  new_time.Second = s;
  time_t t_cp = makeTime(new_time);
  time_t t_rtc = RTC.get();
#ifdef DEBUG_RTC
  Serial.println(F("Updated Time: "));
  displayTime(t_cp);
#endif

  if (first || (0 == data.updateGPSOffset(t_rtc, t_cp))) {
    RTC.set(t_cp);
  #ifdef DEBUG_RTC
    Serial.println(F("RTC Time: "));
    displayTime(t_rtc);
  #endif
  } else {
#ifdef DEBUG_RTC
    Serial.println(F("RTC not changed"));
#endif
  }
}
/**
   Initialises the RTC on first power-up with compile time. This assumes
   the RTC has not been programmed previously (e.g. power loss)
   it also  initialises the alarms to known values, clears the alarm flags, clears the alarm interrupt flags
   and configures the INT/SQW pin for "interrupt" operation (disable square wave output)
*/
void initialiseRTC() {
  int Hour, Minute, Second, Day, Month, Year;

  char time_c[] = __TIME__;
  Second = atoi(time_c + 6);
  *(time_c + 5) = 0;
  Minute = atoi(time_c + 3);
  *(time_c + 2) = 0;
  Hour = atoi(time_c);

  char temp[] = __DATE__;
  int i;
  int sum_out;
  Year = atoi(temp + 9); //ref 1970
  *(temp + 6) = 0;
  Day = atoi(temp + 4);
  *(temp + 3) = 0;
  Month = 0;
  for (i = 0; i < 12; i++) {
    sum_out = (temp[0] + temp[1] + temp[2]);
    if (months[i] == sum_out) {
      Month = i + 1;
      break;
    }
  }
  setRTCTime(Hour, Minute, Second, Day, Month, Year + 30, true);
  time_t t_rtc = RTC.get();
  //alarm 2 not used
  RTC.setAlarm(DS3232RTC::ALM2_MATCH_DATE, 0, 0, 0, 1);
  RTC.alarm(DS3232RTC::ALARM_2);
  RTC.alarmInterrupt(DS3232RTC::ALARM_2, false);
  RTC.squareWave(DS3232RTC::SQWAVE_NONE);
  // Setting alarm 1 to wake up
#ifdef USE_MINUTES
  RTC.setAlarm(DS3232RTC::ALM1_MATCH_MINUTES, 0, (minute(t_rtc) + time_interval) % 60, 0, 0);
#else
  RTC.setAlarm(DS3232RTC::ALM1_MATCH_SECONDS, (second(t_rtc) + time_interval) % 60, 0, 0, 0);
#endif
  // clear the alarm flag
  RTC.alarm(DS3232RTC::ALARM_1);
  // enable interrupt output for Alarm 1
  RTC.alarmInterrupt(DS3232RTC::ALARM_1, true);
 
}
int freeRam () {
  extern int __heap_start, *__brkval;
  int v;
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}

  int error =0;
void setup() {

  setupPins();
  pinMode(13, OUTPUT);
  Serial.begin(9600);
  //Sensors on I2C
  // set the RTC from the compile date
  initialiseRTC();
}
void loop() {
  Serial.println(F("RAM: "));
  Serial.flush();
  Serial.println(freeRam());
  Serial.println(error++);
  
  //readMemory();

  time_t t_rtc = RTC.get();
  displayTime(t_rtc);
  //updateXData(false);
  digitalWrite(13, HIGH);
  delay(1000);
  digitalWrite(13, LOW);
  delay(1000);

}
