const int tempPin=A4;         // analog pin for measuring thermistor voltage

void setup() {
  // initialize the serial communication:
  Serial.begin(9600);
}

void loop() {
  // send the value of analog input 0:
  int V = readSteadyVoltage(tempPin);
  int Vmap = map(V, 400, 900, 0, 12);
  
  int T = (V -450 )*10;
  Serial.print(Vmap);
  Serial.print(",");
  Serial.println(floor_log2(T));
  // wait a bit for the analog-to-digital converter to stabilize after the last
  // reading:
  delay(20);
}
int readSteadyVoltage(int sensorPin) {
  float sensorValue = 0;
  for (int i = 0; i < 10 ; i++) {
    sensorValue += analogRead(sensorPin);
    delay(2);
  }
  return(int(sensorValue/10.0));
}
unsigned int
ones16(register unsigned int x)
{ 
  x -= ((x >> 1) & 0x5555);
  x = (((x >> 2) & 0x3333) + (x & 0x3333));
  x = (((x >> 4) + x) & 0x0f0f);
  x += (x >> 8);
  return(x & 0x003f);
}
unsigned int
floor_log2(register unsigned int x)
{
  x |= (x >> 1);
  x |= (x >> 2);
  x |= (x >> 4);
  x |= (x >> 8);
  return(ones16(x >> 1));

}
