#include <RtcDateTime.h>
#include <RtcDS1307.h>
#include <Wire.h>
#include <Time.h>
#include <TimeLib.h>
const char *monthName[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

tmElements_t tm;

/*
Weather station. Plot barometric trend as bar graph. Stores data in EEPROM and uses RTC. Data can be retrieved using serial port.
BCK 2017

Hardware: I2C bus to DS1307 RTC and 24A512 EEPROM 
Sensor: MPX4115 at 5V

RTC library is RTC by Mankuna library
openGLCD installed from Zip.

 */

const int sensorPin = A0;  // Analog input pin that the pressure sensor is attached to


// initialize the RTC

RtcDS1307<TwoWire> rtc(Wire);
const int ledPin[] = {5,12,11,10, 9,8,7,6, 13};
const short FWD=2;
void displayMinutes(byte numToShow)
{
  for (byte i = 0; i < 4 ; i++) 
  {
    digitalWrite(ledPin[i], bitRead(numToShow, i));
  }
}
void displayHours(byte numToShow)
{
  for (byte i = 0; i < 4 ; i++) 
  {
    digitalWrite(ledPin[i+4], bitRead(numToShow, i));
  }
}
void setup() {
  bool parse=false;

 
   for (int i =0;i<9;i++)
  {
    pinMode(ledPin[i], OUTPUT);
  }
  pinMode(FWD, INPUT);
  Wire.begin();
  //Serial.begin(115200);
  // Initialize the GLCD 
  
  //rtc.begin();
  if (! rtc.GetIsRunning()) {
    //Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.SetIsRunning(true);
  } 
  rtc.SetSquareWavePin(DS1307SquareWaveOut_Low);
   // get the date and time the compiler was run
  if (getDate(__DATE__) && getTime(__TIME__)) 
  {
    parse = true;
    // and configure the RTC with this info
    RtcDateTime nowtime = RtcDateTime(tm.Year, 
      tm.Month,
      tm.Day,
      tm.Hour, 
      tm.Minute, 
      tm.Second);
    rtc.SetDateTime(nowtime);
  }
}



/*
 *
 */
void loop() {
  // read the analog in value:
  RtcDateTime now = rtc.GetDateTime();
  

  //wrap when memory full 
  
  if(now.Minute()%5 == 0) //only every 5 minutes
  {
    displayMinutes((byte) (now.Minute()/5));
  }
  
  
  displayHours((byte) (now.Hour()));

  
  
  
  delay(500); //delay 1 sec before redoing loop.

}


bool getTime(const char *str)
{
  int Hour, Min, Sec;

  if (sscanf(str, "%d:%d:%d", &Hour, &Min, &Sec) != 3) return false;
  tm.Hour = Hour;
  tm.Minute = Min;
  tm.Second = Sec;
  return true;
}

bool getDate(const char *str)
{
  char Month[12];
  int Day, Year;
  uint8_t monthIndex;

  if (sscanf(str, "%s %d %d", Month, &Day, &Year) != 3) return false;
  for (monthIndex = 0; monthIndex < 12; monthIndex++) {
    if (strcmp(Month, monthName[monthIndex]) == 0) break;
  }
  if (monthIndex >= 12) return false;
  tm.Day = Day;
  tm.Month = monthIndex + 1;
  tm.Year = CalendarYrToTm(Year);
  return true;
}







