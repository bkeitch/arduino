const int ledPin[] = {10,11,12,5, 13};
const short FWD=2;
void setup()
{
  for (int i =0;i<5;i++)
  {
    pinMode(ledPin[i], OUTPUT);
  }
  pinMode(FWD, INPUT);
}

void loop()
{
  for (byte hours =0;hours<60; hours++)
  {
    for (byte minutes =0;minutes<60; minutes++)
    {
//      for (byte seconds =0;seconds<60; seconds++)
//    
//      {
        digitalWrite(13, !digitalRead(13)); //flash seconds
        if(digitalRead(FWD) == HIGH) {
          delay(100);
        } else {
          delay(10); //FAST FORWARD TO SET THE TIME
//          seconds +=30;
        }
//      }
     
      if(minutes%5 == 0) //only every 5 minutes
      {
        displayMinutes(minutes/5);
      }
    }
    
    displayHours(hours);
  }
}

void displayMinutes(byte numToShow)
{
  for (byte i = 0; i < 4 ; i++) 
  {
    digitalWrite(ledPin[i], bitRead(numToShow, i));
  }
}
void displayHours(byte numToShow)
{
  for (byte i = 0; i < 4 ; i++) 
  {
    digitalWrite(ledPin[i+4], bitRead(numToShow, i));
  }
}


