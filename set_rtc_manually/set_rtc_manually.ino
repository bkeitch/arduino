/*
Set RTC manually

 */

#include <Wire.h>

#define DS3231_I2C_ADDRESS 0x68

byte numbers[6];
int index;
int i;

byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}

byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte
  dayOfMonth, byte month, byte year)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS+5);
 //05h Century 0 0 10 Month Month Century 01–12 + Century
  Wire.write(128+32); //20th century hard coded
  Wire.endTransmission();
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0);
  Wire.write(decToBcd(second)); 
  Wire.write(decToBcd(minute));
  Wire.write(decToBcd(hour)); 
  Wire.write(decToBcd(dayOfWeek));  
  Wire.write(decToBcd(dayOfMonth)); 
  Wire.write(decToBcd(month)); 
  Wire.write(decToBcd(year)); 
  Wire.endTransmission();
}
void readDS3231time(byte *second,byte *minute,byte *hour,byte *dayOfWeek,
  byte *dayOfMonth,byte *month,byte *year)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); 
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
}
void displayTime()
{
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
  &year);
  
  Serial.print(hour, DEC);
 
  Serial.print("h");
  if (minute<10)
  {
    Serial.print("0");
  }
  Serial.print(minute, DEC);
  Serial.print("min");
  if (second<10)
  {
    Serial.print("0");
  }
  Serial.print(second, DEC);
  Serial.print("sec  ");
  Serial.print(dayOfMonth, DEC);
  Serial.print("/");
  Serial.print(month, DEC);
  Serial.print("/");
  Serial.println(year, DEC);
  //Serial.print(" ");
}

void setup()
{
  i = 0;
  Wire.begin();
  index = 0;
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  Serial.println("second, minute, hour, (dayOfWeek), dayOfMonth, month, year");
  displayTime();
  
  
}
char commandLetter;  // the delineator / command chooser
char numStr[4];      // the number characters and null
long valuel;          // the number stored as a long integer

void loop()
{

        

 while( Serial.available())
  {
    char ch = Serial.read();
    
    //if ( ! isalpha( ch )) {
      // dump until a letter is found or nothing remains
      
    if(i <  3 && ch != '\r' && ch != '\n'){
      numStr[i++] = ch; // add the ASCII character to the string;
      
    } else {
   
    
      //terminate the string with a null prevents atol reading too far
      numStr[i] = '\0';
      
      //read character array until non-number, convert to long int
    valuel = atol(numStr);
    Serial.println(numStr);
    numbers[index++] = (byte)valuel;
    i=0;    
    }
  }


  if(index == 5) 
    setDS3231time( numbers[0], numbers[1], numbers[2] ,1 , numbers[3]  , numbers[4] , numbers[5]);
  if(index > 5) {  
    displayTime();
    delay(5000);
  }
 
}
