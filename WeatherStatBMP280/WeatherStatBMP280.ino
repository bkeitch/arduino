#include <Adafruit_BMP280.h>
#include <openGLCD.h>
#include <Wire.h>

#define DS3231_I2C_ADDRESS 0x68

Adafruit_BMP280 bmp; // I2C
/*
Weather station. Plot barometric trend as bar graph. Stores data in array
on Mega and uses RTC. 
BCK 2019

Hardware: I2C bus to DS3231 RTC and BMP280 on I2C 5V 
Sensor: MPX4115 at 5V

openGLCD installed from Zip.

 */

const int NUM_BARS = 34;
const bool DEBUG = false;
double pressures[NUM_BARS]; //array to store pressures
int addr = 0; //array index
byte oldMinute; //initiated in startup()
//byte count = 0;
//double TotalPressure =0.0;
//double AveragePressure = 0.0;
/*
 * Plot histogram of data in bottom part of screen
 * 
 */
void plotDataTest() { 
  unsigned int currentValue = 1013;
  unsigned int minval = currentValue -25;
  unsigned int maxval = currentValue +25;
  unsigned int startVal = 1000;
  for (byte i=0; i<NUM_BARS; i++) {
    unsigned int outputValue = startVal++; //pressure stored in hPa as double
    GLCD.DrawVBarGraph(GLCD.Left+1+(i*4), GLCD.Bottom-1, 5, -(GLCD.Height-2*10), 0, minval,maxval, outputValue);
  }
}
void shiftPressures() {
  for (unsigned int i =1 ; i <= NUM_BARS; i++) {
   pressures[i-1] = pressures[i]; //array to store pressures
  }

}
byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}

byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte
dayOfMonth, byte month, byte year)
{
 
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0);
  Wire.write(decToBcd(second)); 
  Wire.write(decToBcd(minute));
  Wire.write(decToBcd(hour)); 
  Wire.write(decToBcd(dayOfWeek)); 
  Wire.write(decToBcd(dayOfMonth)); 
  Wire.write(decToBcd(month)); 
  Wire.write(decToBcd(year)); 
  Wire.endTransmission();
}
void readDS3231time(byte *second,
byte *minute,
byte *hour,
byte *dayOfWeek,
byte *dayOfMonth,
byte *month,
byte *year)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); 
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  *dayOfWeek = bcdToDec(Wire.read());
  *dayOfMonth = bcdToDec(Wire.read());
  *month = bcdToDec(Wire.read());
  *year = bcdToDec(Wire.read());
}
void displayTime()
{
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
  &year);
  
  Serial.print(hour, DEC);
 
  Serial.print("h");
  if (minute<10)
  {
    Serial.print("0");
  }
  Serial.print(minute, DEC);
  Serial.print("min");
  if (second<10)
  {
    Serial.print("0");
  }
  Serial.print(second, DEC);
  Serial.print("sec  ");
  Serial.print(dayOfMonth, DEC);
  Serial.print("/");
  Serial.print(month, DEC);
  Serial.print("/");
  Serial.print(year, DEC);
  Serial.print(" ");
}
void setup() {
  Wire.begin();
  Serial.begin(9600);
  Serial.println(F("Weather Station"));

  if (!bmp.begin()) {
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }

  /* Default settings from datasheet. */
  bmp.setSampling(Adafruit_BMP280::MODE_NORMAL,     /* Operating Mode. */
                  Adafruit_BMP280::SAMPLING_X2,     /* Temp. oversampling */
                  Adafruit_BMP280::SAMPLING_X16,    /* Pressure oversampling */
                  Adafruit_BMP280::FILTER_X16,      /* Filtering. */
                  Adafruit_BMP280::STANDBY_MS_500); /* Standby time. */ 
  // Initialize the GLCD 
  GLCD.Init();

  // Select the font for the default text area
  GLCD.SelectFont(System5x7);

  GLCD.print(F("Pressure  Time"));

  displayTime();
  //initalise array at low pressure
  for (byte i=0; i<NUM_BARS; i++) {
    pressures[i] = 1000.0;
  }
  plotData(34);
  delay(1000);
  readDS3231time(NULL, &oldMinute, NULL, NULL, NULL, NULL, NULL);
  plotDataTest();
  //delay(5000);
}

/*
 * Format time for displaying on LCD screen.
 */
void showTime() {
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
  &year);
  if(hour < 10)
     GLCD.print(' ');
  GLCD.print(hour);
  GLCD.print(':');
  if(minute < 10)
     GLCD.print('0');
  GLCD.print(minute);
  GLCD.print(':');
  if(second < 10)
     GLCD.print('0');
  GLCD.print(second);
}
/*
 * Temperature sensor BMP board on I2C bus
 */
void displayTemp() {
  //GLCD.CursorTo(0, 2);
  
    
}
/*
 * Plot histogram of data in bottom part of screen
 * 
 */
void plotData(unsigned int currentIndex) { 
  unsigned int currentValue = int(2.0*pressures[currentIndex]); //2* gives accuracy to 0.5hPa
  unsigned int minval = currentValue -25;
  unsigned int maxval = currentValue +25; // 64 screen - 14 text = 50 pixels = 1 / hPa
  if(DEBUG) Serial.print(currentValue);  Serial.println("**");
  
  for (byte i=0; i<NUM_BARS; i++) {
    unsigned int outputValue = int(2.0*pressures[i]); //pressure stored in hPa as double 2* gives accuracy to 0.5hPa
    if(outputValue > maxval) outputValue = maxval;
    if(outputValue < minval) outputValue = minval;
    GLCD.DrawVBarGraph(GLCD.Left+1+(i*4), GLCD.Bottom-1, 5, -(GLCD.Height-2*10), 0, minval,maxval, outputValue);


  }
}
void outputArray() {
  for (byte i=0; i<NUM_BARS; i++) {
    unsigned int outputValue = int(2*pressures[i]); //pressure stored in hPa as double 2* gives accuracy to 0.5hPa
  
    Serial.print(outputValue); Serial.print(";"); 
  }
  Serial.println();
  
}
/*
 * Reads pressure values every second. Takes average over 10 minutes
 * Plots graph every 10 minutes of average value.
 */
void loop() {
  double pressure = bmp.readPressure();
  //TotalPressure += pressure/100.0;
  byte second, minute, hour, dayOfWeek, dayOfMonth, month, year;
  
  readDS3231time(&second, &minute, &hour, &dayOfWeek, &dayOfMonth, &month,
  &year);
  
  //store data in array every 10 mins and replot graph
  if((minute*60+second - oldMinute*60)%600==0) {
    
    if(DEBUG) outputArray();
    //AveragePressure= TotalPressure/count;
    if(addr == 34) {
      shiftPressures();
    }
    pressures[addr] =  pressure/100.0 ; //AveragePressure;
    //plot last points
    plotData(addr);
    //wrap when array full 
    if(addr < 34 ) //34 doubles in array
      addr++;
    else
      addr = 0;
    displayTime();
    if(DEBUG)  Serial.print(pressure/100.0);    Serial.println(F(" hPa"));    Serial.print(bmp.readTemperature());    Serial.println(F(" C"));
    //TotalPressure = 0; //reset average
    if(DEBUG) Serial.print(oldMinute); Serial.print(" : "); Serial.println(minute);
    oldMinute = minute;
  }
    
  //Display current time, temp and pressure

  GLCD.CursorTo(15,0);
  GLCD.print(bmp.readTemperature());
  //GLCD.print((char)223); //degree symbol, not supported
  GLCD.print(F("C"));
  
  GLCD.CursorTo(0, 1);
  if(pressure < 100000) //in Pa
    GLCD.print(F(" "));
  GLCD.print(pressure/100.0);
  GLCD.print(F("hPa "));
  
  
  showTime();
  delay(1000); //delay 1 sec before redoing loop.
  //count++;
}
