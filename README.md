Arduino Projects
================

Collection of Arduino projects.

Weather Station
---------------

Reads and stores barometric data into EEPROM with RTC time-stamps.

- (C) BCK 2015
