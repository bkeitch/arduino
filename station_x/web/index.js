// Read MessagePack data from stdin and write JSON data to stdout.

const msgpack = require('msgpack');

const dgram = require('dgram');
const client = dgram.createSocket('udp4');
//var SerialPort = require("serialport");				// include the serialport library
const express = require('express');
const app = express();
const server = require('http').createServer(app); // start an HTTP server
const io = require('socket.io').listen(server); // filter the server using socket.io
//const Readline = require('@serialport/parser-readline');
//var portName = process.argv[2];						// third word of the command line should be serial port name
//console.log("opening serial port: " + portName);	// print out the port you're listening on

server.listen(8080); // listen for incoming requests on the server
console.log("Listening for new clients on port 8080");
var connected = false;

// open the serial port. Change the name to the name of your port, just like in Processing and Arduino:
//var myPort = new SerialPort(portName);
// look for return and newline at the end of each data packet:

//const parser = myPort.pipe(new Readline({ delimiter: '\r\n' }));


// respond to web GET requests with the index.html page:
app.get('/', function(request, response) {
    response.sendfile(__dirname + '/index.html');
});

/*
// listen for new socket.io connections:
io.sockets.on('connection', function(socket) {
    // if the client connects:
    if (!connected) {
        // clear out any old data from the serial bufffer:
        myPort.flush();
        // send a byte to the serial port to ask for data:
        myPort.write('c');
        console.log('user connected');
        connected = true;
    }

    // if the client disconnects:
    socket.on('disconnect', function() {
        myPort.write('x');
        console.log('user disconnected');
        connected = false;
    });

    // listen for new serial data:  
    parser.on('data', function(data) {
        // Convert the string into a JSON object:
        //var serialData = JSON.parse(data);
        // for debugging, you should see this in the terminal window:
        //var out = 	 base64url.decode(data)	;
        const out = Buffer.from(data, 'base64');
        console.log(data);
        // send a serial event to the web client with the data:

        var buf = new Buffer.from(out);
        console.log(buf.toJSON());
        var oo = msgpack.unpack(buf);
        console.log(oo.toString());
        console.log(JSON.stringify(msgpack.unpack(buf)));
        socket.emit('serialEvent', JSON.stringify(oo));
    })

});

*/
client.on('error', (err) => {
    console.log(`server error:\n${err.stack}`);
    server.close();
});


client.on('listening', () => {
    const address = server.address();
    console.log(`server listening ${address.address}:${address.port}`);
});
var data;
client.bind(3333);
//var s = new net.Stream(0);
//s.addListener('data', function(b) {
client.on('message', (msg, rinfo) => {
    //console.log(`server got: ${msg} from ${rinfo.address}:${rinfo.port}`);
    var keepalive = Buffer.from("KEEPALIVE");
    const debug = Buffer.from(msg, 'base64');
    let text = debug.toString('ascii');
    console.log(msg.length);
    console.log(text);
if (0 != msg.compare(keepalive, 0, 9 , 0, 9)) {
	try {
	    data = msgpack.unpack(out);
	    console.log(JSON.stringify(data));
	} catch  {}
} else {
	console.log("Keep alive");
	console.log(msg.readInt16BE(9));
}
});
app.get('/data/', function(request, response) {
    response.json(data);
});

/*
var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://192.168.0.210')
client.on('connect', function () {
  client.subscribe('gateway/fcf5c4ffff0cd4e0/event/up', function (err) {
    if (!err) {
      client.publish('gateway', 'Hello mqtt')
    }
  })
})
client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())

//-----------------
// packet decoding
const obj = JSON.parse(message.toString());
// decode a packet
const packet = lora_packet.fromWire(Buffer.from(obj["phyPayload"], "base64"));
 const lora_packet = require("lora-packet");
const packet = lora_packet.fromWire(data.toString(), "base64");

// debug: prints out contents
// - contents depend on packet type
// - contents are named based on LoRa spec
console.log("packet.toString()=\n" + packet.toString());

// e.g. retrieve payload elements
console.log("packet MIC=" + packet.MIC.toString("hex"));
console.log("FRMPayload=" + packet.FRMPayload.toString("hex"));

// check MIC
const NwkSKey = Buffer.from("00000000000000000000000000000000", "hex");
console.log("MIC check=" + (lora_packet.verifyMIC(packet, NwkSKey) ? "OK" : "fail"));

// calculate MIC based on contents
console.log("calculated MIC=" + lora_packet.calculateMIC(packet, NwkSKey).toString("hex"));

// decrypt payload
const AppSKey = Buffer.from("00000000000000000000000000000000", "hex");
var packetD = lora_packet.decrypt(packet, AppSKey, NwkSKey).toString() ;
console.log("Decrypted (ASCII)='" + packetD + "'");
console.log("Decrypted (hex)='0x" + lora_packet.decrypt(packet, AppSKey, NwkSKey).toString("hex") + "'");

//-----------------

//  client.end()
//  */

