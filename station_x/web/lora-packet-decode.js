
var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://192.168.0.210')
client.on('connect', function () {
  client.subscribe('gateway/fcf5c4ffff0cd4e0/event/up', function (err) {
    if (!err) {
      client.publish('gateway', 'Hello mqtt')
    }
  })
})
client.on('message', function (topic, message) {
  // message is Buffer
  console.log(message.toString())
 const lora_packet = require("lora-packet");

//-----------------
// packet decoding
const obj = JSON.parse(message.toString());
// decode a packet
const packet = lora_packet.fromWire(Buffer.from(obj["phyPayload"], "base64"));

// debug: prints out contents
// - contents depend on packet type
// - contents are named based on LoRa spec
console.log("packet.toString()=\n" + packet);

// e.g. retrieve payload elements
console.log("packet MIC=" + packet.MIC.toString("hex"));
console.log("FRMPayload=" + packet.FRMPayload.toString("hex"));

// check MIC
const NwkSKey = Buffer.from("00000000000000000000000000000000", "hex");
console.log("MIC check=" + (lora_packet.verifyMIC(packet, NwkSKey) ? "OK" : "fail"));

// calculate MIC based on contents
console.log("calculated MIC=" + lora_packet.calculateMIC(packet, NwkSKey).toString("hex"));

// decrypt payload
const AppSKey = Buffer.from("00000000000000000000000000000000", "hex");
console.log("Decrypted (ASCII)='" + lora_packet.decrypt(packet, AppSKey, NwkSKey).toString() + "'");
console.log("Decrypted (hex)='0x" + lora_packet.decrypt(packet, AppSKey, NwkSKey).toString("hex") + "'");

//-----------------

//  client.end()
})

