/*
  Station X Weather station.
  BCK 2020
*/  
#include <TinyGPS++.h> //NOT INBUILT https://github.com/mikalhart/TinyGPSPlus.git
#include <SPI.h>
#include <SD.h> //inbuilt arduino version
#include <SdFat.h> //https://github.com/greiman/SdFat
#include <IridiumSBD.h> //Arduino Lib Mikel Hart
#include <avr/sleep.h>          //this AVR library contains the methods that controls the sleep modes
#include <DS3232RTC.h>          //RTC Library https://github.com/JChristensen/DS3232RTC
#include <Wire.h>
#include <BME280I2C.h> //https://github.com/finitespace/BME280.git
#include "station_x.h"
//#include "station_x_predefs.h"
#include "config.h"

//#include "SparkFun_External_EEPROM.h" 
//ExternalEEPROM fMem;
//#include <I2C_eeprom.h>
BME280I2C bme;
// Default : forced mode, standby time = 1000 ms
// Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,
// Declare the IridiumSBD object
IridiumSBD modem(IridiumSerial);

// Create an instance of the TinyGPS object
TinyGPSPlus gps;
/*
Adafruit PCF8523 Real Time Clock
Microchip MCP7940M-I/SN
Adafruit DS3231 Precision RTC ...
*/


//x_config conf;
//config intConf;
//station_x data;

/**
 * Initialises the RTC on first power-up with compile time. This assumes
 * the RTC has not been programmed previously (e.g. power loss)
 * it also  
 * initialises the alarms to known values, clears the alarm flags, clears the alarm interrupt flags
 * and configures the INT/SQW pin for "interrupt" operation (disable square wave output)
*/ 
void displayTime(time_t t)
{
    Serial.print(year(t));
    Serial.print("-");
    Serial.print(month(t));
    Serial.print("-");
    Serial.println(day(t));
    if (hours <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(hour(t));
    Serial.print(":");
    if (minutes <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(minute(t));
    Serial.print(":");
    if (seconds <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.println(second(t));
}

/**
 * Initialises the RTC on first power-up with compile time. This assumes
 * the RTC has not been programmed previously (e.g. power loss)
 * it also  
 * initialises the alarms to known values, clears the alarm flags, clears the alarm interrupt flags
 * and configures the INT/SQW pin for "interrupt" operation (disable square wave output)
  * sets initial RTC time to compiled time if the
  * RTC has not already been initialised. Should 
  * only run once, when first powered up or after battery 
  * failure
*/
void initialiseRTC() 
 { 
    int Hour, Minute, Second, Day, Month, Year;
    time_t t_cp, t_rtc;
    
 
    char time_c[] = __TIME__;
    Second = atoi(time_c + 6);
    *(time_c + 5) = 0;
    Minute = atoi(time_c + 3);
    *(time_c + 2) = 0;
    Hour = atoi(time_c);
    
    char temp[] = __DATE__;
    int i;
    int sum_out;
    Year = 30 + atoi(temp + 9); //ref 1970
    *(temp + 6) = 0;
    Day = atoi(temp + 4);
    *(temp + 3) = 0;
    for (i = 0; i < 12; i++) {
        sum_out = (temp[0] + temp[1] + temp[2]);
        if (months[i] == sum_out) {
            Month = i + 1;
            break;
        }
    }
    setTime(Hour, Minute, Second, Day, Month, Year);
    t_cp = now();
    t_rtc = RTC.get();
    if (DEBUG)
        displayTime(t_cp);
    if (DEBUG)
        displayTime(t_rtc);
    if ((t_rtc < t_cp))
        RTC.write(tm);
    
    RTC.setAlarm(ALM1_MATCH_DATE, 0, 0, 0, 1);
    RTC.setAlarm(ALM2_MATCH_DATE, 0, 0, 0, 1);
    RTC.alarm(ALARM_1);
    RTC.alarm(ALARM_2);
    RTC.alarmInterrupt(ALARM_1, false);
    RTC.alarmInterrupt(ALARM_2, false);
    RTC.squareWave(SQWAVE_NONE);
}

/**
 * call back from SD card to intialise file creation times
*/ 
void dateTime(uint16_t * date, uint16_t * time, uint8_t * ms10)
{
    time_t t;
    t = RTC.get();
    // Return date using FS_DATE macro to format fields.
    *date = FS_DATE(year(t), month(t), day(t));
    // Return time using FS_TIME macro to format fields.
    *time = FS_TIME(hour(t), minute(t), second(t));
    // Return low time bits in units of 10 ms.
    *ms10 = second(t) & 1 ? 100 : 0;
} 

/**
 * Create binary file on SD card
 * file nams is current day hour and minute
 * in a folder for the month
*/ 
void createBinFile(char *userFile)
{
    binFile.close();
    if (DEBUG)
        Serial.print(F("Opening: "));
    if (DEBUG)
        Serial.println(userFile);
    if (!binFile.open(userFile, O_RDWR | O_CREAT)) {
        if (DEBUG)
            Serial.println("open binName failed");
    }
    //Serial.print(F("Allocating: "));
    //Serial.print(MAX_FILE_SIZE_MiB);
    //Serial.println(F(" MiB"));
    if (!binFile.preAllocate(MAX_FILE_SIZE)) {
        if (DEBUG)
            Serial.println("preAllocate failed");
    }
}

/**
 * open binary file for writing
*/ 
void openBinFile(char *userFileName)
{
    if (DEBUG) {
        time_t t;
        t = RTC.get();
        char timeStamp[70];
        sprintf(timeStamp, "Recorded at: %02u-%02u-%02uT%02u:%02u:%02u \n", year(t), month(t), day(t), hour(t), minute(t), second(t));  //Prints time stamp
        Serial.println(timeStamp);
    }
    if (!sd.exists(userFileName)) {
        if (DEBUG)
            Serial.println(userFileName);
        if (DEBUG)
            Serial.println(F("File does not exist"));
        createBinFile(userFileName);
        //return;
    }
    binFile.close();
    if (!binFile.open(userFileName, O_RDWR)) {
        if (DEBUG)
            Serial.println(userFileName);
        if (DEBUG)
            Serial.println(F("open failed"));
        return;
    }
    if (DEBUG)
        Serial.println(F("File opened"));
}

void logData(data_t * bufferD)
{
//  block_t fifoBuffer[FIFO_DIM];
// Write metadata.
//  block_t fifoBuffer[FIFO_DIM];
//  memset(fifoBuffer, 0, sizeof(fifoBuffer));
//  memcpy (fifoBuffer, bufferD, sizeof(data_t));
    if (sizeof(data_t) != binFile.write(bufferD, sizeof(data_t))) {
        if (DEBUG)
            Serial.println("Write data failed");
    }
    delay(10);
    // Truncate file if recording stopped early.
    if (binFile.curPosition() < MAX_FILE_SIZE) {
        //Serial.println(F("Truncating file"));
        //Serial.flush();
        if (!binFile.truncate()) {
            if (DEBUG)
                Serial.println("Can't truncate file");
        }
    }
}

void wlog(char *message, bool sendOut)
{
    return wlog(message, sendOut, 0);
}

/*
   wlog
*/ 
void wlog(char *message, bool sendOut, data_t * binaryMsg)
{
    char userFile[12];
    time_t t;
    t = RTC.get();
    if (binaryMsg) {
        sprintf(userFile, "%04u-%02u/log%02u%02u.bin", year(t), month(t),
                day(t), hour(t));
        openBinFile(userFile);
        logData(binaryMsg);
    }
    if (message) {
        sprintf(userFile, "%04u-%02u/log%02u%02u.csv", year(t), month(t),
                day(t), hour(t));
        if (!file.open(userFile, FILE_WRITE)) {
            error("open failed");
        }
        // Write text data.
        if (!file.print(message)) {
            error("txt write failed");
        }
    }
    if (sendOut) {
        data.sendIridium(message);
    }

}

 
void setup() 
 { 

    //intConf = conf.getConfig();
    pinMode(wake_up, INPUT_PULLUP);    //Set pin input using the built-in pullup resistor
    pinMode(SD_1_CS_PIN, OUTPUT);
    digitalWrite(SD_1_CS_PIN, 0);
    pinMode(SD_2_CS_PIN, OUTPUT);
    digitalWrite(SD_2_CS_PIN, 0);
    
    pinMode(gpsCTRL, OUTPUT);
    pinMode(ST_1_PIN, INPUT);
    
    pinMode(ST_2_PIN, INPUT);
    
    if (DEBUG)
        Serial.begin(115200);
    //FLASH memory
    Wire.begin();
    /*
    Wire.setClock(400000);      // EEPROMs can run 400kHz and higher
    if (myMem.begin(EEPROM_0_ADR_LOW_BLOCK) == false) {
        if (DEBUG)
            Serial.println("No memory detected.");
        criticalFailure = true;
    } else {
        if (DEBUG)
            Serial.println("Memory detected!");
        //Set settings for this EEPROM
        myMem.setMemorySize(262144);    //In bytes. 1024kbits x 2 blocks.
        myMem.setPageSize(128); //In bytes. Has 128 byte page size.
        myMem.enablePollForWriteComplete();     //Supports I2C polling of write completion
        myMem.setPageWriteTime(3);      //3 ms max write time
        if (DEBUG)
            Serial.print("Mem size in bytes: ");
        if (DEBUG)
            Serial.println(myMem.length());
    }*/
    
        //Sensors on I2C
        if (!bme.begin()) {
        if (DEBUG)
            Serial.println("Could not find BME280 sensor!");
        //delay(1000);
    }
    switch (bme.chipModel()) {
    case BME280::ChipModel_BME280:
        if (DEBUG)
            Serial.println("Found BME280 sensor! Success.");
        break;
    case BME280::ChipModel_BMP280:
        if (DEBUG)
            Serial.println("Found BMP280 sensor! No Humidity available.");
        break;
    default:
        if (DEBUG)
            Serial.println("Found UNKNOWN sensor! Error!");
    }
    // Initialize SD.
    if (!sd.begin(SD_CONFIG)) {
        if (DEBUG)
            Serial.println("sd.begin failed");
    }
    // Set callback
    FsDateTime::setCallback(dateTime);
    //createBinFile();
    if (DEBUG)
        sd.ls(&Serial, LS_DATE | LS_SIZE);
    
        //TIME and RTC
    data.gps_offset = 0;
    
    data.iridium_offset = 0;
    
        // set the RTC from the compile date
    initialiseRTC();
    
 
    time_t t;
    t = RTC.get();              //Gets the current time of the RTC
    RTC.setAlarm(ALM1_MATCH_MINUTES , 0, (minute(t) + time_interval) % 60, 0, 0); // Setting alarm 1 every 20 minutes
    //RTC.setAlarm(ALM1_MATCH_SECONDS,
                 (second(t) + time_interval) % 60, 0, 0, 0);
    // clear the alarm flag
    RTC.alarm(ALARM_1);
    // enable interrupt output for Alarm 1
    RTC.alarmInterrupt(ALARM_1, true);
    //GPS
    GPSSerial.begin(9600);      //GPS  init
    getGPSData();

}

/*
   main loop not used. Just sleeping
*/ 
void loop()
{
    gotoSleep();

} 

/*
   Sleep cycle
*/ 
void gotoSleep()
{
    if (DEBUG)
        Serial.println("Sleep");// + String(hour(t)) + ":" + String(minute(t)) + ":" + String(second(t))); 
    sleep_enable();
    attachInterrupt(digitalPinToInterrupt(wake_up), wakeUp, LOW);       //attaching an interrupt to RTC
    set_sleep_mode(SLEEP_MODE_PWR_DOWN); 
    time_t t;                   // creates temp time variable

    if (DEBUG)
        Serial.flush();
    //delay(1000);
    sleep_cpu();                //activating sleep mode
    // ... sleeping ... //
    t = RTC.get();
    if (DEBUG)
        Serial.println("WakeUp");// + String(hour(t)) + ":" + String(minute(t)) + ":" + String(second(t))); 
    bool sendOut = (minute(t) % 10 == 0);
    char message[250];
    data.ppopulate();   
    data.getData(message);
        
    //count++;
    wlog(message, false);
    
    //Set New Alarm
    //RTC.setAlarm(ALM1_MATCH_MINUTES , 0, (minute(t)+time_interval)%60, 0, 0);
    RTC.setAlarm(ALM1_MATCH_SECONDS, (second(t) + 20) % 60, 0, 0, 0);
    RTC.alarm(ALARM_1);

}

/*
   Routine fired by interupt
*/ 
void wakeUp()
{
    sleep_disable(); 
    detachInterrupt(digitalPinToInterrupt(wake_up));    //Removes the interrupt from pin 2;
}
