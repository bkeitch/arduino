// invoke with port number `PORT=3000 node index.js`
// visit: http://localhost:3000/plots/0/index.html
const plotlib = require('nodeplotlib');

let arrayOfData = require ('./data.json');
var data_x = [];
var data_y = [];
const csv = false;
var t0 = 0;
for (var i = 0; i < arrayOfData.length; i++) {
  var object = arrayOfData[i];
  //for (var property in object) {
  //  console.log('item ' + i + ': ' + property + '=' + object[property]);
  //}

  if (i==0) {
	  t0= object.time;
  }
  if (csv) {
	console.log((object.time-t0) + ',' + ((object.T_air)/100)
	+ ',' + ((object.T_ground)/100) 
	+ ',' + ((object.P)/10) 
	+ ',' + ((object.P2)/10) 
	+ ',' + ((object.Vb)/200)
	+ ',' + ((object.I)/66) );
  } 

  data_x.push(object.time-t0);
  data_y.push(object.Vb/200);

}
const data = [{x: data_x, y: data_y, type: 'scatter'}];
plotlib.plot(data);



