
//The 24LC1025 is 1024kbit or 128k bytes.
//If the eeaddress is less than the 64k byte threshold we use I2C address 0x50
//If the address is above 65535 then we use 0x54 address
//Based on: https://playground.arduino.cc/Code/I2CEEPROM
void writeEEPROM(long eeAddress, byte data)
{
  if (eeAddress < 65536)
  {
    Wire.beginTransmission(EEPROM_ADR_LOW_BLOCK);
    eeAddress &= 0xFFFF; //Erase the first 16 bits of the long variable
  }
  else
  {
    Wire.beginTransmission(EEPROM_ADR_HIGH_BLOCK);
  }

  Wire.write((byte)(eeAddress >> 8)); // MSB
  Wire.write((byte)(eeAddress & 0xFF)); // LSB
  Wire.write(data);
  Wire.endTransmission();
}

byte readEEPROM(long eeaddress)
{
  if (eeaddress < 65536)
    Wire.beginTransmission(EEPROM_ADR_LOW_BLOCK);
  else
    Wire.beginTransmission(EEPROM_ADR_HIGH_BLOCK);

  Wire.write((byte)(eeaddress >> 8)); // MSB
  Wire.write((byte)(eeaddress & 0xFF)); // LSB
  Wire.endTransmission();

  if (eeaddress < 65536)
    Wire.requestFrom(EEPROM_ADR_LOW_BLOCK, 1);
  else
    Wire.requestFrom(EEPROM_ADR_HIGH_BLOCK, 1);

  byte rdata = 0xFF;
  if (Wire.available()) rdata = Wire.read();
  return rdata;
}


//Given a global array, write 32 bytes to EEPROM
//EEPROM can handle single writes up to 128 bytes but Arduino's Wire buffer is only 32 bytes :(
//Function does not handle crossing block boundary, it's left to the user
int writeEEPROMPage(long eeAddress, byte * data, int offset)
{
  if (eeAddress < 65536)
  {
    Wire.beginTransmission(EEPROM_ADR_LOW_BLOCK);
    eeAddress &= 0xFFFF; //Erase the first 16 bits of the long variable
  }
  else
  {
    Wire.beginTransmission(EEPROM_ADR_HIGH_BLOCK);
  }

  Wire.write((byte)(eeAddress >> 8)); // MSB
  Wire.write((byte)(eeAddress & 0xFF)); // LSB

  //Write bytes to EEPROM
  for (byte x = 0 ; x < MAX_I2C_WRITE ; x++)
    Wire.write(data[x + (MAX_I2C_WRITE*offset)]); //Write the data
  Wire.endTransmission(); //Send stop condition
  uint32_t timeout=millis();
  int err;
  while((millis()-timeout<100)&&(err!=0)) {
   if (eeAddress < 65536)
  {
    Wire.beginTransmission(EEPROM_ADR_LOW_BLOCK);
    eeAddress &= 0xFFFF; //Erase the first 16 bits of the long variable
  }
  else
  {
    Wire.beginTransmission(EEPROM_ADR_HIGH_BLOCK);
  }
   err=Wire.endTransmission();
  }
  return err;
}