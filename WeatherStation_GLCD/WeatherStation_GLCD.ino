#include <RtcDateTime.h>
#include <RtcDS1307.h>
#include <openGLCD.h>
#include <Wire.h>

/*
Weather station. Plot barometric trend as bar graph. Stores data in EEPROM and uses RTC. Data can be retrieved using serial port.
BCK 2017

Hardware: I2C bus to DS1307 RTC and 24A512 EEPROM 
Sensor: MPX4115 at 5V

RTC library is RTC by Mankuna library
openGLCD installed from Zip.

 */

const int MAXVAL = 1015;
const int MINVAL = 985;
const int PERIOD = 2;//600;
const int EEPROMAddress = 80;
const int sensorPin = A0;  // Analog input pin that the pressure sensor is attached to
const int tempAddr = 0x48; //1001000
const int NUM_BARS = 34;

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value converted to mV
int addr = 0; //start address of EEPROM to store data. Can be set from serial
int secondsRun = 0;

// initialize the RTC

RtcDS1307<TwoWire> rtc(Wire);

void setup() {
  Wire.begin();
  Serial.begin(115200);
  // Initialize the GLCD 
  GLCD.Init();

 // Select the font for the default text area
  GLCD.SelectFont(System5x7);

  GLCD.print(F("Pressure  Time"));
  //rtc.begin();
  if (! rtc.GetIsRunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.SetIsRunning(true);
  } 
  rtc.SetSquareWavePin(DS1307SquareWaveOut_Low);
  Serial.print("#Current date:  ");
  printDate(rtc.GetDateTime());
  printTime(rtc.GetDateTime());
  Serial.println();       
}
/*
 * Format date string on the serial port ISO format
 */
void printDate(RtcDateTime now) {
  Serial.print(now.Year(), DEC);
  Serial.print('/');
  Serial.print(now.Month(), DEC);
  Serial.print('/');
  Serial.print(now.Day(), DEC);
  Serial.print(' ');
}
/*
 * Format time string on the serial port ISO format
 */
void printTime(RtcDateTime now) {
  Serial.print(now.Hour(), DEC);
  Serial.print(':');
  if(now.Minute() < 10)
     Serial.print('0');
  Serial.print(now.Minute(), DEC);
  Serial.print(':');
  if(now.Second() < 10)
     Serial.print('0');
  Serial.print(now.Second(), DEC);
}
/*
 * Format time for displaying on LCD screen.
 */
void showTime(RtcDateTime now) {
  if(now.Hour() < 10)
     GLCD.print(' ');
  GLCD.print(now.Hour());
  GLCD.print(':');
  if(now.Minute() < 10)
     GLCD.print('0');
  GLCD.print(now.Minute());
  GLCD.print(':');
  if(now.Second() < 10)
     GLCD.print('0');
  GLCD.print(now.Second());
}
unsigned int TotalPressure, AveragePressure = 0;
/*
 * Temperature sensor T77 is on SPI bus
 */
void printTemp() {
  byte temp = 0;
  Wire.beginTransmission(tempAddr);
  Wire.write((byte)(tempAddr));
  Wire.endTransmission();
  Wire.requestFrom(tempAddr, 2);
  while(Wire.available())    // slave may send less than requested
  {
    temp = Wire.read();    // receive a byte as character    
  }
  
  GLCD.CursorTo(0, 2);
  GLCD.print(temp);
    
}
/*
 * Plot histogram of data in bottom part of screen
 * 
 */
void plotData(int currentAddr) {

  if(addr < NUM_BARS*6)
    return;
  for (unsigned int num = 0, theMemoryAddress = 0, i=0; i<NUM_BARS; i++) {
  
    theMemoryAddress = currentAddr + (i - NUM_BARS*6);
    //long timestamp = WireEepromReadLong(EEPROMAddress, theMemoryAddress);
    theMemoryAddress +=4;     
    outputValue = WireEepromReadInt(EEPROMAddress, theMemoryAddress);
    //      GLCD.CursorTo(0,i%5+3);
    //      showTime(DateTime(timestamp));
    //      GLCD.print(' ');
    //      GLCD.print(theMemoryAddress);
    //      GLCD.print(' ');
    //      GLCD.print(num);
    GLCD.DrawVBarGraph(GLCD.Left+1+(i*4), GLCD.Bottom-1, 5, -(GLCD.Height-2*10), 0, 0, MAXVAL-MINVAL, max(outputValue-MINVAL,0));
  }
}
/*
 * Reads pressure values
 * Plots graphs
 * Responds to serial commands
 * Stores data
 */
void loop() {
  // read the analog in value:
  sensorValue = analogRead(sensorPin);
  // map it to mV
  outputValue = map(sensorValue, 0, 1023, 0, 5000);
  // convert to nearest hPa as per data sheet
  unsigned int pressure = (unsigned int) ((outputValue / (50*0.009))+ (9.5/.009));
  TotalPressure += pressure;
  RtcDateTime now = rtc.GetDateTime();
  
  //store data in EEPROM every 10 mins and replot graph
  if(secondsRun%PERIOD==0) {
    WireEepromWriteLong(EEPROMAddress, addr, now.TotalSeconds64());//outputValue
    //Increment EEPROM address
    addr += sizeof(long);
    //store value in EEPROM
    AveragePressure=round(TotalPressure/(float)PERIOD);
    WireEepromWriteInt(EEPROMAddress, addr, AveragePressure);
    addr+= sizeof(int);
    //plot last points
    plotData(addr);
    secondsRun=0; //reset count
    TotalPressure = 0; //reset average
  }
    
  //Display current time and pressure
  GLCD.CursorTo(0, 1);
  if(pressure < 10000)
    GLCD.print(' ');
  GLCD.print(pressure/10.0);
  GLCD.print("hPa");
  GLCD.print(" ");
  
  showTime(now);
  //wrap when memory full 
  if(addr >= 65535 ) //24A512 is 512Kibits = 64 Kibytes
    addr = 0;
  
  // send data if amount is requested (amount must be bytes, %6)
  String txtMsg = "";  
  char s;
  int retrieve = 0;
  while (Serial.available() > 0) {
    s=(char)Serial.read();
    if (s == '\n') {
      if(txtMsg[0] == 'T') {
        uint32_t timeFrom2000 = atol(txtMsg.substring(1).c_str());
        rtc.SetDateTime(timeFrom2000);
        printDate(rtc.GetDateTime());
        printTime(rtc.GetDateTime());
      } else if(txtMsg[0] == 'R') {
          
      } else if (txtMsg[0] == 'A') {
        addr = atoi(txtMsg.substring(1).c_str());
      } else  if (txtMsg[0] == 'G') {
        retrieveData();
      }
      txtMsg = "";  
    } else {  
      txtMsg +=s; 
    }
  }
  
  
  
  delay(1000); //delay 1 sec before redoing loop.
  ++secondsRun; 
}
/*
 * Resets all memory to zeros (slow process as requires I2C)
 */
void resetMemory() {
  GLCD.CursorTo(0, 0);
  GLCD.print(F(" *Resetting Memory* "));
  for (unsigned int theMemoryAddress = 0; theMemoryAddress < 65535; ) {
    WireEepromWriteLong(EEPROMAddress, theMemoryAddress, 0L);//outputValue
    theMemoryAddress += sizeof(long);
    WireEepromWriteInt(EEPROMAddress, theMemoryAddress, 0);
    theMemoryAddress+= sizeof(int);
}  
}
/*
 * Retreives data via serial port
 */
void retrieveData() {
  GLCD.CursorTo(0, 0);
  GLCD.print(F(" *Sending Data* "));
  for (unsigned int theMemoryAddress = 0; theMemoryAddress < 65535; ) {
    Serial.print(WireEepromReadLong(EEPROMAddress, theMemoryAddress));
    theMemoryAddress +=sizeof(long);    
    Serial.print(", ");
    Serial.print(WireEepromReadInt(EEPROMAddress, theMemoryAddress));
    theMemoryAddress +=sizeof(int);
    Serial.println();
  }   
}



void WireEepromRead(int EEPROMAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(EEPROMAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.endTransmission();
    Wire.requestFrom(EEPROMAddress, 1);
    while(Wire.available()) {
      theByteArray[theByteIndex] = Wire.read();
    }
  }
}

byte WireEepromReadByte(int EEPROMAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(byte)];
  WireEepromRead(EEPROMAddress, theMemoryAddress, sizeof(byte), theByteArray);
  return (byte)(((theByteArray[0] << 0)));
}

int WireEepromReadInt(int EEPROMAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(int)];
  WireEepromRead(EEPROMAddress, theMemoryAddress, sizeof(int), theByteArray);
  return ((((int)theByteArray[0] << 8)) | (((int)theByteArray[1] << 0)));
}
long WireEepromReadLong(int EEPROMAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(long)];
  WireEepromRead(EEPROMAddress, theMemoryAddress, sizeof(long), theByteArray);  
  return ((((long)theByteArray[0] << 24) |(((long)theByteArray[1] << 16) | ((long)theByteArray[2] << 8))) | ((long)theByteArray[3] << 0));
}
void WireEepromWrite(int EEPROMAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(EEPROMAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.write(theByteArray[theByteIndex]);
    Wire.endTransmission();
    delay(5);
  }
}

void WireEepromWriteByte(int EEPROMAddress, unsigned int theMemoryAddress, byte theByte) {
  byte theByteArray[sizeof(byte)] = {(byte)(theByte >> 0)};
  WireEepromWrite(EEPROMAddress, theMemoryAddress, sizeof(byte), theByteArray);
}

void WireEepromWriteInt(int EEPROMAddress, unsigned int theMemoryAddress, int theInt) {
  byte theByteArray[sizeof(int)] = {(byte)(theInt >> 8), (byte)(theInt >> 0)};
  WireEepromWrite(EEPROMAddress, theMemoryAddress, sizeof(int), theByteArray);
}

void WireEepromWriteLong(int EEPROMAddress, unsigned int theMemoryAddress, long theLong) {
  byte theByteArray[sizeof(long)] = {(byte)(theLong >> 24), (byte)(theLong >> 16), (byte)(theLong >> 8), (byte)(theLong >> 0)};
  WireEepromWrite(EEPROMAddress, theMemoryAddress, sizeof(long), theByteArray);
}

