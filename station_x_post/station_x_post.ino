/**
   Station X Power On Self Test

  BCK 2022

*/
#define DEBUG 1
#define DEBUG_RTC 1
#define USE_EEPROM 1
#define USE_GPS  1
#define USE_IRIDIUM 1
//#define WIFI_MODEM 1
#define USE_SD 1
#define USE_MENU 1
#define USE_DEPTH 1
//default wake up every few seconds. Change to minutes.
//#define USE_MINUTES 1
#define USE_BME 1
//#define DIAGNOSTICS 1

#ifdef USE_GPS
#include <TinyGPS++.h> //http://arduiniana.org/libraries/tinygpsplus/
#endif
#ifndef PRE
#include <DS3232RTC.h>  //RTC Library https://github.com/JChristensen/DS3232RTC
#include <Wire.h> //built in
#include <avr/sleep.h>          //contains sleep modes
#include <BME280I2C.h>
#include "x_data.h"
//#include <MsgPack.h>
//#include <Base64.h>
#endif
#ifdef USE_MENU
#include <slight_DebugMenu.h>
#endif
#ifdef USE_EEPROM
#include <SparkFun_External_EEPROM.h> //  http://librarymanager/All#SparkFun_External_EEPROM
#endif

#ifdef USE_SD
#include <SD.h> //built in
#endif
#ifdef USE_IRIDIUM
#include <IridiumSBD.h> // v2.0 from library
#endif


static constexpr int timeout = 2000;
static constexpr int time_interval = 5;
static constexpr int months[] = {
  281,
  269,
  288,
  291,
  295,
  301,
  299,
  285,
  296,
  294,
  307,
  268
};

//Global objects:

x_data data;
#ifdef USE_BME
BME280I2C bme; // Default : forced mode, standby time = 1000 ms
// Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off
#endif
#ifdef USE_EEPROM
ExternalEEPROM EEPROM_1;
#endif
DS3232RTC RTC;

// slight_DebugMenu(Stream &in_ref, Print &out_ref, uint8_t input_length_new);
#ifdef USE_MENU
slight_DebugMenu menu(Serial, Serial, 2);
#endif
unsigned int wakes;
static constexpr unsigned long memorySize = 131072UL;
static constexpr unsigned int MAX_WAKES = (int) (memorySize / sizeof(data));
bool haveBME = false;
/**
   Shows time on serial output
*/
void displayTime(time_t t) {
  char cbuffer[20];
  sprintf(cbuffer, "%04u-%02u-%02u %02u:%02u:%02u",
          year(t), month(t),  day(t),  hour(t),  minute(t),  second(t)
         );
  Serial.println(cbuffer);
}
/**
 * Update distance to ground from echo sensor using current temp
 * @return error code
 * c_air =331.3 + T * 0.606 m/s
 * 1cm ~ 27us in warm air
 * 2m ~ 5491us in warm air (7000 in cold air)
 */
 
#ifdef USE_DEPTH
int getDepth(float temp) {
  unsigned long echo_time=0;
  double distance = 0;
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(EN_SNOW, LOW);
  delayMicroseconds(100);
  for (unsigned int i=0 ; i<10 ; i++) {
  noInterrupts();
    digitalWrite(SNOW_TRIG, LOW);
    delayMicroseconds(2);
    digitalWrite(SNOW_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite (SNOW_TRIG, LOW);
    echo_time = pulseIn(SNOW_SIG, HIGH, 10000); //max 3m at -70C
  interrupts();
    if (echo_time==0) return (-1);
    distance+=echo_time;
  }
  if (temp < -60.0 || temp > 60.0) temp = 20.0; //for testing
  distance = distance/200000.0 * (331.3 + temp * 0.606) ; //cm (there and back)
  if((distance  > 255.0) || (distance < 0.1)) return (-2);
  digitalWrite(EN_SNOW, HIGH);
  #ifdef DEBUG
  Serial.print(distance); Serial.println(F(" cm"));
  #endif
  return ((int) round(distance));
}
#endif
/**
   Set RTC from individual components
*/
void setRTCTime(int h, int m, int s, int d, int mn, int y, bool first)	{
  tmElements_t new_time;
  new_time.Year   = y;
  new_time.Month  = mn;
  new_time.Day    = d;
  new_time.Hour   = h;
  new_time.Minute = m;
  new_time.Second = s;
  time_t t_cp = makeTime(new_time);
  time_t t_rtc = RTC.get();
#ifdef DEBUG_RTC
  Serial.println(F("Updated Time: "));
  displayTime(t_cp);
#endif

  if (first || (0 == data.updateGPSOffset(t_rtc, t_cp))) {
    RTC.set(t_cp);
  #ifdef DEBUG_RTC
    Serial.println(F("RTC Time: "));
    displayTime(t_rtc);
  #endif
  } else {
#ifdef DEBUG_RTC
    Serial.println(F("RTC not changed"));
#endif
  }
}
#ifdef USE_GPS
void getGPS() {
  TinyGPSPlus gps;
  bool timeNeeded = true;
  bool posNeeded = true;
  unsigned long start_time = millis();
  //NEO1.begin(9600);
  while (posNeeded || timeNeeded) {
    if (millis() - start_time > 10000) {
      Serial.println(F("GPS Timed out"));
      posNeeded = false;
      timeNeeded = false;
    }
    while (NEO1.available() > 0)
    {
      int c = NEO1.read();
      if (gps.encode(c)) {
        if (gps.time.isUpdated() && timeNeeded)
        {
          setRTCTime(gps.time.hour(), gps.time.minute(), gps.time.second(), gps.date.day(), gps.date.month(), gps.date.year() - 1970, false); //GPS epoch is 2000 not 1970
          timeNeeded = false;
        }
        if (gps.location.isUpdated() && posNeeded)
        {
#ifdef DEBUG
          Serial.print(F("\nLAT=")); Serial.print(gps.location.lat(), 6);
          Serial.print(F("\tLONG=")); Serial.println(gps.location.lng(), 6);
#endif
          posNeeded = false;
        }
      }
    }
  }
  //NEO1.end();
}
#endif

/**
   Initialises the RTC on first power-up with compile time. This assumes
   the RTC has not been programmed previously (e.g. power loss)
   it also  initialises the alarms to known values, clears the alarm flags, clears the alarm interrupt flags
   and configures the INT/SQW pin for "interrupt" operation (disable square wave output)
*/
void initialiseRTC() {
  int Hour, Minute, Second, Day, Month, Year;

  char time_c[] = __TIME__;
  Second = atoi(time_c + 6);
  *(time_c + 5) = 0;
  Minute = atoi(time_c + 3);
  *(time_c + 2) = 0;
  Hour = atoi(time_c);

  char temp[] = __DATE__;
  int i;
  int sum_out;
  Year = atoi(temp + 9); //ref 1970
  *(temp + 6) = 0;
  Day = atoi(temp + 4);
  *(temp + 3) = 0;
  Month = 0;
  for (i = 0; i < 12; i++) {
    sum_out = (temp[0] + temp[1] + temp[2]);
    if (months[i] == sum_out) {
      Month = i + 1;
      break;
    }
  }
  setRTCTime(Hour, Minute, Second, Day, Month, Year + 30, true);
  time_t t_rtc = RTC.get();
  //alarm 2 not used
  RTC.setAlarm(DS3232RTC::ALM2_MATCH_DATE, 0, 0, 0, 1);
  RTC.alarm(DS3232RTC::ALARM_2);
  RTC.alarmInterrupt(DS3232RTC::ALARM_2, false);
  RTC.squareWave(DS3232RTC::SQWAVE_NONE);
  // Setting alarm 1 to wake up
#ifdef USE_MINUTES
  RTC.setAlarm(DS3232RTC::ALM1_MATCH_MINUTES, 0, (minute(t_rtc) + time_interval) % 60, 0, 0);
#else
  RTC.setAlarm(DS3232RTC::ALM1_MATCH_SECONDS, (second(t_rtc) + time_interval) % 60, 0, 0, 0);
#endif
  // clear the alarm flag
  RTC.alarm(DS3232RTC::ALARM_1);
  // enable interrupt output for Alarm 1
  RTC.alarmInterrupt(DS3232RTC::ALARM_1, true);
  //attachInterrupt(digitalPinToInterrupt(WAKE_UP), wakeUp, LOW); //attaching an interrupt to RTC
 
}
/**
   Call back for Filesystem times.
*/
#ifdef USE_SD
void dateTime(uint16_t* date, uint16_t* time) {
  time_t t_rtc = RTC.get();
  // Return date using FS_DATE macro to format fields.
  *date = FAT_DATE(year(t_rtc), month(t_rtc), day(t_rtc));

  // Return time using FS_TIME macro to format fields.
  *time = FAT_TIME(hour(t_rtc), minute(t_rtc), second(t_rtc));

  // Return low time bits in units of 10 ms.
  //*ms10 = second(t_rtc) & 1 ? 100 : 0;
}
#endif

/**
   Gets up-to-date parameters from sensors
*/
void updateXData(bool update) {
  unsigned long mem = wakes * sizeof(x_data);
  float bme_p = 0, bme_t = 0, bme_u = 0;

  float temperatureSensorValue = 0, pressureSensorValue = 0, solarVoltage = 0, batteryVoltage = 0;
  for(int i=0; i< 10; i++) {
    temperatureSensorValue += analogRead(externalTPin);
    pressureSensorValue += analogRead(pressurePin);
    batteryVoltage += analogRead(vBatteryPin);
    solarVoltage += analogRead(vSolarPin);
    delay(50);
  }
  
  int tempV = map(temperatureSensorValue, 0, 10230, 0, 1000) ;
  float tempRratio = (tempV-1) ; //10k resistor in series. R in k but 10k is divided away below
  #define BETA 3320
  float temperature = 1/(1/(BETA)*log(tempRratio) + 1/297.15 ) - 272.15;
  // convert to nearest hPa as per data sheet
  int pressure = (map(pressureSensorValue, 0, 10230, 0, 5000) + (3300*0.095))/ (3.3*0.9);
  int bV = map(batteryVoltage, 0, 10230, 0, 55000);
  int sV = map(solarVoltage, 0, 10230, 0, 55000);

  int depth = 0;
  #ifdef USE_BME
  if (haveBME ) {
    bme.read(bme_p, bme_t, bme_u, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
  }
  #endif
#ifdef USE_DEPTH
  depth = getDepth(bme_t);
#endif
#ifdef DEBUG
  Serial.print(bme_p); Serial.println(F(" Pa\t"));
  Serial.print(bme_t); Serial.println(F(" C\t"));
  Serial.print(pressure); Serial.println(F(" Pa\t"));
  Serial.print(temperature); Serial.println(F(" C\t"));
  Serial.print(bV); Serial.println(F("(battery) V\t"));
  Serial.print(sV); Serial.println(F("(solar) V\t"));
  Serial.print(bme_u); Serial.println(F("%"));
  Serial.print(depth); Serial.println(F("cm"));
#endif
  time_t t = RTC.get();
  if (update) data.update(bme_t, bme_p, bme_u, depth, t, mem);
}


void freeRam () {
  extern int __heap_start, *__brkval;
  int v;
  int freeRam = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
  if(200 < freeRam) {
    Serial.print(F("RAM: "));
    Serial.println(freeRam);
    Serial.flush();
  }
}


#ifdef USE_IRIDIUM
int testIridium () {
  IridiumSBD modem(IM1, SLEEP);
  int err;
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {
    return err;
  }

  // Print the firmware revision
  char version[12];
  err = modem.getFirmwareVersion(version, sizeof(version));
  if (err != ISBD_SUCCESS)
  {
    return err;
  }
  Serial.print(F("Firmware Version is "));
  Serial.print(version);
  int signalQuality = -1;

  err = modem.getSignalQuality(signalQuality);
  if (err != ISBD_SUCCESS)
  { 
    return err;
  }

  Serial.print(F("Signal quality (0-5): "));
  Serial.println(signalQuality);


  return ISBD_SUCCESS;

}
int getIridiumTime () {
  IridiumSBD modem(IM1, SLEEP);
  int err;
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {
    return err;
  }

  time_t t_cp;
  err = modem.getSystemTime(t_cp);
  if (err == ISBD_SUCCESS)
  {
    time_t t_rtc = RTC.get();
#ifdef DEBUG_RTC
    Serial.println(F("Iri Time: "));
    displayTime(t_cp);
    Serial.println(F("RTC Time: "));
    displayTime(t_rtc);
#endif

    if (0 == data.updateIridiumOffset(t_rtc, t_cp)) {
      RTC.set(t_cp);
    } else {
#ifdef DEBUG_RTC
      Serial.println(F("RTC not set"));
#endif
    }
  } else {
    return err;
  }
  return ISBD_SUCCESS;
}

int sendIridiumMessage(const uint8_t *txData, size_t txDataSize) {
  IridiumSBD modem(IM1, SLEEP);
  int err;
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {

    return err;
  }
  err = modem.sendSBDBinary(txData, txDataSize);
  if (err != ISBD_SUCCESS)
  {
    return err;
  }
  return ISBD_SUCCESS;
}
#endif
#ifdef DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD * device, char c)
{
  Serial.write(c);
}

void ISBDDiagsCallback(IridiumSBD * device, char c)
{
  Serial.write(c);
}
#endif
#ifdef USE_SD
void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print(F("\t"));
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println(F("/"));
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print(F("\t\t"));
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}
int testSD(int SD_CARD) {
  //digitalWrite(EN_EEPROM, LOW); //switch on V translate
   int CS = SD_1_CS;
   if (SD_CARD == 1) {
   } else {
    CS = SD_2_CS;
   }

  if (!SD.begin(CS)) {
    return SD1_INIT_FAIL + SD_CARD;
  } 
  Serial.println("SD Contents");
  File root = SD.open(F("/"));
  printDirectory(root, 0);
  root.close();
  SD.end();
  
  return SD_TEST_OK;

}
#endif
#ifdef USE_EEPROM
int setupEEPROM() {

  Wire.begin();
  Wire.setClock(400000); // EEPROMs can run 400kHz and higher
  if (EEPROM_1.begin(EEPROM_0_ADR_LOW_BLOCK_1) == false) {
    return EEPROM_NOT_DETECTED;
  }
  //Set settings for this EEPROM
  EEPROM_1.setMemorySize(memorySize); //In bytes. 1024kbits x 2 blocks.
  EEPROM_1.setPageSize(128); //In bytes. Has 128 byte page size.
  EEPROM_1.enablePollForWriteComplete(); //Supports I2C polling of write completion
  EEPROM_1.setPageWriteTime(3); //3 ms max write time
  EEPROM_1.get(0, wakes);
  Serial.print(F("EEPROM size: "));
  Serial.print(wakes);
  Serial.print(F(" of "));
  Serial.println(MAX_WAKES);
  return EEPROM_TEST_OK;
}
#endif
#ifdef USE_BME
int setupSensor() {
  const unsigned int MAXTRIES = 2;
  unsigned int i = 0;
  while (!bme.begin() && i < MAXTRIES ) {
    delay(100);
    ++i;
  }
  if (i == MAXTRIES) {
    #ifdef DEBUG
    Serial.println("No BME found");
    #endif
    return BME_NOT_FOUND;

  }
  if (bme.chipModel() == BME280::ChipModel_BME280 || bme.chipModel() ==BME280::ChipModel_BMP280) {
      return BME_SUCCESS;
  #ifdef DEBUG
    if(bme.chipModel() == BME280::ChipModel_BME280) Serial.println("BME280 found");
    if(bme.chipModel() == BME280::ChipModel_BMP280) Serial.println("BMP280 found");
    #endif
    
  }
  return BME_NOT_FOUND;
}
#endif
#ifdef USE_EEPROM
void readMemory() {
  char cbuffer [250];
  for (unsigned int addr = 0; addr < wakes * sizeof(x_data); addr += sizeof(x_data)) {
    Serial.print('@');
    Serial.print(addr, HEX);
    Serial.print(F(" \t "));

    if (addr == 0) {
      int current_wakes;
      EEPROM_1.get(addr, current_wakes);
      Serial.println(current_wakes);
    } else {
      x_data data_out;
      EEPROM_1.get(addr, data_out); //location,  data
      data_out.getJSONData(cbuffer);
      Serial.println(cbuffer);
    }
  }
}
#endif
/**
   Places Arduino to sleep. When woken up and sets alarm again
*/
void gotoSleep() {

  // writeDataEEPROM();
  #ifdef DEBUG
  Serial.println(F("Sleeping")); // + String(hour(t)) + ":" + String(minute(t)) + ":" + String(second(t)));
  //if we want to see the message before going to sleep:
  Serial.flush();
  digitalWrite(EN_GPS, HIGH);
  digitalWrite(EN_MODEM, HIGH);
  digitalWrite(EN_SD1, HIGH);//switch off SD1
  digitalWrite(EN_SD2, HIGH);//switch off SD2
  digitalWrite(EN_EEPROM, HIGH);
  digitalWrite(EN_SENSE, HIGH);

  #endif
  sleep_enable();
  attachInterrupt(digitalPinToInterrupt(WAKE_UP), wakeUp, LOW); //attaching an interrupt to RTC
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  //delay(1000);
  sleep_cpu(); //activating sleep mode
  // ... sleeping ... //
  time_t t_rtc = RTC.get();
  Serial.print(F("Woke up at: ")); 
  displayTime(t_rtc);
  //TODO: check power
  digitalWrite(EN_MODEM, LOW);
  //TODO check we need GPS
  digitalWrite(EN_GPS, LOW);    // green on = GPS on

  digitalWrite(EN_SD1, LOW);//switch on SD1
  digitalWrite(EN_SD2, LOW);//switch on SD2
  digitalWrite(EN_EEPROM, LOW);
  digitalWrite(EN_SENSE, LOW);
  delay(30000); //fire up satellites
  displayTime(t_rtc);


  // Reset alarm to go off in next time interval
#ifdef USE_MINUTES
  RTC.setAlarm(DS3232RTC::ALM1_MATCH_MINUTES, 0, (minute(t_rtc) + time_interval) % 60, 0, 0);
#else
  RTC.setAlarm(DS3232RTC::ALM1_MATCH_SECONDS, (second(t_rtc) + time_interval) % 60, 0, 0, 0);
#endif
  RTC.alarm(DS3232RTC::ALARM_1);

}

/**
   Routine fired by interrupt to wake up.
*/
void wakeUp() {
  sleep_disable();
  #ifdef DEBUG
  Serial.print("INT Wakeup pin is " ); 
  Serial.println (digitalRead(WAKE_UP));
  #endif
  detachInterrupt(digitalPinToInterrupt(WAKE_UP));

}
/**
   Save data to EEPROM. Store data count at 1 and increment.
*/
#ifdef USE_EEPROM
void writeDataEEPROM() {

  EEPROM_1.get(0, wakes); // current count of data
  wakes++;
  if ( MAX_WAKES  < wakes ) { //need to leave 1 space
    wakes = 1;
  }
  updateXData(true);
#ifdef DEBUG
  char cbuffer[250];
  Serial.println(F("Current Data:"));
  data.getJSONData(cbuffer);
  Serial.println(cbuffer);
  Serial.print(F("Writing entry: ")); Serial.println(wakes);
#endif
  EEPROM_1.put((wakes * sizeof(data)), data); //(location, data)
  EEPROM_1.put(0, wakes); // store incremented data count
}
#endif
/**
   Log data to SD card
*/
#ifdef USE_SD
int writeDataSD(int SD_CARD)
{
//  Serial.println(freeRam());
 int CS = SD_1_CS;
   if (SD_CARD == 1) {
   } else {
    CS = SD_2_CS;
   }
  if (!SD.begin(CS)) {
    return SD1_INIT_FAIL + SD_CARD;
  }
  char userFile[14];
  time_t t;
  t = RTC.get();
  //8.3 file names
  snprintf(userFile, 14, "d%04u-%02u.csv", minute(t), second(t));

  File dataFile = SD.open(userFile, FILE_WRITE);

  if (!dataFile) {
    SD.end();
    return SD_FILE_ERROR;
  }
  dataFile.println("TESTING TESTING TESTING");
  /*
    snprintf(userFile, 14, "d%04u-%02u.csv", year(t), month(t));

      File dataFile = SD.open(userFile, FILE_WRITE);

      if (dataFile) {
        x_data data_out;
        char cbuffer [250];

        for (unsigned int addr = sizeof(data_out); addr < wakes * sizeof(data_out); addr += sizeof(data_out)) {
          EEPROM_1.get(addr, data_out);
          data_out.getCSVData(cbuffer);
          dataFile.println(cbuffer);
        }
  */
  dataFile.close();

  SD.end();
  //Serial.println(freeRam());

  return SD_WRITE_OK;
}
#endif

/**
    Send data via WiFi modem (MD1) or Iridium Modem
*/
#ifdef WIFI_MODEM
void sendData(bool IRIDIUM) {


  MsgPack::Packer packer;
  packer.serialize(data);
#ifdef USE_IRIDIUM
    //sendIridiumMessage(output_buffer);
    return sendIridiumMessage(packer.data(), packer.size());
#endif
  long start = millis();
  boolean done = false;
  // read for incoming messages. c = send, x = don't send:
  while (!done) {
    if (timeout > millis() - start) {
      done = true;
    }
    while (MD1.available() > 0) {
      char inChar = MD1.read();
      switch (inChar) {
        case 'b': // begin
          {
            char cbuffer [50]; //allow for base 64 increase 8/6
            Base64.encode(cbuffer, (char*) packer.data(), (int) packer.size());
            MD1.println(cbuffer);
            done = true;
          }
          break;
        case 'e': // end
          done = true;
          break;
        default:
          break;
      }
    }
  }
}
#endif
// Main Menu
#ifdef USE_MENU
void mainMenu(slight_DebugMenu * pInstance) {
  Print &out = pInstance->get_stream_out_ref();
  char *command = pInstance->get_command_current_pointer();
  switch (command[0]) {
    case 'h':
    case 'H':
    case '?': {
        // help
        out.println();
        out.println(F("\t '?': this help"));
        out.println(F("\t 'i': test Iridium"));
        out.println(F("\t 'y': get Iridium time"));
        out.println(F("\t 't': Read RTC "));
        out.println(F("\t 'g': GPS time"));
        out.println(F("\t 'm': read memory"));
        out.println(F("\t 'w': Write memory "));
        out.println(F("\t 'p': Read PTU Data "));
        out.println(F("\t 'd': Read Snow Depth"));
        out.println(F("\t 's': Send data"));
        out.println(F("\t 'x': Send data on Iridium"));
        out.println(F("\t 'a': list both SD card contents"));
        out.println(F("\t '1': Log data SD 1 "));
        out.println(F("\t '2': Log data SD 2 "));
        out.println(F("\t 'c': Clear Memory "));
        out.println(F("\t 'f': Free RAM "));
        out.println(F("\t 'z': Sleep 20 seconds "));
        out.println(F("\t 'l': Flash LED "));
        out.println();
      } break;
    case 'd': {
        int i = getDepth(20.0);
        out.print(F(" Depth : "));
        out.println(i);
      } break;
#ifdef USE_IRIDIUM
    case 'i': {
        testIridium();
      } break;
    case 'y': {
        getIridiumTime();
      } break;
#endif
    case 'c': {
        //EEPROM_1.erase();
      } break;
    case 'm': {
        readMemory();
      } break;
#ifdef USE_GPS
    case 'g': {
        getGPS();
      } break;
#endif
    case 'f': {
        freeRam();
      } break;
#ifdef WIFI_MODEM
    case 's': {
        sendData(false);
      } break;
    case 'x': {
        sendData(true);
      } break;
#endif
#ifdef USE_SD
    case 'a': {
      testSD(1);
      testSD(2);
    } break;
    case '1': {
        writeDataSD(1);
      } break;
    case '2': {
        writeDataSD(2);
      } break;
#endif
    case 't': {
        time_t t_rtc = RTC.get();
        displayTime(t_rtc);
      } break;
    case 'p': {
        updateXData(false);
      } break;
    case 'w': {
        writeDataEEPROM();
      } break;
    case 'z': {
        gotoSleep();
      } break;
    case 'l': {
      flashLED();
    } break;
    //---------------------------------------------------------------------
    default: {
        if (strlen(command) > 0) {
          out.print(F("command '"));
          out.print(command);
          out.println(F("' not recognized. "));
        }
        pInstance->get_command_input_pointer()[0] = '?';
        pInstance->set_flag_EOC(true);
      }
  } // end switch

}
#endif
void flashLED() {
  digitalWrite(13, HIGH);
  delay(700);
  digitalWrite(13, LOW);
  delay(700);
}
void setup() {
  int error =0;
  setupPins();
  //turn on all peripherals
  digitalWrite(EN_MODEM, LOW);
  digitalWrite(EN_GPS, LOW);  
  digitalWrite(EN_SD1, LOW);
  digitalWrite(EN_SD2, LOW);
  digitalWrite(EN_EEPROM, LOW);
  digitalWrite(EN_SENSE, LOW);
  digitalWrite(EN_SNOW, LOW);
  digitalWrite(EN_WIND, LOW);  
  
  pinMode(13, OUTPUT);
  
  Serial.begin(9600);
  //Sensors on I2C
  #ifdef USE_BME
  error+=setupSensor();
  #endif
  // set the RTC from the compile date
  initialiseRTC();
  //Iridium: start the serial port connected to the satellite modem
#ifdef USE_IRIDIUM
  IM1.begin(19200);
  error+=testIridium();
#endif
#ifdef USE_GPS
  NEO1.begin(9600);
#endif
#ifdef USE_EEPROM
  error+=setupEEPROM();
#endif
  // Initialize SD.
#ifdef USE_SD
  //testSD();
  SdFile::dateTimeCallback(dateTime);
#endif
  //start the menu
#ifdef USE_MENU
  menu.set_callback(mainMenu);
  Serial.println(F(" --------  Station-X POST -----------"));
  menu.begin(true);
#endif
}
void loop() {
  flashLED();
  #ifdef USE_MENU
  menu.update();
#else
  freeRam();
  

#ifdef USE_IRIDIUM
  testIridium();
  getIridiumTime();
#endif
#ifdef USE_EEPROM
  //EEPROM_1.erase();
  readMemory();
#endif
#ifdef USE_GPS
  getGPS();
#endif
#ifdef WIFI_MODEM
  sendData(false);
  sendData(true);
#endif
#ifdef USE_SD
  int err = 0;
  //err +=testSD(1);
  //err +=testSD(2);
  err +=writeDataSD(1);
  err +=writeDataSD(2);
  Serial.print(F("SD Errors: "));
  Serial.println(err);
#endif
  time_t t_rtc = RTC.get();
  displayTime(t_rtc);
  updateXData(false);
  writeDataEEPROM();

#endif //end menu


}
