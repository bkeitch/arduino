//int int_array[54] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
//0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int int_array[23] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                      0 };
const int MAX_INPUT_SIZE = 50;     // Maximum size of the input string
char inputBuffer[MAX_INPUT_SIZE];  // Buffer to store incoming characters
int bufferIndex = 0;               // Index to track the buffer position

void setup() {
  Serial.begin(9600);
  for (int ii = 2; ii <= sizeof(int_array) / sizeof(int_array[0]); ii++) {
    pinMode(ii, OUTPUT);
  }
  Serial.print("Welcome\nEnter Pin number:\n ");
}
void query() {
  for (int k = 3; k <= 10; k++) {
    Serial.print(k - 3);
    if (int_array[k] == 0) {
      Serial.println(" OFF");
    } else {
      Serial.println(" ON");
    }
  }
}
void reset() {
  Serial.println("RESETTING");
  for (int k = 3; k <= 10; k++) {
    int_array[k] = 0;
    digitalWrite(k, int_array[k]);
  }
}
void set(int pin_num) {
  pin_num +=3;
  int_array[pin_num] = !int_array[pin_num];
  digitalWrite(pin_num, int_array[pin_num]);
  Serial.print(pin_num - 3);
  if (int_array[pin_num] == 0) {
    Serial.println(" OFF");
  } else {
    Serial.println(" ON");
  }
}
void pulse() {
  int pin_num =7;
  digitalWrite(pin_num, HIGH);
  Serial.println("PULSE 4");
  digitalWrite(pin_num, LOW);
  
}
void loop() {
  // Check for incoming serial data
  if (Serial.available() > 0) {
    char receivedChar = Serial.read();  // Read a character from Serial

    if (receivedChar == '\n') {         // Check if newline character is received
      inputBuffer[bufferIndex] = '\0';  // Null-terminate the string
      processCommand(inputBuffer);      // Process the command
      bufferIndex = 0;                  // Reset buffer for the next input
    } else if (bufferIndex < MAX_INPUT_SIZE - 1) {
      inputBuffer[bufferIndex++] = receivedChar;  // Store character in buffer
    } else {
      Serial.println("Input too long! Ignoring...");
      bufferIndex = 0;  // Reset buffer if overflow
    }
  }
}

void processCommand(const char *command) {
  // Match the input string to a specific command and call the appropriate function
  if (strncmp(command, "?", 1) == 0) {
    query();
  } else if (strncmp(command, "s", 1) == 0) {
    set(parseNumberFromString(command));
  } else if (strncmp(command, "p", 1) == 0) {
    pulse();
  }  else if (strncmp(command, "r", 1) == 0) {
    reset();
  } else {
    Serial.println("Unknown command!");
  }
}
int parseNumberFromString(const char *input) {
  // Check for null or short input
  if (input == nullptr || strlen(input) < 3) {
    return 0;  // Return 0 for invalid input
  }

  // Verify the format: first character followed by a space
  if (input[1] != ' ') {
    return 0;  // Return 0 if there's no space after the first character
  }

  // Extract the substring after the first character and space
  const char *numberPart = input + 2;

  // Convert the extracted substring to an integer
  return atoi(numberPart);
}
