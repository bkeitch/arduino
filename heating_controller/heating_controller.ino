/**
 * 
 * Switches off and on central heating relays and hotwater relays using 
 * Arduino NANO (328P, old bootloader)
 * 
 * Pins: D0 RX D1 TX D2 SQW/INT D3 TX_EN D4 HWL D5 CHL D6 CHR D7 HWR D8-D13 NC
 * A0 T1 A2 T2 A2 T3 A3 NC A4 SDA A5 SCL A6 HWI A7 CHI
 * 
 * */

int int_array[] = {0,0,0,0,0,0,0,0};//D0-D7
String inputString = "";      // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

#include <DS3232RTC.h>    // v2.0.1 https://github.com/JChristensen/DS3232RTC
#include <Streaming.h>    // v6.1.1 https://github.com/janelia-arduino/Streaming allows << operator
#include <TimeLib.h>      // https://github.com/PaulStoffregen/Time

const uint8_t RTC_IRQ(2);
const uint8_t TX_EN(3);
DS3232RTC RTC0;
void printDateTime(time_t t)
{
    Serial << ((day(t)<10) ? "0" : "") << _DEC(day(t));
    Serial << ' ' << monthShortStr(month(t)) << ' ' << _DEC(year(t)) << ' ';
    Serial << ((hour(t)<10) ? "0" : "") << _DEC(hour(t)) << ':';
    Serial << ((minute(t)<10) ? "0" : "") << _DEC(minute(t)) << ':';
    Serial << ((second(t)<10) ? "0" : "") << _DEC(second(t));
}

// function to return the compile date and time as a time_t value
time_t compileTime()
{
    const time_t FUDGE(10);    //fudge factor to allow for upload time, etc. (seconds, YMMV)
    const char *compDate = __DATE__, *compTime = __TIME__, *months = "JanFebMarAprMayJunJulAugSepOctNovDec";
    char compMon[4], *m;

    strncpy(compMon, compDate, 3);
    compMon[3] = '\0';
    m = strstr(months, compMon);

    tmElements_t tm;
    tm.Month = ((m - months) / 3 + 1);
    tm.Day = atoi(compDate + 4);
    tm.Year = atoi(compDate + 7) - 1970;
    tm.Hour = atoi(compTime);
    tm.Minute = atoi(compTime + 3);
    tm.Second = atoi(compTime + 6);

    time_t t = makeTime(tm);
    return t + FUDGE;        //add fudge factor to allow for compile time
}
void wakeUp() 
{
  int_array[5] = !int_array[5];
  digitalWrite(5,int_array[5]);
  RTC0.alarm(DS3232RTC::ALARM_1);
}

void setup() 
{
  Serial.begin(9600);
  for (int ii = 4;ii<8;ii++){
    pinMode(ii,OUTPUT);
  }
  pinMode(RTC_IRQ, INPUT_PULLUP);     // enable pullup on interrupt pin (RTC SQW pin is open drain)
  //pinMode(TX_EN,OUTPUT);
  //digitalWrite(TX_EN, LOW); //rx mode
  RTC0.setAlarm(DS3232RTC::ALM1_MATCH_DATE, 0, 0, 0, 1);
  RTC0.setAlarm(DS3232RTC::ALM2_MATCH_DATE, 0, 0, 0, 1);
  RTC0.alarm(DS3232RTC::ALARM_1);
  RTC0.alarm(DS3232RTC::ALARM_2);
  RTC0.alarmInterrupt(DS3232RTC::ALARM_1, false);
  RTC0.alarmInterrupt(DS3232RTC::ALARM_2, false);
  RTC0.squareWave(DS3232RTC::SQWAVE_NONE);//SQWAVE_1_HZ
  
  // set the RTC time and date to the compile time
  printDateTime(compileTime());
  RTC0.set(compileTime());
  
  // set Alarm 1 to occur at 5 seconds ~~after every minute~~
  RTC0.setAlarm(DS3232RTC::ALM1_EVERY_SECOND, 5, 0, 0, 0);
  // clear the alarm flag
  RTC0.alarm(DS3232RTC::ALARM_1);
  attachInterrupt(digitalPinToInterrupt(RTC_IRQ), wakeUp, FALLING);
  RTC0.alarmInterrupt(DS3232RTC::ALARM_1, true);
 
  Serial << millis() << " Start ";
  printDateTime(RTC0.get());
  Serial << endl;
  delay(300);


}
unsigned long millisLast;
void loop() {

  if (millis() - millisLast > 10000) {
    millisLast = millis();
    Serial << "keep alive" << endl;
    
  }
  if (stringComplete) {
//digitalWrite(TX_EN, HIGH); //tx mode
    Serial.println(inputString);//echo back
    int pin_num = inputString.toInt();
    if(pin_num > 3 && pin_num < 8) {
        int_array[pin_num] = !int_array[pin_num];
        digitalWrite(pin_num,int_array[pin_num]);
    }
    switch (pin_num) {
      case 4:
        Serial.print("CH LED ");Serial.println(int_array[pin_num]);
        break;
      case 5:
        Serial.print("HW LED ");Serial.println(int_array[pin_num]);
        break;
      case 6:
        Serial.print("CH ");Serial.println(int_array[pin_num]);
        break;
      case 7:
        Serial.print("HW ");Serial.println(int_array[pin_num]);
        break;
      case 8:
        Serial.print("HW CURRENT "); Serial.println(analogRead(A6));
        break;
      case 9:
        Serial.print("CH CURRENT "); Serial.println(analogRead(A7));
        break;
      case 10:
        Serial.print("TIME "); printDateTime(RTC0.get());
     default:
       Serial.print("CMD "); Serial.println(pin_num);
    } //end switch
//digitalWrite(TX_EN, LOW); // back to RX
    inputString = "";
    stringComplete = false;
  }//end string complete
}
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
