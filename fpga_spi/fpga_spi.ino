/*

 The circuit:

  * CS - to digital pin 10  (SS pin)
  * SDI - to digital pin 11 (MOSI pin)
  * CLK - to digital pin 13 (SCK pin)


*/


// inslude the SPI library:
#include <SPI.h>
  SPISettings FPGASettings(2000000, MSBFIRST, SPI_MODE1); 

// set pin 10 as the slave select 
const int slaveSelectPin = 10;

void setup() {
  // set the slaveSelectPin as an output:
  pinMode(slaveSelectPin, OUTPUT);
  // initialize SPI:

  SPI.begin();
}

void loop() {
  // go through the six channels of the digital pot:
    for (byte level = 0; level < 256; level+=32) {
      fpgaWrite(0, 0, level);
      delay(1000);
    }

  delay(1000);
  for (byte level = 0; level < 256; level+=32) {
      fpgaWrite(0, level, 0);
      delay(1000);
    }
    for (byte level = 0; level < 256; level+=32) {
      fpgaWrite(0, level, level);
      delay(1000);
    }
    
}

void fpgaWrite(byte red,  byte green, byte blue) {
  SPI.beginTransaction(FPGASettings);
  // take the SS pin low to select the chip:
  digitalWrite(slaveSelectPin, LOW);
  //delay(100);
  //  send in the address and value via SPI:
  SPI.transfer(red);
  SPI.transfer(green);
  SPI.transfer(blue);
  //delay(100);
  // take the SS pin high to de-select the chip:
  digitalWrite(slaveSelectPin, HIGH);
  SPI.endTransaction();
}
