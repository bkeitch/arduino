/*
 * Arduino GPS to 7 seg display
 * Assumes NMEA GPS device
 * Uses TinyGPS library (needs to be installed)
 * outputs on 7 seg LEDS
 * Currently using 5V system from http://store.linksprite.com/gps-shield-with-sd-slot-for-arduino-v2-b/
 */

#include <TinyGPS.h>
#include <SoftwareSerial.h>

SoftwareSerial gpss(2, 3); //RX TX
bool gpsPower = false;

static const int gpsCTRL = 6;

 

// Define the LED digit patters, from 0 - 9
// Note that these patterns are for common cathode displays
// For common anode displays, change the 1's to 0's and 0's to 1's
// 1 = LED on, 0 = LED off, in this order:
// http://www.hacktronics.com/Tutorials/arduino-and-7-segment-led.html
// License: http://www.opensource.org/licenses/mit-license.php (Go crazy)
//                                    Arduino pin: 2,3,4,5,6,7,8
byte seven_seg_digits[10][7] = { { 1,1,1,1,1,1,0 },  // = 0
                                 { 0,1,1,0,0,0,0 },  // = 1
                                 { 1,1,0,1,1,0,1 },  // = 2
                                 { 1,1,1,1,0,0,1 },  // = 3
                                 { 0,1,1,0,0,1,1 },  // = 4
                                 { 1,0,1,1,0,1,1 },  // = 5
                                 { 1,0,1,1,1,1,1 },  // = 6
                                 { 1,1,1,0,0,0,0 },  // = 7
                                 { 1,1,1,1,1,1,1 },  // = 8
                                 { 1,1,1,0,0,1,1 }   // = 9
                                 };
TinyGPS gps;
unsigned long lat, lon;
unsigned int speed_kph;
void setup()
{
  lat = 50000000;
  lon = 2000000;
  speed_kph = 5;
  pinMode(gpsCTRL, OUTPUT);
  pinMode(A2, OUTPUT);  //2 used by GPS TX
  pinMode(A3, OUTPUT);  //3 used by GPS RX
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(A0, OUTPUT); //6 used by GPS CTL
  pinMode(7, OUTPUT);
  pinMode(A1, OUTPUT);  //9 is used by SD card
// 4 common anode (+) driver by 74LS05 Hex inverters with open collector outputs
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT); //11 is MOSI on SD card and tied low
  pinMode(10, OUTPUT);
  pinMode(12, OUTPUT);
  //  swOffGPS(); // GPS is active low, so as soon as pin is set, switch it off.
  Serial.begin(115200);
  
  swOnGPS();
  delay(1000); // warm up GPS
  testDigits();
//writeDot(0);  // start with the "dot" off
}
/*
 * Outputs integer values on 7 seg display. Using multiple digits and "persistence of vision" POV technique
 */
void showDigits (int number)
{

  // e.g. we have "1234"
  sevenSegWrite(number/1000);  // segments are set to display "1"
  digitalWrite(8, HIGH); // first digit on,
  digitalWrite(9, LOW); // other off
  digitalWrite(10, LOW);
  digitalWrite(12, LOW);
  
  delay (1);
  
  number = number%1000;  // remainder of 1234/1000 is 234
  digitalWrite(8, LOW); // first digit is off
  sevenSegWrite(number/100); //// segments are set to display "2"
  digitalWrite(9, HIGH); // second digit is on
  delay (1); // and so on....
  
  number =number%100;    
  digitalWrite(9, LOW);
  sevenSegWrite(number/10);
  digitalWrite(10, HIGH);
  delay (1);
  
  number =number%10; 
  digitalWrite(10, LOW);
  sevenSegWrite(number); 
  digitalWrite(12, HIGH);
  delay (1);

}

/* The getgps function will interpret data from GPS (NMEA) and display on monitor 
 * retrieves +/- lat/long in 1000,000ths of a degree
 * Output diagnostics including float values lat and lon and satellite status
*/
bool getgps()
{
  unsigned long fix_age;
  float flat, flon;
  gps.f_get_position(&flat, &flon, &fix_age);
  Serial.print("LAT=");
  Serial.print(flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat, 6);
  Serial.print(" LON=");
  Serial.print(flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon, 6);
  Serial.print(" SAT=");
  Serial.print(gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
  Serial.print(" PREC=");
  Serial.print(gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
  unsigned long chars;
  unsigned short sentences, failed;
  gps.stats(&chars, &sentences, &failed);
  Serial.print(" CHARS=");
  Serial.print(chars);
  Serial.print(" SENTENCES=");
  Serial.print(sentences);
  Serial.print(" CSUM ERR=");
  Serial.println(failed);
  if (chars == 0)
    Serial.println("** No characters received from GPS: check wiring **");
  return true;
}
  
void sevenSegWrite(byte digit) {
  byte pin = 2;
  for (byte segCount = 0; segCount < 7; ++segCount) {
    if(pin == 6) //used by gps
    digitalWrite(A0, seven_seg_digits[digit][segCount]);
    else if(pin == 8) //used by gps
    digitalWrite(A1, seven_seg_digits[digit][segCount]);
    else if(pin == 2) //used by gps
    digitalWrite(A2, seven_seg_digits[digit][segCount]);
    else if(pin == 3) //used by gps
    digitalWrite(A3, seven_seg_digits[digit][segCount]);
    else
    digitalWrite(pin, seven_seg_digits[digit][segCount]);
    ++pin;
  }
}



void loop()
{
   unsigned long fix_age;
  
   while (gpss.available())
   {
      char c = gpss.read();
      //Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c)) // Did a new valid sentence come in?
      {
          gps.get_position(&lat, &lon, &fix_age);
          *speed_kph = (unsigned int) (ceil) (18.52* gps.f_speed_knots());        
          int j=0;
          while (j < 10) { // one-sec loop to show new data
            int i = 0;
            while(i<500) { //half sec lat
             showDigits((int) (lat/100000.0));
             i++;
            }
            i = 0;
            while(i<500) {//half sec lon
             showDigits((int) (lon/100000.0));
             i++;
            }
            i = 0;
            while(i<1000) {//1 sec speed
             showDigits(speed_kph);
             i++;
            }
          }          
      }
    }

}
void toggleGPS() {
  if(gpsPower == false)
    swOnGPS();
  else
    swOffGPS();
}
/**
 * GPS module has active LOW - see datasheet.
 */
void swOnGPS()
{
  gpss.begin(9600);
  digitalWrite(gpsCTRL, LOW);
  gpsPower = true;
}
void swOffGPS()
{
  gpss.end();
  digitalWrite(gpsCTRL, HIGH);
  gpsPower = false;
}



void testDigits() {
  for (int count = 10; count > 0; --count) {
    int i = 0;
    while(i<100) {
     showDigits((count - 1)*1000 + (count - 1)*100 +(count - 1)*10 +(count - 1));
     i++;
    }
  }
  //flashDot();
}
