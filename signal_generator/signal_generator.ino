#include <AD9833.h>     // Include the library
/*
DAT -> Arduino Pin 11 // SPI Data pin

CLK -> Arduino Pin 13 // SPI Clock pin

FSY -> Arduino Pin 4 // SPI Load pin (FSYNC in AD9833 terminology)

(and GND!)
*/
#define FNC_PIN 4       // Can be any digital IO pin

//--------------- Create an AD9833 object ---------------- 
// Note, SCK and MOSI must be connected to CLK and DAT pins on the AD9833 for SPI
AD9833 gen(FNC_PIN);       // Defaults to 25MHz internal reference frequency
//-----------------------------------------------------------------------------
// InitSigGen
//-----------------------------------------------------------------------------

void InitSigGen() {
    // This MUST be the first command after declaring the AD9833 object
    gen.Begin();              
    gen.DisableInternalClock(false); //use internal clock
    // Apply a 1 MHz square wave using REG0 (register set 0). There are two register sets,
    // REG0 and REG1. 
    // Each one can be programmed for:
    //   Signal type - SINE_WAVE, TRIANGLE_WAVE, SQUARE_WAVE, and HALF_SQUARE_WAVE
    //   Frequency - 0 to 12.5 MHz
    //   Phase - 0 to 360 degress (this is only useful if it is 'relative' to some other signal
    //           such as the phase difference between REG0 and REG1).
    // In ApplySignal, if Phase is not given, it defaults to 0.
    gen.ApplySignal(SQUARE_WAVE,REG0,10000000);

   
    gen.EnableOutput(true);   // Turn ON the output - it defaults to OFF
    // There should be a 1 MHz square wave on the PGA output of the AD9833
}




//-----------------------------------------------------------------------------
// SerialCommand
//   if a byte is available in the serial input buffer
//   execute it as a command
//-----------------------------------------------------------------------------
  bool input = false;
  float freq = 30000;
  char c = '\0';
  String waveType = "None";
void SerialCommand(void) {
  while ( Serial.available() > 0 ) {
    
    if(input && c!= '\n') {
      freq = Serial.parseFloat(); 
    } else {
      //setBinary(freq);
      Serial.println(freq);
      input = false;
    }
    c = Serial.read();
    if(c>='a' && c<='z') {
        c-=32;
    }
    switch (c) {
      case 'F': input = true; break;
      case 'S': waveType = "Sine";gen.ApplySignal(SINE_WAVE,REG0,freq); break;   // SigGen wave is sine
      case 'T': waveType = "Triangle"; gen.ApplySignal(TRIANGLE_WAVE,REG0,freq); break;   // SigGen wave is triangle
      case 'Q': waveType = "Square"; gen.ApplySignal(SQUARE_WAVE,REG0,freq); break;   // SigGen wave is square
      //case 'R': SG_Reset();  break;   // SigGen reset
      //case 'M': for (int i=0; i<=5; i++) freqSGHi[i] = freqSGLo[i]; break;   // move freq to high array
      //case 'G': Sweep(1000);  break; // sweep SigGen
      //case 'H': Sweep(5000);  break; // sweep SigGen
      //case 'I': Sweep(20000);  break; // sweep SigGen
      case 'A': report() ; break;
      default: return;
    }
  }
}


void report() {
   Serial.print("Frequency : ");
   float f = gen.GetActualProgrammedFrequency (REG0);
   Serial.println(f);
   Serial.print("Mode : ");
   Serial.println(waveType);
}
#define BAUDRATE 9600

//-----------------------------------------------------------------------------
// Main routines
// The setup function
//-----------------------------------------------------------------------------
void setup (void) {
  // Open serial port with a baud rate of BAUDRATE b/s
  Serial.begin(BAUDRATE);

  Serial.println("SigGen " __DATE__); // compilation date
  Serial.println("OK");

  pinMode(LED_BUILTIN, OUTPUT);

  InitSigGen();
}

//-----------------------------------------------------------------------------
// Main routines
// loop
//-----------------------------------------------------------------------------
void loop (void) {
  SerialCommand();
}
