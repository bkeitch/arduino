/*
 * Station X Power On Self Test

  BCK 2020

  Hardware: Mega

  I2C bus (21 SCL 20 SDA)
  =======================
  RTC : DS3231  0x68
  Sensor: BMPE280
  FLASH1
  FLASH2

  Analogue
  ========
  Pressure: MPX4115 at 5V on A0
  Battery Voltage A1
  Solar voltage A2
  Battery current A3
  Internal temp: A4
  Battery temp: A6

  Serial
  ======
  GPS: SS Serial1 RX 19 TX 18 PWR D6
  Iridium Modem: Serial3 PWR D48

  SPI pins 10-13 (Purple on 13)
  =============================

  Digital:
  SD Card 1 SS : D53
  SD Card 2 SS : D49
  PSU 1 :  D5
  PSU 2 :  D6
  Interupt (Wake) : D4
*/
#include <SPI.h>
#include <DS3232RTC.h>  //RTC Library https://github.com/JChristensen/DS3232RTC
#include <Wire.h>
#include <avr/sleep.h>          //contains sleep modes

#define DS3231_I2C_ADDRESS 0x68
#define SD_1_CS_PIN   49
#define SD_2_CS_PIN   53
#define wake_up       2 //Pin from RTC (has to be 2 or 3)
#define CTS_PIN       27 //Pin to wake modem


// Declare the Humdity/Pressure/Temperature object
#include <BME280I2C.h>
#include <MsgPack.h>


BME280I2C bme;    // Default : forced mode, standby time = 1000 ms
                  // Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,

#define USE_MINUTES 1
const bool DEBUG = true;
const bool DEBUG_RTC = true;
#include <Base64.h> //https://www.arduino.cc/reference/en/libraries/base64/
const int time_interval = 30;
#include "x_data.h"

x_data data;
void displayTime(time_t t);
static const int months[] = { 281 ,269 ,288 ,291 ,295 ,301 ,299 ,285 ,296 ,294 ,307 ,268 };
/**
  * sets initial RTC time to compiled time if the
  * RTC has not already been initialised. Should 
  * only run once, when first powered up or after battery 
  * failure
*/
void initialiseRTC() 
 { 
    int Hour, Minute, Second, Day, Month, Year;
    time_t t_cp, t_rtc;
    
 
    char time_c[] = __TIME__;
    Second = atoi(time_c + 6);
    *(time_c + 5) = 0;
    Minute = atoi(time_c + 3);
    *(time_c + 2) = 0;
    Hour = atoi(time_c);
    
    char temp[] = __DATE__;
    int i;
    int sum_out;
    Year = atoi(temp + 9); //ref 1970
    *(temp + 6) = 0;
    Day = atoi(temp + 4);
    *(temp + 3) = 0;
    Month = 0;
    for (i = 0; i < 12; i++) {
        sum_out = (temp[0] + temp[1] + temp[2]);
        if (months[i] == sum_out) {
            Month = i + 1;
            break;
        }
    }
    setTime(Hour, Minute, Second, Day, Month, Year);
    t_cp = now();
    t_rtc = RTC.get();
    if(DEBUG_RTC) {
      Serial.println("setting time");
        displayTime(t_cp);
        displayTime(t_rtc);
    }
    if ((t_rtc < t_cp) || (t_rtc - t_cp > 157788000.0)) // 5 years
        RTC.set(t_cp);
    else if(DEBUG_RTC)
         Serial.println("No need to set time");
    
    //RTC.setAlarm(ALM1_MATCH_DATE, 0, 0, 0, 1);
    RTC.setAlarm(ALM2_MATCH_DATE, 0, 0, 0, 1);
    RTC.alarm(ALARM_1);
    RTC.alarm(ALARM_2);
    //RTC.alarmInterrupt(ALARM_1, false);
    RTC.alarmInterrupt(ALARM_2, false);
    RTC.squareWave(SQWAVE_NONE);
    // Setting alarm 1 every 20 minutes
    #ifdef USE_MINUTES
    RTC.setAlarm(ALM1_MATCH_MINUTES , 0, (minute(t_rtc) + time_interval) % 60, 0, 0); 
    #else
    RTC.setAlarm(ALM1_MATCH_SECONDS, (second(t_rtc) + time_interval) % 60, 0, 0, 0);
    #endif
    // clear the alarm flag
    RTC.alarm(ALARM_1);
    // enable interrupt output for Alarm 1
    RTC.alarmInterrupt(ALARM_1, true);
}
/**
 * Initialises the RTC on first power-up with compile time. This assumes
 * the RTC has not been programmed previously (e.g. power loss)
 * it also  
 * initialises the alarms to known values, clears the alarm flags, clears the alarm interrupt flags
 * and configures the INT/SQW pin for "interrupt" operation (disable square wave output)
*/ 
void displayTime(time_t t)
{
    Serial.print(year(t));
    Serial.print("-");
    if (month(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(month(t));
    Serial.print("-");
    if (day(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.println(day(t));
    if (hour(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(hour(t));
    Serial.print(":");
    if (minute(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.print(minute(t));
    Serial.print(":");
    if (second(t) <= 9)
        Serial.print("0");      // adjust for 0-9
    Serial.println(second(t));
}
/**
 * Get's up-to-date parameters from sensors
 */
void updateXData() {
  unsigned int mem = 64;
  float bme_p, bme_t, bme_u;
  bme.read(bme_p, bme_t, bme_u, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
  time_t t = RTC.get();
 
  data.update(bme_t, bme_p, bme_u, t, mem);
}
void setup() { 
    pinMode(CTS_PIN, OUTPUT);
     //switch off modem
    digitalWrite(CTS_PIN, 1);
    //intConf = conf.getConfig();
    pinMode(wake_up, INPUT_PULLUP);    //Set pin input using the built-in pullup resistor
    pinMode(SD_1_CS_PIN, OUTPUT);
    digitalWrite(SD_1_CS_PIN, 0);
    pinMode(SD_2_CS_PIN, OUTPUT);
    digitalWrite(SD_2_CS_PIN, 0);
    
    
    pinMode(gpsCTRL, OUTPUT);
    pinMode(ST_1_PIN, INPUT);  
    pinMode(ST_2_PIN, INPUT);

    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);

    Serial3.begin(9600);
    if (DEBUG)
        Serial.begin(9600);
    //FLASH memory
    Wire.begin();
    /*
    Wire.setClock(400000);      // EEPROMs can run 400kHz and higher
    if (myMem.begin(EEPROM_0_ADR_LOW_BLOCK) == false) {
        if (DEBUG)
            Serial.println("No memory detected.");
        criticalFailure = true;
    } else {
        if (DEBUG)
            Serial.println("Memory detected!");
        //Set settings for this EEPROM
        myMem.setMemorySize(262144);    //In bytes. 1024kbits x 2 blocks.
        myMem.setPageSize(128); //In bytes. Has 128 byte page size.
        myMem.enablePollForWriteComplete();     //Supports I2C polling of write completion
        myMem.setPageWriteTime(3);      //3 ms max write time
        if (DEBUG)
            Serial.print("Mem size in bytes: ");
        if (DEBUG)
            Serial.println(myMem.length());
    }*/
    
    //Sensors on I2C
    unsigned int i =0;
    while(!bme.begin() && i < 10)
    {
      if (DEBUG)
        Serial.println("Looking for BME280 sensor!");
      delay(1000);
      ++i;
    }
     if (DEBUG && i == 10)
        Serial.println("Could not find BME280 sensor!");
    switch (bme.chipModel()) {
    case BME280::ChipModel_BME280:
        if (DEBUG)
            Serial.println("Found BME280 sensor! Success.");
        break;
    case BME280::ChipModel_BMP280:
        if (DEBUG)
            Serial.println("Found BMP280 sensor! No Humidity available.");
        break;
    default:
        if (DEBUG)
            Serial.println("Found UNKNOWN sensor! Error!");
    }
    // Initialize SD.
    #ifdef USE_SD
    if (!sd.begin(SD_CONFIG)) {
        if (DEBUG)
            Serial.println("sd.begin failed");
    }
    // Set callback
    FsDateTime::setCallback(dateTime);
    //createBinFile();
    if (DEBUG)
        sd.ls(&Serial, LS_DATE | LS_SIZE);
    #endif
    
    
    // set the RTC from the compile date
    initialiseRTC();
    
 
    //GPS
    #ifdef USE_GPS
    GPSSerial.begin(9600);      //GPS  init
    getGPSData();
    #endif

   

}


void loop()
{
    gotoSleep();

} 

/*
   Sleep cycle
*/ 
void gotoSleep()
{
    
    //    bool sendOut = (minute(t_rtc) % 10 == 0);
//    char message[250];
//    data.ppopulate();   
//    data.getData(message);
//        
    //count++;
    //wlog(message, false);
    digitalWrite(CTS_PIN, 0);
    delay(50);
    digitalWrite(CTS_PIN, 1);
    sendData();
    
    if (DEBUG)
        Serial.println("Sleep");// + String(hour(t)) + ":" + String(minute(t)) + ":" + String(second(t))); 
    sleep_enable();
    attachInterrupt(digitalPinToInterrupt(wake_up), wakeUp, LOW);       //attaching an interrupt to RTC
    set_sleep_mode(SLEEP_MODE_PWR_DOWN); 
    time_t t_rtc;                   // creates temp time variable

    if (DEBUG)
        Serial.flush();
    //delay(1000);
    sleep_cpu();                //activating sleep mode
    // ... sleeping ... //
    t_rtc = RTC.get();
    if (DEBUG)
        Serial.println("WakeUp");// + String(hour(t)) + ":" + String(minute(t)) + ":" + String(second(t))); 


    
    // Setting alarm 1 every 20 minutes
    #ifdef USE_MINUTES
    RTC.setAlarm(ALM1_MATCH_MINUTES , 0, (minute(t_rtc) + time_interval) % 60, 0, 0); 
    #else
    RTC.setAlarm(ALM1_MATCH_SECONDS, (second(t_rtc) + time_interval) % 60, 0, 0, 0);
    #endif
    RTC.alarm(ALARM_1);

}

/*
   Routine fired by interupt
*/ 
void wakeUp()
{
    sleep_disable(); 
    detachInterrupt(digitalPinToInterrupt(wake_up));    //Removes the interrupt from pin 2;
}
static const int timeout = 20000;

void packData() {
  char cbuffer [200];
  
  MsgPack::Packer packer;
  packer.serialize(data);
  //uint8_t output_buffer[100];
  
  //    data.getJSONData(buffer);
  //    if(DEBUG) Serial.println(buffer);  
  //unsigned int len = encode_base64(packer.data(), packer.size(), output_buffer);
  int len = Base64.encode(cbuffer, (char*) packer.data(), (int) packer.size());
  if(DEBUG) Serial.println(len);  
  Serial3.println((char *) cbuffer);
  if(DEBUG) Serial.println((char *) cbuffer);  
  //Serial3.flush();
}
void sendData() 
{
  updateXData();
  boolean sent = false;
  // read for incoming messages. c = send, x = don't send:
  long last = millis();
  while (!sent && (timeout > millis() - last) ) {
    while (Serial3.available()) {
      char inChar = Serial3.read() ;
      if(DEBUG) Serial.println(inChar, HEX);
      switch (inChar) {
      case 'c':    // connection open
        packData();
        break;
      case 'x':    // connection closed
        sent = true;
        break;
      default:
      break;    
      }
    }
  }
}
