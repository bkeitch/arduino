/*
  Debounce

  Each time the input pin goes from LOW to HIGH (e.g. because of a push-button
  press), the output pin is toggled from LOW to HIGH or HIGH to LOW. There's a
  minimum delay between toggles to debounce the circuit (i.e. to ignore noise).

  The circuit:
  - LED attached from pin 13 to ground through 220 ohm resistor
  - pushbutton attached from pin 2 to +5V
  - 10 kilohm resistor attached from pin 2 to ground

  - Note: On most Arduino boards, there is already an LED on the board connected
    to pin 13, so you don't need any extra components for this example.

  created 21 Nov 2006
  by David A. Mellis
  modified 30 Aug 2011
  by Limor Fried
  modified 28 Dec 2012
  by Mike Walters
  modified 30 Aug 2016
  by Arturo Guadalupi

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Debounce
*/

// constants won't change. They're used here to set pin numbers:
const int buttonPin = 3;  // the number of the pushbutton pin
const int ledPin = 8                                                                                                                                                                                ;    // the number of the LED pin

// Variables will change:
int ledState = HIGH;        // the current state of the output pin
int buttonState;            // the current reading from the input pin
int lastButtonState = LOW;  // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers


long randNumber;                                                            // the variable which is supposed to hold the random number
const int analogOutPin = 6;                                                 // Analog output pin where the LED is attached to

void setup() {
  pinMode(buttonPin, INPUT);
  pinMode(ledPin, OUTPUT);
  pinMode(ledPin-1, OUTPUT);
  pinMode(ledPin-2, OUTPUT);
  pinMode(ledPin-3, OUTPUT);
 randomSeed(analogRead(0));
  // set initial LED state
  digitalWrite(ledPin, ledState);
}

void loop() {
    // read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
        ledState = !ledState;
        flash();
      }
    }
  }

  // set the LED:


  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
}
  int leds[4] = {8,7,6,5};
void flash () {
  for(int i=0; i< 10; i++) {
    digitalWrite(leds[random(0,sizeof(leds)/sizeof(int))],HIGH);
    delay(random(10,100));
    digitalWrite(leds[random(0,sizeof(leds)/sizeof(int))],LOW);
  }
  digitalWrite(ledPin, LOW);
  digitalWrite(ledPin-1, LOW); 
  digitalWrite(ledPin-2, LOW);
  digitalWrite(ledPin-3, LOW);
  randNumber = random(0, 2);
  if(randNumber <1.0 ) {
    digitalWrite(ledPin, HIGH);
    digitalWrite(ledPin-1, HIGH);
    randomSeed(analogRead(1));
  } else {
    digitalWrite(ledPin-2, HIGH);
    digitalWrite(ledPin-3, HIGH);
    randomSeed(analogRead(2));
  }
  delay(2000);
  digitalWrite(ledPin, LOW);
  digitalWrite(ledPin-1, LOW); 
  digitalWrite(ledPin-2, LOW);
  digitalWrite(ledPin-3, LOW);
}
