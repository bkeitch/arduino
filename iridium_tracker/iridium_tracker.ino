/*
 * Uses Maplin GPS to acquire fix on press of a button
 * Then transmits over Iridium
 */
#include <TinyGPS.h>
#include <SoftwareSerial.h>
//#include <SPI.h>
//#include <SD.h>
//#include "SdFat.h"
#include <IridiumSBD.h>

SoftwareSerial inss(4, 5);
IridiumSBD isbd(inss, 7);
int btnVal;
bool gpsPower = false;

static const int ledPin = 9;
//static const int btnPIN = 7;
static const int LED_RED = 12;
static const int LED_GREEN = 10;
static const int LED_YELLOW = 9;
static const int btnPIN = 8;
static const int gpsCTRL = 6;
static const char defaultMessage[] = "Position update for S/Y Flutterby. No status message available.";
static const int maxChars = 62; // limit of Iridium/Rock block
char message[maxChars+1]; // must be big enough for digits and terminating null
int index = 0;         // the index into the array storing the received digits
long lat, lon;
SoftwareSerial nss(2,3); // configure software serial port for GPS 

// Create an instance of the TinyGPS object
TinyGPS gps;

void setup()
{
  pinMode(btnPIN, INPUT); 
  pinMode(gpsCTRL, OUTPUT);
  swOffGPS(); // GPS is active low, so as soon as pin is set, switch it off.
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_YELLOW, OUTPUT);
  pinMode(LED_RED, OUTPUT);
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  isbd.attachConsole(Serial);
  isbd.setPowerProfile(1);
  Serial.println(F("Type your messsage now (max 62 characters) and press return :"));
  //Flash LEDs so we know they work
  digitalWrite(LED_RED, LOW);
  digitalWrite(LED_YELLOW, LOW);
  digitalWrite(LED_GREEN, LOW);
  delay(500);
  digitalWrite(LED_RED, HIGH);
  digitalWrite(LED_YELLOW, HIGH);
  digitalWrite(LED_GREEN, HIGH);
  swOnGPS();
  delay(1000); // warm up GPS

}

/* The getgps function will interpret data from GPS and display on myFile monitor */
bool getgps()
{
  unsigned long fix_age, gps_time, date;
  
  
  // retrieves +/- lat/long in 1000,000ths of a degree
  gps.get_position(&lat, &lon, &fix_age);
   
  // time in hhmmsscc, date in ddmmyy
  gps.get_datetime(&date, &gps_time, &fix_age);
  if (fix_age == TinyGPS::GPS_INVALID_AGE) {
    digitalWrite(LED_RED, LOW);    // red on = GPS fail
    Serial.println("No fix detected");    
    return false;
  } 
  digitalWrite(LED_GREEN, LOW);    // green on = GPS success
  return true;
}
bool sendMessage() {
//getgps();
//lat = 35102681;
//lon = -127856262;
  char userMsg[100];
  if(strlen(message) > 3) {
    sprintf(userMsg, "{\"p\":[%ld,%ld],\"m\":\"%s\"}", lat, lon, &message);
  } else {
    sprintf(userMsg, "{\"p\":[%ld,%ld],\"m\":\"%s\"}", lat, lon, &defaultMessage);
  }
  Serial.println("Sending: ");
  Serial.println(userMsg); 
  Serial.print(lat); 
  Serial.print(","); 
  Serial.print(lon);
  Serial.print(","); 
//  Serial.print(date); 
  Serial.print(","); 
//  Serial.println(gps_time);
  sendIridium(userMsg);
  return true;

}

typedef enum {
    BTN_STATE_RELEASED,
    BTN_STATE_PRESSED
} ButtonState;

ButtonState btnStatus = BTN_STATE_RELEASED;


void loop()
{
  while (nss.available() )
  {
    int c = nss.read();
    Serial.write(c);
    if (gps.encode(c))
      // process new gps info here
    {
      if( getgps() ) {
      //  success so switch off GPS
        swOffGPS();

      }
      digitalWrite(LED_GREEN, HIGH);
    }
  }
  if( Serial.available())
  {
    char ch = Serial.read();
    if(index <  maxChars && ch != '\r' && ch != '\n'){
      message[index++] = ch; // add the ASCII character to the string;
    }
    else
    {
      // here when buffer full or on the first non digit
      message[index] = 0;        // terminate the string with a 0
      //toggleGPS(); //fire sending straight away.
    }
  }
  btnVal = digitalRead(btnPIN);

  switch(btnStatus)
  {
  case BTN_STATE_PRESSED:
      // Handle button release
      if(btnVal == HIGH)
      {
          btnStatus = BTN_STATE_RELEASED;
      }

      break;

  case BTN_STATE_RELEASED:
      // Handle button press
      if(btnVal == LOW)
      {
          //toggleGPS();
          sendMessage();
          btnStatus = BTN_STATE_PRESSED;
      }
      break;
  }
}
void toggleGPS() {
  if(gpsPower == false)
    swOnGPS();
  else
    swOffGPS();
}
/**
 * GPS module has active LOW - see datasheet.
 */
void swOnGPS()
{
  digitalWrite(LED_YELLOW, LOW);    // green on = GPS on
  nss.begin(9600);
  digitalWrite(gpsCTRL, LOW);
  gpsPower = true;
}
void swOffGPS()
{
  digitalWrite(LED_YELLOW, HIGH);    // green off = GPS off
  nss.end();
  digitalWrite(gpsCTRL, HIGH);
  gpsPower = false;
}


void sendIridium(char* msg) {
  int signalQuality = -1;

  inss.begin(19200);
  
  isbd.begin();
  
  int err = isbd.getSignalQuality(signalQuality);
  if (err != 0)
  {
    Serial.print("SignalQuality failed: error ");
    digitalWrite(LED_GREEN, HIGH);    
    digitalWrite(LED_RED, LOW);    // show fail with red LED
    Serial.println(err);
    return;
  }

  Serial.print(F("Signal quality is "));
  Serial.println(signalQuality);
  Serial.print(F("Sending message: "));
  Serial.println(msg);
  err = isbd.sendSBDText(msg);
  if (err != 0)
  {
    Serial.print(F("sendSBDText failed: error "));
    digitalWrite(LED_GREEN, HIGH);    
    digitalWrite(LED_RED, LOW);    // show fail with red LED
    Serial.println(err);
    return;
  }

  Serial.println(F("Message Sent"));
  digitalWrite(LED_RED, LOW);    
  digitalWrite(LED_GREEN, HIGH);    // show success with green LED
  //Serial.print("Messages left: ");
  //Serial.println(isbd.getWaitingMessageCount());   
  inss.end();
  isbd.sleep();
}


bool ISBDCallback()
{
   digitalWrite(ledPin, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
   return true;
}

