#include <Si4703_Breakout.h>
#include <Wire.h>

int resetPin = 2;
int resetPinRDS = 3;
int selectRadio = 4;
int selectRDS = 5;
int SDIO = A4;
int SCLK = A5;
int stereoPin = 7;
bool rds;
bool radioOn;
Si4703_Breakout radio(resetPin, SDIO, SCLK);
Si4703_Breakout rds(resetPinRDS, SDIO, SCLK);
int channel;
int volume;
char rdsBuffer[20];
char rdsname[20];
char rdsrt[200];
bool displayRDS;
int channels[] =  {
  895,
 917,
 939,
 952,
 991,
1013,
1051,
1064,
1068,
1079 
};

void setup()
{
  Serial.begin(9600);
  Serial.println("\n\nSi4703_Breakout Test Sketch");
  Serial.println("===========================");  
  Serial.println("a b     Favourite stations");
  Serial.println("+ -     Volume (max 15)");
  Serial.println("u d     Seek up / down");
  Serial.println("r       Show available RDS Data (1 sec timeout)");
  Serial.println("R       Continously scan RDS");
  Serial.println("x       show registers");
  Serial.println("W       sWap radio tuners");
  Serial.println("s       Scan (1 sec timeout)");
  Serial.println("Send me a command letter.");
  
  pinMode(stereoPin, OUTPUT);
  pinMode(selectRadio, OUTPUT);
  pinMode(selectRDS, OUTPUT);
  digitalWrite(stereoPin, LOW);
  digitalWrite(selectRDS, LOW); 
  digitalWrite(selectRadio, HIGH); 
  radioOn = false;
  rds = true;
  rds.powerOn();
  rds.setVolume(3);
  delay(100);
  digitalWrite(selectRadio, LOW); 
  digitalWrite(selectRDS, HIGH); 
  radioOn = true;
  rds = false;
  radio.powerOn();
  radio.setVolume(3);

}
void showReg () {
    for(int i = 0 ; i < 16 ; i++) {
      Serial.print("Reg ");
      Serial.print(i);
      Serial.print(" : ");
     
      Serial.print(  radio.si4703_registers[i], HEX);  
      Serial.print(" : ");
      Serial.println(  radio.si4703_registers[i], BIN); 
    }
}
void loop()
{
  if (Serial.available())
  {
    char ch = Serial.read();
    if (ch >= 'a' && ch < ('a'+10)) 
    {
      channel = channels[ch-'a']; 
      radio.setChannel(channel);
      displayInfo();
    } 
//    else if (ch == 'd') 
//    {
//      channel = radio.seekDown();
//      displayInfo();
//    } 
    else if (ch == '+') 
    {
      volume ++;
      if (volume == 16) volume = 15;
      radio.setVolume(volume);
      displayInfo();
    } 
    else if (ch == '-') 
    {
      volume --;
      if (volume < 0) volume = 0;
      radio.setVolume(volume);
      displayInfo();
    } 
    else if (ch == 'W')
    {
       if(rds == true) {
        digitalWrite(selectRDS, HIGH);
        digitalWrite(selectRadio, LOW); 
        rds = false;
        radioOn = true;
      }
        else
      {
        digitalWrite(selectRDS, LOW); 
        digitalWrite(selectRadio, HIGH);
        rds = true; 
        radioOn = false; 
      }
    }
    else if (ch == 'r')
    {
          radio.readRDS(rdsBuffer, 5000);
          Serial.print(" RDS: ");
          Serial.println(rdsBuffer); 
    }
    else if (ch == 'R')
    {
      // The calling of readRDS and printing of rdsrt really need
      // to be looped to catch all of the data...
      // this will just print a snapshot of what is in the Si4703 RDS buffer...
//      radio.readMoreRDS(rdsname, rdsrt);
//      Serial.print("RDS name:");
//      Serial.print(rdsname);
//      Serial.println(rdsrt); 
      displayRDS = !displayRDS;
    }
    else if (ch == 'x')
    {
      showReg();
    }
    else if (ch == 's')
    {
        digitalWrite(selectRDS, LOW); 
        digitalWrite(selectRadio, HIGH);
      Serial.println("RDS scanning");
      int j =0;
      for (int i = 875; i < 1080; i++) {
        rds.setChannel(i);
        int ss = rds.signalStrength();
        if(ss > 15) {
          Serial.print (i/10.0);
          Serial.print (" MHz : ");
          Serial.print(ss);
          rds.readRDS(rdsBuffer, 15000);
          Serial.print(" RDS: ");
          Serial.println(rdsBuffer); 
          if(String(rdsBuffer).length() >1 ) {
            channels[j++] = i;
            i += 5;     
          }
        }   
      }
    digitalWrite(selectRDS, HIGH); 
    digitalWrite(selectRadio, LOW);

    }
  }
  if(displayRDS) {
    radio.readMoreRDS(rdsname, rdsrt);
    Serial.print(char(13));
    Serial.print(rdsname);
    Serial.print(rdsrt); 
  }
}
void displayInfo()
{
   Serial.print("Channel:"); Serial.print(channel); 
   Serial.print(" Volume:"); Serial.println(volume); 
   if(radio.isStereo()) {
    digitalWrite(stereoPin, HIGH);
   } else {
    digitalWrite(stereoPin, LOW);
    
   }
}
