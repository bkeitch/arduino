// ESP32 Touch Test
// Just test touch pin - Touch0 is T0 which is on GPIO 4.

void setup()
{
  Serial.begin(9600);
  delay(1000); // give me time to bring up serial monitor
  Serial.println("ESP32 Touch Test");
  pinMode(33, OUTPUT);  

  touch_pad_filter_start(T0);
    touch_pad_set_filter_period(T0);

    touch_pad_filter_start(T2);
    touch_pad_set_filter_period(T2);
}
int ledState = LOW;    
void loop()
{
  Serial.print(touchRead(T0));  // get value using T0
  Serial.print(" T2: ");  // get value using T0
  Serial.println(touchRead(T2));  // get value using T0
  delay(5000);
  pinMode(33, OUTPUT);
  if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    digitalWrite(33, ledState);
  
}
