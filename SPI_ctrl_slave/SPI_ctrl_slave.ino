#include <SPI.h>
#define LEDpin 7
#define buttonpin 2
#define SS 10
volatile boolean received;
volatile byte Slavereceived,Slavesend;
int buttonvalue;
int x;
void setup()

{
    Serial.begin(9600);
    Serial.print("Hello from MEGA \n");
    pinMode(buttonpin, INPUT);               // Setting pin 2 as INPUT
    pinMode(LEDpin, OUTPUT);                 // Setting pin 7 as OUTPUT
    pinMode(MISO, OUTPUT);                   //Sets MISO as OUTPUT (Have to Send data to Master IN 
    
    SPCR |= _BV(SPE);                       //Turn on SPI in Slave Mode
    received = false;
    
    SPI.attachInterrupt();                  //Interuupt ON is set for SPI commnucation
  
}

ISR (SPI_STC_vect)                        //Inerrrput routine function 
{
  Slavereceived = SPDR;         // Value received from master if store in variable slavereceived
  received = true;                        //Sets received as True 
}

void loop()
{ 
  if(received)                            //Logic to SET LED ON OR OFF depending upon the value recerived from master
   {
      if (Slavereceived>0 || Slavesend == 1) 
      {
        digitalWrite(LEDpin, HIGH);         //Sets pin 7 as HIGH LED ON
        Serial.println(received, BIN);
        Serial.println("Slave LED ON");
        
      }
      else
      {
        digitalWrite(LEDpin, LOW);          //Sets pin 7 as LOW LED OFF
        Serial.println("Slave LED OFF");
      }
      
      buttonvalue = digitalRead(buttonpin);  // Reads the status of the pin 2
      
      Slavesend = (buttonvalue == HIGH) ? 1 : 0;               //Logic to set the value of x to send to master
                            
    SPDR = Slavesend;                           //Sends the x value to master via SPDR 
    delay(1000);
  }
}
