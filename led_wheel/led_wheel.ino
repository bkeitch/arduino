int sensorPin = A0;    // select the input pin for the potentiometer
int ledPins[] = {2,3,4,5,8,9, 10, 11};      // select the pin for the LED
int sensorValue = 0;  // variable to store the value coming from the sensor
int count;
bool reverse;
void setup() {
  // declare the ledPin as an OUTPUT:
  for (int i =0 ; i<8; i++) {
    pinMode(ledPins[i], OUTPUT);
    
  }
  count = 0;
}

void loop() {
  
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
    if( count == 0) reverse = false;
    if (count == 8) reverse = true;

  
    // turn the ledPin off:
    digitalWrite(ledPins[count], HIGH);
    // turn the ledPin on
    if(!reverse)
    digitalWrite(ledPins[(count-1)%8], LOW);
    else 
    digitalWrite(ledPins[(count+1)%8], LOW);
    if(sensorValue > 200) {
  
    if (!reverse) count++;
    if(reverse) count--;
    // stop the program for <sensorValue> milliseconds:
    } //delay(50);

}
