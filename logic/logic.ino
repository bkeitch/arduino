/*
  logic

  Turns an LED on for one second, then off for one second, repeatedly.

  
*/
#define A1 13
#define B1 12
#define C1 11
#define D1 10
#define A2 9
#define B2 8
#define C2 7
#define D2 6
#define P 5
#define Q 4
#define R 3
#define S 2
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(A1, OUTPUT);
  pinMode(B1, OUTPUT);
  pinMode(C1, OUTPUT);
  pinMode(D1, OUTPUT);
  
  pinMode(A2, OUTPUT);
  pinMode(B2, OUTPUT);
  pinMode(C2, OUTPUT);
  pinMode(D2, OUTPUT);
  
  pinMode(P, INPUT);
  pinMode(Q, INPUT);
  pinMode(R, INPUT);
  pinMode(S, INPUT);
  pinMode(A0, INPUT);
  Serial.begin(115200);
}

// the loop function runs over and over again forever
void loop() {
//while (Serial.available() > 0) {

    // look for the next valid integer in the incoming serial stream:
    //int i = Serial.parseInt();

    //int j = Serial.parseInt();
    
    // look for the newline. That's the end of your sentence:
    //if (Serial.read() == '\n') {
    
  for (byte i = 0; i < 16; i++) {
   digitalWrite(A1, i&1);   // turn the LED on (HIGH is the voltage level)
   digitalWrite(B1, (i>>1)&1);   // turn the LED on (HIGH is the voltage level)
   digitalWrite(C1, (i>>2)&1);   // turn the LED on (HIGH is the voltage level)
   digitalWrite(D1, (i>>3)&1);   // turn the LED on (HIGH is the voltage level)
   for (byte j = 0; j < 16; j++) {
     digitalWrite(A2, j&1);   // turn the LED on (HIGH is the voltage level)
     digitalWrite(B2, (j>>1)&1);   // turn the LED on (HIGH is the voltage level)
     digitalWrite(C2, (j>>2)&1);   // turn the LED on (HIGH is the voltage level)
     digitalWrite(D2, (j>>3)&1);   // turn the LED on (HIGH is the voltage level)
                       // wait for a second
       Serial.print(i);
       Serial.print(" + ");
       Serial.print(j);
       Serial.print( " = ");
       Serial.print(i + j);
       //Serial.print( " \t ");
       byte t = digitalRead(A0);
       //Serial.print(t);
       byte s = digitalRead(S);
       //Serial.print(s);
       byte r = digitalRead(R);
       //Serial.print(r);
       byte q = digitalRead(Q);
       //Serial.print(q);
       byte p = digitalRead(P);
       //Serial.print(p);
       ///Serial.print( " \t ");
int read_in = int((t<<4) | (s<<3) |(r<<2) |(q<<1) | p);
if (read_in == (i+j)) Serial.println( "\tOK"); else Serial.println("\tFail");
       //Serial.println(read_in);
       //delay(200);  
       }  
  }
  while(1);
//  delay(2000);
}
