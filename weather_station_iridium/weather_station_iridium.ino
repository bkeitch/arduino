#include <FreqCounter.h>
#include <IridiumSBD.h>
#include <SoftwareSerial.h>

SoftwareSerial nss(2, 3);
IridiumSBD isbd(nss, 10);
static const int ledPin = 13;

const int temperaturePin = A0;  // Analog input pin LM35T is attached to
const int pressurePin = A1;  // Analog input pin pressure sensor is attached to

float pressureVoltage, temperatureVoltage, temperatureSensorValue, pressureSensorValue, humidityFreq;
int temperature, pressure, humidity;
double fMHz;
String msg;
bool enableIridium = true;

void setup() {
  int signalQuality = -1;

  pinMode(ledPin, OUTPUT);
  Serial.begin(115200);
  if(enableIridium) {
    nss.begin(19200);
  
    isbd.attachConsole(Serial);
    isbd.setPowerProfile(1);
    isbd.begin();
  
    int err = isbd.getSignalQuality(signalQuality);
    if (err != 0)
    {
      Serial.print("SignalQuality failed: error ");
      Serial.println(err);
      return;
    }
  
    Serial.print("Signal quality is ");
    Serial.println(signalQuality);
  
    msg = getPTU();
    delay(10000); // wait 10sec
    msg = getPTU();
    Serial.println(msg);
    char buf[50];
    msg.toCharArray(buf,50);
    err = isbd.sendSBDText(buf);
    if (err != 0)
    {
      Serial.print("sendSBDText failed: error ");
      Serial.println(err);
      return;
    }
  
    Serial.println("Message Sent");
    Serial.print("Messages left: ");
    Serial.println(isbd.getWaitingMessageCount());   
  }
}

void loop() {
  if(!enableIridium) {
    msg = getPTU();
    Serial.println(msg);
    delay(2000);
  }
}
bool ISBDCallback()
{
   digitalWrite(ledPin, (millis() / 1000) % 2 == 1 ? HIGH : LOW);
   return true;
}
String getPTU() {
  String ptu;
  temperatureSensorValue = 0;
  pressureSensorValue = 0;
  for(int i=0; i< 10; i++) {
    temperatureSensorValue += analogRead(temperaturePin);
    pressureSensorValue += analogRead(pressurePin);
    delay(100);
  }
  temperatureVoltage = map(temperatureSensorValue, 0, 10230, 0, 5000);
  temperature = (unsigned int) temperatureVoltage * 100;
  pressureVoltage = map(pressureSensorValue, 0, 10230, 0, 5000);
  // convert to nearest hPa as per data sheet
  pressure = (unsigned int) ((pressureVoltage + (5000*0.095))/ (5*0.9));
  ptu = "P : ";
  ptu += pressure;
  ptu += " T : ";
  ptu += temperature;
  
  FreqCounter::f_comp= 8;             // Set compensation to 12
  FreqCounter::start(100);            // Start counting with gatetime of 100ms
  while (FreqCounter::f_ready == 0)         // wait until counter ready
  
  humidityFreq = FreqCounter::f_freq;            // read result
  fMHz = humidityFreq/1000000.0;
  humidity =  (int) (((1.2 / 0.051 / fMHz) -22 ) / 0.6);
  
  ptu += " U : ";
  ptu += humidity;
  return ptu;
}

