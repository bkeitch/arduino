//
//    FILE: I2C_eeprom_test.ino
//  AUTHOR: Rob Tillaart
// VERSION: 0.1.09
// PURPOSE: show/test I2C_EEPROM library
//

#include <Wire.h> //I2C library
#include <I2C_eeprom.h>


#define MEMORY_SIZE 131072 //total bytes can be accessed 24LC1025 = 0x8000 (maximum address = 0x1FFF)

I2C_eeprom ee0(0x52, MEMORY_SIZE);
I2C_eeprom ee1(0x51, MEMORY_SIZE);

void setup() 
{
  ee0.begin();
//  ee1.begin();
  Serial.begin(57600); 
  while (!Serial); // wait for Serial port to connect. Needed for Leonardo only
  
  Serial.print("Demo I2C eeprom library ");
  
  Serial.println("\nTEST: determine size");
  int start = micros();
  int size = ee0.determineSize();
  int diff = micros() - start;
  Serial.print("TIME: ");
  Serial.println(diff);
  if (size > 0)
  {
    Serial.print("SIZE ee0: ");
    Serial.print(size);
    Serial.println(" KB");
  } else if (size == 0)
  {
    Serial.println("WARNING: Can't determine eeprom size");
  }
  else
  {
    Serial.println("ERROR: Can't find eeprom\nstopped...");
    while(1);
  }

//  size = ee1.determineSize();
//  diff = micros() - start;
//  Serial.print("TIME: ");
//  Serial.println(diff);
//  if (size > 0)
//  {
//    Serial.print("SIZE ee1: ");
//    Serial.print(size);
//    Serial.println(" KB");
//  } else if (size == 0)
//  {
//    Serial.println("WARNING: Can't determine eeprom size");
//  }
//  else
//  {
//    Serial.println("ERROR: Can't find eeprom\nstopped...");
//    while(1);
//  }
    Serial.println("On power Up");
Serial.println (ee0.readByte(0));
//Serial.println (ee1.readByte(0));
Serial.println (ee0.readByte(20));
//Serial.println (ee1.readByte(20));
  dumpEEPROM(&ee0, 0, 128);
//  dumpEEPROM(&ee1, 0, 128);
//  char data[] = "Hello This Is Ben";
//  ee0.writeBlock(0, (uint8_t*) data, 17);
ee0.writeByte(0, (uint8_t*) 'B');
//ee1.writeByte(0, (uint8_t*) 'E');
ee0.writeByte(20, (uint8_t*) 'N');
//ee1.writeByte(20, (uint8_t*) 'K');
      Serial.println("Asfter Write");

//  dumpEEPROM(&ee0, 0, 128);
//  *data = "Hello This Is Ben";
//  ee1.writeBlock(60, (uint8_t*) data, 17);
//  dumpEEPROM(&ee1, 0, 128);

Serial.println (ee0.readByte(0));
//Serial.println (ee1.readByte(0));
Serial.println (ee0.readByte(20));
//Serial.println (ee1.readByte(20));
  
}

void loop() 
{
}
#define BLOCK_TO_LENGTH 10
void dumpEEPROM(I2C_eeprom *ee, uint16_t memoryAddress, uint16_t length)
{
  
  
  Serial.print("\t ");
  for(int x = 0; x < BLOCK_TO_LENGTH; x++) {
    if(x != 0) {
      
      
      Serial.print("   ");
    }
    
    Serial.print(x,HEX);
  }
  Serial.println();

  // block to defined length
  memoryAddress = memoryAddress / BLOCK_TO_LENGTH * BLOCK_TO_LENGTH;
  length = (length + BLOCK_TO_LENGTH - 1) / BLOCK_TO_LENGTH * BLOCK_TO_LENGTH;

  byte b = ee->readByte(memoryAddress); 
  for (unsigned int i = 0; i < length; i++)
  {
    char buf[6];
    if (memoryAddress % BLOCK_TO_LENGTH == 0)
    {
      if(i != 0) {
        Serial.println();
      }

      
      sprintf(buf, "%04X", memoryAddress);
      Serial.print(buf);
      Serial.print(":\t");
    }
    
    
    sprintf(buf, "%02X", b);
    Serial.print(buf);
    b = ee->readByte(++memoryAddress); 
    Serial.print("  ");
  }
  Serial.println();
}
// END OF FILE
