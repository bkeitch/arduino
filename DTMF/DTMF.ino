#include <avr/sleep.h>
#include <Tone.h>

int duraciondeltono=400;
int duracionDESPUESdeltono=500;
unsigned long timesincelastpress;
unsigned long timeout=30;  //if the "timeout" in seconds is reached without any touch , the processor goes to sleep

Tone freq1;
Tone freq2;
//Order of dtmf are : 0123456789 * #
const int DTMF_freq1[] = { 1336, 1209, 1336, 1477, 1209, 1336, 1477, 1209, 1336, 1477,1209 ,1477  }; 
const int DTMF_freq2[] = {  941,  697,  697,  697,  770,  770,  770,  852,  852,  852, 941 ,941   };

void setup()
{
  Serial.begin(9600);  // just as debug, you can take it out later
  Serial.println("start");  // just as debug, you can take it out later
  freq1.begin(11);   // PINS 11 and  12 have one diode each  connected to same side of speaker, the other side of speaker to ground 
  freq2.begin(12);
  pinMode(13, INPUT);
  digitalWrite(13, HIGH);       // turn on pullup resistors
  
// timesincelastpress=millis();
}

 

void playDTMF(uint8_t number, long duration)
{
  freq1.play(DTMF_freq1[number], duration);
  freq2.play(DTMF_freq2[number], duration);
}

 

 

void loop()

{

  char customKey = digitalRead(13);
  
  if (customKey){
     
    switch (customKey) {
      case '0':    
       playDTMF(0, duraciondeltono);
      break;
      case '1':    
       playDTMF(1, duraciondeltono);
      break;
     case '2':    
       playDTMF(2, duraciondeltono);
      break;
      case '3':    
       playDTMF(3, duraciondeltono);
      break;
      case '4':    
       playDTMF(4, duraciondeltono);
      break;
      case '5':    
       playDTMF(5, duraciondeltono);
      break;
      case '6':    
       playDTMF(6, duraciondeltono);
      break;
      case '7':    
       playDTMF(7, duraciondeltono);
      break;
      case '8':    
       playDTMF(8, duraciondeltono);
      break;
      case '9':    
       playDTMF(9, duraciondeltono);
      break;
      case '*':    
       playDTMF(10, duraciondeltono);
      break;
      case '#':    
       playDTMF(11, duraciondeltono);
      break;
     
     default:   //just in case the caracter read has no value at all
      delay(1);
    }
    
    
  
 
   Serial.println(customKey);   // just as debug, you can take it out later
   delay(duracionDESPUESdeltono);
   timesincelastpress=millis();
      
    for (int pin= 0 ;pin<19;pin++){   //to make sure no pin is left high if the avr goes to sleep
      digitalWrite(pin, LOW);    
    }
     
    
  }
  
  if ( millis() > timesincelastpress+(timeout*1000)){
 
  freq1.play(4000, 100);
   delay(duracionDESPUESdeltono/10);
  freq1.play(3000, 100);
   delay(duracionDESPUESdeltono/10);
  freq1.play(2000, 100);
   delay(duracionDESPUESdeltono/10);
  freq1.play(1000, 100);
   delay(duracionDESPUESdeltono/10);


  for (int pin= 0 ;pin<19;pin++){
    digitalWrite(pin, LOW);
  }


    Serial.println("A DORMIR TOCAN");    // just as debug, you can take it out later
    delay(duracionDESPUESdeltono);
    sleepNow();     // sleep function called here
  }




}

