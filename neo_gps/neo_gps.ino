/*
Weather station. Plot barometric trend as bar graph. Stores data in array
on Mega and uses RTC. 
BCK 2019

Hardware: I2C bus on pins 20/21
RTC: DS3231 RTC and 
Sensor: BMP280 on I2C 5V 

Analogue:
Sensor: MPX4115 at 5V on A?

Graphics:K108 64 x 128 on pins 24-34

GPS: NEO1 on Mega 14/15. PWR on D7

SD Card : SPI on pins 50-53 (Purple on 13)

Iridium: Modem on NEO1 18/19

openGLCD installed from Zip.

 */
#include "TinyGPS++.h"
#include "x_data.h"

const int time_interval = 5;

const bool DEBUG = true;
byte oldMinute; //initiated in startup()
bool gpsPower = false;



// Create an instance of the TinyGPS object
TinyGPSPlus gps;


void toggleGPS() {
  if(gpsPower == false)
    swOnGPS();
  else
    swOffGPS();
}
/**
 * GPS module has active LOW - see datasheet.
 */
void swOnGPS()
{
  digitalWrite(EN_GPS, LOW);    // green on = GPS on
  NEO1.begin(9600);
  gpsPower = true;
}
void swOffGPS()
{
  digitalWrite(EN_GPS, HIGH);    // green off = GPS off
  NEO1.end();
  gpsPower = false;
}



void getGPSTime() {
  bool timeNeeded = true;
  swOnGPS();
  delay(1000); // warm up GPS
  unsigned long start_time = millis();
  while (timeNeeded) {
    if(millis() - start_time > 10000) timeNeeded = false; 
    while (NEO1.available() > 0)
    {
      int c = NEO1.read();
      if(DEBUG) Serial.write(c);
      if (gps.encode(c)) {
        // process new gps info here
        //if (gps.time.isUpdated())
        {
          tmElements_t tm;
          tm.Hour = gps.time.hour();
          tm.Minute = gps.time.minute(),
          tm.Second = gps.time.second();
          tm.Day = gps.date.day();
          tm.Month = gps.date.month();
          tm.Year = (gps.date.year()-1970);      // tmElements_t.Year is the offset from 1970
          if(DEBUG) Serial.print(gps.time.value());
          //RTC.write(tm);              // set the RTC from the tm structure
          timeNeeded = false;
        }
      }
    }
  }
  swOffGPS();

}
void getPosition()
{
  swOnGPS();
  delay(1000); // warm up GPS
  
  bool posNeeded = true;
  unsigned long start_time = millis();
  while(posNeeded) {
    if(millis() - start_time > 10000) posNeeded = false; 
    while (NEO1.available() > 0)
    {
      int c = NEO1.read();
      if(DEBUG) Serial.write(c);
      if (gps.encode(c)) {
        // process new gps info here
        if (gps.time.isUpdated())
        {
          tmElements_t tm;
          tm.Hour = gps.time.hour();
          tm.Minute = gps.time.minute(),
          tm.Second = gps.time.second();
          tm.Day = gps.date.day();
          tm.Month = gps.date.month();
          tm.Year = (gps.date.year()-1970);      // tmElements_t.Year is the offset from 1970
          //RTC.write(tm);              // set the RTC from the tm structure
        }
        //if (gps.location.isUpdated())
        {
          if(DEBUG) {Serial.print("\nLAT="); Serial.print(gps.location.lat(), 6);
          Serial.print("\tLNG="); Serial.println(gps.location.lng(), 6);}
             posNeeded = false;
        }
  
      }
    }
  }

  //      //  success so switch off GPS
  swOffGPS();

}


void setup()
{

  
  
  if(DEBUG) {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  
  
  pinMode(EN_GPS, OUTPUT);
 
}


}
void loop() {

  getGPSTime();
  getPosition();
  delay(5000);
}
