/*
Lucy's pen app
*/
int leds [] = {13,12,11,10,9,8,7,6,5,4,3,2} ; // 12 LEDS connected to each output

const int buttonPin = A5;     // the number of the pushbutton pin
const int moisturePin=A2;     // analog pin for measuring capacitor voltage
const int tempPin=A4;         // analog pin for measuring thermistor voltage
const int lightPin=A3;        // analog pin for measuring LDR voltage

//capacitance measure
const int chargePin=2;        // pin to charge the capacitor - connected to one end of the charging resistor
const int dischargePin=3;     // pin to discharge the capacitor
const float resistorValue=10000.0F;   // resistor value for charging capacitor
                              


// POLL driven menu
int buttonState = 0;         // variable for reading the pushbutton status
int menuCount = 0;


void setup(){
  pinMode(buttonPin, INPUT);
  //pinMode(chargePin, OUTPUT);     // set chargePin to output
  //digitalWrite(chargePin, LOW);  
  // initialize digital pins for LED display
  for (int i = 0; i < 12 ; i++) {
    pinMode(leds[i], OUTPUT);
  }
}

void loop() {
  // read the state of the pushbutton value:
  buttonState = digitalRead(buttonPin);

  // check if the pushbutton is pressed. If it is, the buttonState is HIGH:
  if (buttonState == HIGH) {
    // turn LEDs off:
    //displayBinary((21<<8)|21);
    displayBinary(0); 

    menuCount++;
    delay(100); // cope with switch bounce
  }
  switch(menuCount)
  {
    case 1:
      displayBinary(1);
    break;
    case 2:
      binaryRandomNumber();
    break;
    case 3:
      knightRider();
    break;
    case 4:
      {
        unsigned int V = readSteadyVoltage(tempPin);
        int Vmap = map(V, 300, 900, 0, 12);
        displayBar(Vmap);
      }
    break;
    case 5:
      {//displayBinary(5);
        unsigned int V = readSteadyVoltage(lightPin);
        int Vmap = map(floor_log2(V), 0, 10, 0, 12);
        displayBar(Vmap);
      }
    break;
    case 6:
      dice();
    break;
    case 7:
      count();
    break;
    default:
      menuCount = 0;    
  }
delay(2000);

}

void dice() {
  randomSeed(analogRead(0)); 
  unsigned int left ;
  unsigned int right ;
  int first = 3;
  int second = 2;
  
  left = (1<<first) - 1;
  right = (1<<second) -1; 
  displayBinary((left << 6) | right); 
  delay(3000);
  first = 4;
  second = 5;

  left = (1<<first) - 1;
  right = (1<<second) -1; 
  displayBinary((left << 6) | right); 
  delay(3000);

  first = random(5)+1;
  second = random(5)+1;

  
  left = (1<<first) - 1;
  right = (1<<second) -1; 
  displayBinary((left << 6) | right); 
  delay(3000);
}

void count() {
  for (int i = 0; i < 12; i++) {//(2^12 -1)
    //displayBinary (i);
    digitalWrite(leds[i], HIGH);    
    delay(500);
    digitalWrite(leds[i], LOW);
  }
}
void countBinary() {
  for (int i = 0; i < ((2^12) -1); i++) {
    displayBinary (i);
    delay(100);
  }
}
int readCapacitance(){
  unsigned long startTime,elapsedTime;
  digitalWrite(chargePin, HIGH);  // set chargePin HIGH and capacitor charging
  //time how long it takes to charge
  startTime = millis();
  while(analogRead(moisturePin) < 648){       // 647 is 63.2% of 1023, which corresponds to full-scale voltage
  }
  elapsedTime = millis() - startTime;
  
  /* dicharge the capacitor  */
  digitalWrite(chargePin, LOW);             // set charge pin to  LOW
  pinMode(dischargePin, OUTPUT);            // set discharge pin to output
  digitalWrite(dischargePin, LOW);          // set discharge pin LOW
  while(analogRead(moisturePin) > 4){         // wait until capacitor is completely discharged
  }
  pinMode(dischargePin, INPUT);            // set discharge pin back to input

  if (elapsedTime > 1024){
    return(elapsedTime>>10);       // divide by 1024
  }
  return (elapsedTime);
}


void binaryRandomNumber() {
  randomSeed(analogRead(0)); 
  int randNumber = random(4096);
  displayBinary(randNumber);
}

int readSteadyVoltage(int sensorPin) {
  float sensorValue = 0;
  for (int i = 0; i < 10 ; i++) {
    sensorValue += analogRead(sensorPin);
    delay(1);
  }
  return(int(sensorValue/10.0));
}
void displayBar(int value)   {
  bool newValue = !digitalRead( leds[value-1]);
  displayBinary(0);
  displayBinary((1<<value) - 1); 
  delay(200);
  if(newValue) {
    digitalWrite(leds[value-1], LOW);
    delay(200);
    digitalWrite(leds[value-1], HIGH);
    delay(200);
    digitalWrite(leds[value-1], LOW);
    delay(200);
    digitalWrite(leds[value-1], HIGH);
  }
}
void displayBinary (unsigned int num) {
  for (int i = 0; i < 12 ; i++) {
     digitalWrite(leds[i], bitRead(num, i));
  }
}
void knightRider() {
  int i = 0;
  for (; i < 11 ; i++) {
    digitalWrite(leds[i], HIGH);   
    delay(20); 
    digitalWrite(leds[i + 1], HIGH);
    delay(20);
    digitalWrite(leds[i], LOW);
    delay(40); 
  }
  for (; i >= 1 ; i--) {
    digitalWrite(leds[i], HIGH);
    delay(20);
    digitalWrite(leds[i - 1], HIGH);
    delay(20);
    digitalWrite(leds[i], LOW);
    delay(40);  
  }
}
/* 16-bit recursive reduction using SWAR...
     but first step is mapping 2-bit values
     into sum of 2 1-bit values in sneaky way
  */
unsigned int
ones16(register unsigned int x)
{ 
  x -= ((x >> 1) & 0x5555);
  x = (((x >> 2) & 0x3333) + (x & 0x3333));
  x = (((x >> 4) + x) & 0x0f0f);
  x += (x >> 8);
  return(x & 0x003f);
}
unsigned int
floor_log2(register unsigned int x)
{
  x |= (x >> 1);
  x |= (x >> 2);
  x |= (x >> 4);
  x |= (x >> 8);
  return(ones16(x >> 1));

}
