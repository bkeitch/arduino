/**
   Station X Power On Self Test

  BCK 2022

*/
#define DEBUG_DATA 1
#define DEBUG_RTC 0
#define USE_GPS  1
#define USE_IRIDIUM 1
//#define WIFI_MODEM 1
#define USE_SD 1
//#define USE_MENU 1
#define USE_DEPTH 1
//default wake up every few seconds. Change to minutes.
//#define USE_MINUTES 1

#ifdef USE_GPS
#include <TinyGPS++.h> //http://arduiniana.org/libraries/tinygpsplus/
#endif
//#include <DS3232RTC.h>  //RTC Library https://github.com/JChristensen/DS3232RTC
#include <Wire.h> //built in
//#include <avr/sleep.h>          //contains sleep modes
#include <BME280I2C.h>
#include <MsgPack.h>
#ifdef USE_MENU
#include <slight_DebugMenu.h>
#endif
#include <SparkFun_External_EEPROM.h> //  http://librarymanager/All#SparkFun_External_EEPROM
#include <Base64.h> //https://www.arduino.cc/reference/en/libraries/base64/
#include "x_data.h"
#ifdef USE_SD
#include <SD.h> //built in
#endif
#ifdef USE_IRIDIUM
#include <IridiumSBD.h> // v2.0 from library
#endif

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  5        /* Time ESP32 will go to sleep (in seconds) */

RTC_DATA_ATTR int bootCount = 0;


static constexpr int timeout = 2000;
static constexpr int time_interval = 5;
static constexpr int months[] = {
  281,
  269,
  288,
  291,
  295,
  301,
  299,
  285,
  296,
  294,
  307,
  268
};

//Global objects:

x_data data;

BME280I2C bme; // Default : forced mode, standby time = 1000 ms
// Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,

ExternalEEPROM EEPROM_1;
//DS3232RTC RTC;

// slight_DebugMenu(Stream &in_ref, Print &out_ref, uint8_t input_length_new);
#ifdef USE_MENU
slight_DebugMenu menu(Serial, Serial, 2);
#endif
unsigned int wakes;
static constexpr unsigned long memorySize = 131072UL;
static constexpr unsigned int MAX_WAKES = (int) (memorySize / sizeof(data));
bool haveBME = false;
/**
   Shows time on serial output
*/
void displayTime(time_t t) {
  char cbuffer[20];
  sprintf(cbuffer, "%04u-%02u-%02u %02u:%02u:%02u",
          year(t), month(t),  day(t),  hour(t),  minute(t),  second(t)
         );
  Serial.println(cbuffer);
}
/**
 * Update distance to ground from echo sensor using current temp
 * @return error code
 * c_air =331.3 + T * 0.606 m/s
 * 1cm ~ 27us in warm air
 * 2m ~ 5491us in warm air (7000 in cold air)
 */
 
#ifdef USE_DEPTH
int getDepth(float temp) {
  unsigned long echo_time=0;
  double distance = 0;
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(EN_SNOW, LOW);
  delayMicroseconds(100);
  noInterrupts();
  for (unsigned int i=0 ; i<10 ; i++) {
    digitalWrite(SNOW_TRIG, LOW);
    delayMicroseconds(2);
    digitalWrite(SNOW_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite (SNOW_TRIG, LOW);
    echo_time = pulseIn(SNOW_SIG, HIGH, 10000); //max 3m at -70C
    if (echo_time==0) return (-1);
    distance+=echo_time;
  }
  interrupts();
  if (temp < -60.0 || temp > 60.0) temp = 20.0; //for testing
  distance = distance/200000.0 * (331.3 + temp * 0.606) ; //cm (there and back)
  if((distance  > 255.0) || (distance < 0.1)) return (-2);
  digitalWrite(EN_SNOW, HIGH);
  #ifdef DEBUG_DATA
  Serial.print(distance); Serial.println(F(" cm"));
  #endif
  return ((int) round(distance));
}
#endif

#ifdef USE_GPS
void getGPS() {
  TinyGPSPlus gps;
  bool timeNeeded = true;
  bool posNeeded = true;
  unsigned long start_time = millis();
  digitalWrite(EN_GPS, LOW);    // green on = GPS on
  NEO1.begin(9600);
  delay(1000); // warm up GPS
  while (posNeeded || timeNeeded) {
    if (millis() - start_time > 10000) {
      posNeeded = false;
      timeNeeded = false;
    }
    while (NEO1.available() > 0)
    {
      int c = NEO1.read();
      if (gps.encode(c)) {
        if (gps.time.isUpdated() && timeNeeded)
        {
          //*TODO
          //setRTCTime(gps.time.hour(), gps.time.minute(), gps.time.second(), gps.date.day(), gps.date.month(), gps.date.year() - 1970, false); //GPS epoch is 2000 not 1970
          timeNeeded = false;
        }
        if (gps.location.isUpdated() && posNeeded)
        {
#ifdef DEBUG_DATA
          Serial.print(F("\nLAT=")); Serial.print(gps.location.lat(), 6);
          Serial.print(F("\tLONG=")); Serial.println(gps.location.lng(), 6);
#endif
          posNeeded = false;
        }
      }
    }
  }
  NEO1.end();
  digitalWrite(EN_GPS, HIGH);
}
#endif


/**
   Call back for Filesystem times.
*/
#ifdef USE_SD
void dateTime(uint16_t* date, uint16_t* time) {
//*TODO
  time_t t_rtc = 0;//RTC.get();
  // Return date using FS_DATE macro to format fields.
//  *date = FAT_DATE(year(t_rtc), month(t_rtc), day(t_rtc));

  // Return time using FS_TIME macro to format fields.
//  *time = FAT_TIME(hour(t_rtc), minute(t_rtc), second(t_rtc));

  // Return low time bits in units of 10 ms.
  //*ms10 = second(t_rtc) & 1 ? 100 : 0;
}
#endif

/**
   Get's up-to-date parameters from sensors
*/
void updateXData(bool update) {
  unsigned long mem = wakes * sizeof(x_data);
  float bme_p = 0, bme_t = 0, bme_u = 0;
  if (haveBME ) {
    bme.read(bme_p, bme_t, bme_u, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
  }
  int depth = getDepth(bme_t);
#ifdef DEBUG_DATA
  Serial.print(bme_p); Serial.print(F(" Pa\t"));
  Serial.print(bme_t); Serial.print(F(" C\t"));
  Serial.print(bme_u); Serial.println(F("%"));

#endif
//*TODO
  time_t t = 0;//RTC.get();
  if (update) data.update(bme_t, bme_p, bme_u, depth, t, mem);
}


int freeRam () {
  //extern int __heap_start, *__brkval;
  int v =100;
  return v;//(int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
}


#ifdef USE_IRIDIUM
int testIridium () {
  IridiumSBD modem(IM1, SLEEP);
  digitalWrite(EN_MODEM, LOW);
  int err;
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {
    return err;
  }

  // Print the firmware revision
  char version[12];
  err = modem.getFirmwareVersion(version, sizeof(version));
  if (err != ISBD_SUCCESS)
  {
    return err;
  }
  Serial.print(F("Firmware Version is "));
  Serial.print(version);
  int signalQuality = -1;

  err = modem.getSignalQuality(signalQuality);
  if (err != ISBD_SUCCESS)
  { 
    return err;
  }

  Serial.print(F("Signal quality (0-5): "));
  Serial.println(signalQuality);


  digitalWrite(EN_MODEM, HIGH);
  return ISBD_SUCCESS;

}
int getIridiumTime () {
  IridiumSBD modem(IM1, SLEEP);
  digitalWrite(EN_MODEM, LOW);
  int err;
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {
    return err;
  }

  time_t t_cp;
  err = modem.getSystemTime(t_cp);
  if (err == ISBD_SUCCESS)
  {
    //*TODO
    time_t t_rtc = 0;//RTC.get();
#ifdef DEBUG_RTC
    Serial.println(F("Iri Time: "));
    displayTime(t_cp);
    Serial.println(F("RTC Time: "));
    displayTime(t_rtc);
#endif

    if (0 == data.updateIridiumOffset(t_rtc, t_cp)) {
//*TODO
//      RTC.set(t_cp);
    } else {
#ifdef DEBUG_RTC
      Serial.println(F("RTC not set"));
#endif
    }
  } else {
    return err;
  }

  digitalWrite(EN_MODEM, HIGH);
  return ISBD_SUCCESS;
}

int sendIridiumMessage(const uint8_t *txData, size_t txDataSize) {
  IridiumSBD modem(IM1, SLEEP);
  digitalWrite(EN_MODEM, LOW);
  int err;
  err = modem.begin();
  if (err != ISBD_SUCCESS)
  {

    return err;
  }
  err = modem.sendSBDBinary(txData, txDataSize);
  if (err != ISBD_SUCCESS)
  {
    return err;
  }

  digitalWrite(EN_MODEM, HIGH);
  return ISBD_SUCCESS;
}
#endif
#ifdef DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD * device, char c)
{
  Serial.write(c);
}

void ISBDDiagsCallback(IridiumSBD * device, char c)
{
  Serial.write(c);
}
#endif
#ifdef USE_SD
void printDirectory(File dir, int numTabs) {
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print(F("\t"));
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println(F("/"));
      printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print(F("\t\t"));
      Serial.println(entry.size(), DEC);
    }
    entry.close();
  }
}
int testSD(int SD_CARD) {
  //digitalWrite(EN_EEPROM, LOW); //switch on V translate
   int CS = SD_1_CS;
   if (SD_CARD == 1) {
   
    digitalWrite(EN_SD1, LOW); //switch on SD1
  } else {
    CS = SD_2_CS;
    digitalWrite(EN_SD2, LOW); //switch on SD1
  }
  delay(5);

  if (!SD.begin(CS)) {
    return SD1_INIT_FAIL + SD_CARD;
  } 
  Serial.println("SD Contents");
  File root = SD.open(F("/"));
  printDirectory(root, 0);
  root.close();
  SD.end();
  
  digitalWrite(EN_SD1, HIGH);//switch off SD1
  digitalWrite(EN_SD2, HIGH);//switch off SD2
  return SD_TEST_OK;

}
#endif
int setupEEPROM() {
  digitalWrite(EN_EEPROM, LOW);
  Wire.begin();
  Wire.setClock(400000); // EEPROMs can run 400kHz and higher
  if (EEPROM_1.begin(EEPROM_0_ADR_LOW_BLOCK_1) == false) {
    return EEPROM_NOT_DETECTED;
  }
  //Set settings for this EEPROM
  EEPROM_1.setMemorySize(memorySize); //In bytes. 1024kbits x 2 blocks.
  EEPROM_1.setPageSize(128); //In bytes. Has 128 byte page size.
  EEPROM_1.enablePollForWriteComplete(); //Supports I2C polling of write completion
  EEPROM_1.setPageWriteTime(3); //3 ms max write time
  EEPROM_1.get(0, wakes);
  Serial.print(F("EEMPROM size: "));
  Serial.print(wakes);
  Serial.print(F(" of "));
  Serial.println(MAX_WAKES);
  return EEPROM_TEST_OK;
}

int setupSensor() {
  digitalWrite(EN_SENSE, LOW);
  const unsigned int MAXTRIES = 2;
  unsigned int i = 0;
  while (!bme.begin() && i < MAXTRIES ) {
    delay(100);
    ++i;
  }
  if (i == MAXTRIES) {
    return BME_NOT_FOUND;
  }
  if (bme.chipModel() == BME280::ChipModel_BME280 || bme.chipModel() ==BME280::ChipModel_BMP280) 
      return BME_SUCCESS;
  return BME_NOT_FOUND;
}

void readMemory() {
  x_data data_out;
  //char cbuffer [250];
  for (unsigned int addr = 0; addr < wakes * sizeof(data_out); addr += sizeof(data_out)) {
    Serial.print('@');
    Serial.print(addr, HEX);
    Serial.print(F(" \t "));

    if (addr == 0) {
      int current_wakes;
      EEPROM_1.get(addr, current_wakes);
      Serial.println(current_wakes);
    } else {
      EEPROM_1.get(addr, data_out); //location,  data
      //data_out.getJSONData(cbuffer);
      //Serial.println(cbuffer);
      //Serial.println(data_out, HEX);
    }
  }
}
/**
   Places Arduino to sleep. When woken up and sets alarm again
*/
void gotoSleep() {

  // writeDataEEPROM();
  #ifdef DEBUG
  Serial.println(F("Sleeping")); // + String(hour(t)) + ":" + String(minute(t)) + ":" + String(second(t)));
  //if we want to see the message before going to sleep:
  Serial.flush();
  #endif
//*TODO
//  sleep_enable();
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  esp_deep_sleep_start();
 
//  attachInterrupt(digitalPinToInterrupt(WAKE_UP), wakeUp, LOW); //attaching an interrupt to RTC
//  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  //delay(1000);
//  sleep_cpu(); //activating sleep mode
  // ... sleeping ... //
//  time_t t_rtc = RTC.get();
  Serial.print(F("Woke up at: ")); 
//  displayTime(t_rtc);


}

/**
   Routine fired by interrupt to wake up.
*/
void wakeUp() {
//*TODO
//  sleep_disable();
  #ifdef DEBUG 
  Serial.print("INT Wakeup pin is " ); 
  Serial.println (digitalRead(WAKE_UP));
  #endif
  detachInterrupt(digitalPinToInterrupt(WAKE_UP));
}
/**
   Save data to EEPROM. Store data count at 1 and increment.
*/
void writeDataEEPROM() {

  EEPROM_1.get(0, wakes); // current count of data
  wakes++;
  if ( MAX_WAKES  < wakes ) { //need to leave 1 space
    wakes = 1;
  }
  updateXData(true);
#ifdef DEBUG_DATA
  Serial.println(freeRam());
  char cbuffer[250];
  Serial.println(F("Current Data:"));
  data.getJSONData(cbuffer);
  Serial.println(cbuffer);


  Serial.print(wakes); Serial.println(F(" ...writing..."));
#endif
  EEPROM_1.put((wakes * sizeof(data)), data); //(location, data)
  EEPROM_1.put(0, wakes); // store incremented data count
}
/**
   Log data to SD card
*/
#ifdef USE_SD
int writeDataSD(int SD_CARD)
{
//  Serial.println(freeRam());
  int CS = SD_2_CS;
  //digitalWrite(EN_EEPROM, LOW); //switch on V translate
  if (SD_CARD == 1) {
    CS = SD_1_CS;
    digitalWrite(EN_SD1, LOW); //switch on SD1
  } else {
    CS = SD_2_CS;
    digitalWrite(EN_SD2, LOW); //switch on SD1
  }
  delay(5);

  if (!SD.begin(CS)) {
    return SD1_INIT_FAIL + SD_CARD;
  }
  char userFile[14];
  time_t t;
  //*TODO
  t = 0;//RTC.get();
  //8.3 file names
  snprintf(userFile, 14, "d%04u-%02u.csv", minute(t), second(t));

  File dataFile = SD.open(userFile, FILE_WRITE);

  if (!dataFile) {
    SD.end();
    return SD_FILE_ERROR;
  }
  dataFile.println("TESTING TESTING TESTING");
  /*
    snprintf(userFile, 14, "d%04u-%02u.csv", year(t), month(t));

      File dataFile = SD.open(userFile, FILE_WRITE);

      if (dataFile) {
        x_data data_out;
        char cbuffer [250];

        for (unsigned int addr = sizeof(data_out); addr < wakes * sizeof(data_out); addr += sizeof(data_out)) {
          EEPROM_1.get(addr, data_out);
          data_out.getCSVData(cbuffer);
          dataFile.println(cbuffer);
        }
  */
  dataFile.close();

  SD.end();
  //Serial.println(freeRam());

  digitalWrite(EN_SD1, HIGH);//switch off SD1
  digitalWrite(EN_SD2, HIGH);//switch off SD2
  return SD_WRITE_OK;
}
#endif

/**
    Send data via WiFi modem (MD1) or Iridium Modem
*/
#ifdef WIFI_MODEM
void sendData(bool IRIDIUM) {


  MsgPack::Packer packer;
  packer.serialize(data);
#ifdef USE_IRIDIUM
  if (IRIDIUM) {
    //sendIridiumMessage(output_buffer);
    return sendIridiumMessage(packer.data(), packer.size());
  }
#endif
  long start = millis();
  boolean done = false;
  // read for incoming messages. c = send, x = don't send:
  while (!done) {
    if (timeout > millis() - start) {
      done = true;
    }
    while (MD1.available() > 0) {
      char inChar = MD1.read();
      switch (inChar) {
        case 'b': // begin
          {
            char cbuffer [50]; //allow for base 64 increase 8/6
            Base64.encode(cbuffer, (char*) packer.data(), (int) packer.size());
            MD1.println(cbuffer);
            done = true;
          }
          break;
        case 'e': // end
          done = true;
          break;
        default:
          break;
      }
    }
  }
}
#endif
// Main Menu
#ifdef USE_MENU
void mainMenu(slight_DebugMenu * pInstance) {
  Print &out = pInstance->get_stream_out_ref();
  char *command = pInstance->get_command_current_pointer();
  switch (command[0]) {
    case 'h':
    case 'H':
    case '?': {
        // help
        out.println();
        out.println(F("\t '?': this help"));
        out.println(F("\t 'i': test Iridium"));
        out.println(F("\t 'y': get Iridium time"));
        out.println(F("\t 't': Read RTC "));
        out.println(F("\t 'g': GPS time"));
        out.println(F("\t 'm': read memory"));
        out.println(F("\t 'w': Write memory "));
        out.println(F("\t 'p': Read PTU Data "));
        out.println(F("\t 'd': Read Snow Depth"));
        out.println(F("\t 's': Send data"));
        out.println(F("\t 'x': Send data on Iridium"));
        out.println(F("\t 'l': list SD card contents"));
        out.println(F("\t '1': Log data SD 1 "));
        out.println(F("\t '2': Log data SD 2 "));
        out.println(F("\t 'c': Clear Memory "));
        out.println(F("\t 'f': Free RAM "));
        out.println(F("\t 'z': Sleep 20 seconds "));
        out.println();
      } break;
    case 'd': {
        int i = getDepth(20.0);
        out.print(F(" Depth : "));
        out.println(i);
      } break;
#ifdef USE_IRIDIUM
    case 'i': {
        testIridium();
      } break;
    case 'y': {
        getIridiumTime();
      } break;
#endif
    case 'c': {
        //EEPROM_1.erase();
      } break;
    case 'm': {
        readMemory();
      } break;
#ifdef USE_GPS
    case 'g': {
        getGPS();
      } break;
#endif
    case 'f': {
        out.println(freeRam());
      } break;
#ifdef WIFI_MODEM
    case 's': {
        sendData(false);
      } break;
    case 'x': {
        sendData(true);
      } break;
#endif
#ifdef USE_SD
    case '1': {
        testSD(1);
        writeDataSD(1);
      } break;
    case '2': {
        testSD(2);
        writeDataSD(2);
      } break;
#endif
    case 't': {
        time_t t_rtc = RTC.get();
        displayTime(t_rtc);
      } break;
    case 'p': {
        updateXData(false);
      } break;
    case 'w': {
        writeDataEEPROM();
      } break;
    case 'z': {
        gotoSleep();
      } break;
    //---------------------------------------------------------------------
    default: {
        if (strlen(command) > 0) {
          out.print(F("command '"));
          out.print(command);
          out.println(F("' not recognized. "));
        }
        pInstance->get_command_input_pointer()[0] = '?';
        pInstance->set_flag_EOC(true);
      }
  } // end switch

}
#endif
void setup() {
  int error =0;
  setupPins();
  Serial.begin(9600);
  //Sensors on I2C
  error+=setupSensor();
  // set the RTC from the compile date
  //initialiseRTC();
  //Iridium: start the serial port connected to the satellite modem
#ifdef USE_IRIDIUM
  IM1.begin(19200);
  error+=testIridium();
#endif
  //EEPROM memory
  error+=setupEEPROM();
  // Initialize SD.
#ifdef USE_SD
  //testSD();
  //TODO use RTC
  //SdFile::dateTimeCallback(dateTime);
#endif
  //start the menu
#ifdef USE_MENU
  menu.set_callback(mainMenu);
  Serial.println(F(" --------  Station-X POST -----------"));
  menu.begin(true);
#endif
}
void loop() {
#ifdef USE_MENU
  menu.update();
#endif
#ifdef USE_IRIDIUM
        testIridium();
        getIridiumTime();
#endif
        //EEPROM_1.erase();
        readMemory();
#ifdef USE_GPS
        getGPS();
#endif
       Serial.println(freeRam());
#ifdef WIFI_MODEM
        sendData(false);
        sendData(true);
#endif
#ifdef USE_SD
        int err = 0;
        err +=testSD(1);
        err +=testSD(2);
        err +=writeDataSD(1);
        err +=writeDataSD(2);
        Serial.println(F("SD Errors: "));
        Serial.println(err);
#endif
//*TODO
        time_t t_rtc = 0;//RTC.get();
        displayTime(t_rtc);
        updateXData(false);
        writeDataEEPROM();
        //gotoSleep();
delay(15);
}
