#include "FastLED.h"
#define NUM_LEDS 32
CRGB leds[NUM_LEDS];
void setup() { 
  FastLED.addLeds<NEOPIXEL, 6>(leds, NUM_LEDS); 
  randomSeed(analogRead(0));
 
}
void loop() {
  // read the input on analog pin 0:
  //int sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  int red = random(64, 255);//sensorValue * (255.0 / 1023.0);
  int green = random(0, 128);//sensorValue * (255.0 / 1023.0);
  int blue = random(0, 64);//sensorValue * (255.0 / 1023.0);
  int time_wait = random (100,300);
  // print out the value you read:
  for(int i = 0 ; i < NUM_LEDS; i++) {
    if(random(0,10) > 5) {
      leds[i] = CRGB(red,green,blue); 
    }
  }
  FastLED.show();
  delay(time_wait);
//  for(int i = 0 ; i < NUM_LEDS; i++) {
//    leds[i] = CRGB::Black; FastLED.show(); 
//  
//  }
//  delay(30);
  
}
