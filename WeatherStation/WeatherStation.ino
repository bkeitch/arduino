/*

 */
#include <openGLCD.h>
//#include <LiquidCrystal.h>
#include <Wire.h>
#include "RTClib.h"

const int theDeviceAddress = 80;
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value converted to mV
byte value;
int addr = 0; 
int secondsRun = 0;


// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(7,8,9,10,11,12);

// initialize the RTC
RTC_DS1307 rtc;


void setup() {
  Wire.begin();
  Serial.begin(115200);
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  lcd.print("Pressure Time");
  rtc.begin();
  if (! rtc.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  } 
  Serial.print("Current date:  ");
  printDate(rtc.now());
  printTime(rtc.now());
  Serial.println();       
}
void printDate(DateTime now) {
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(' ');
}
void printTime(DateTime now) {
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  if(now.minute() < 10)
     Serial.print('0');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  if(now.second() < 10)
     Serial.print('0');
  Serial.print(now.second(), DEC);
}
void showTime(DateTime now) {
  lcd.print(now.hour());
  lcd.print(':');
  if(now.minute() < 10)
     lcd.print('0');
  lcd.print(now.minute());
  lcd.print(':');
  if(now.second() < 10)
     lcd.print('0');
  lcd.print(now.second());
}

void loop() {
  
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // map it to mV
  outputValue = map(sensorValue, 0, 1023, 0, 5000);
  // convert to nearest hPa as per data sheet
  unsigned int pressure = (unsigned int) ((outputValue + (5000*0.095))/ (100*5*.009));
  DateTime now = rtc.now();
  
  //store datestamp in EEPROM every 10 mins
  if(secondsRun%600==0) {
    WireEepromWriteLong(theDeviceAddress, addr, now.unixtime());//outputValue
    //Increment EEPROM address
    addr += sizeof(long);
    //store value in EEPROM
    WireEepromWriteInt(theDeviceAddress, addr, pressure);
    addr+=sizeof(int);
  }
    
  //Display current time and pressure
  lcd.setCursor(0, 1);
  if(pressure < 1000)
    lcd.print(' ');
  lcd.print(pressure);
  lcd.print("hPa");
  lcd.print(" ");
  showTime(now);
  
  //24 readings is 4 hours every 10 mins
  //144 Bytes of memory = 24 readings (6 bytes per measurement)  
  if(addr == 65535 ) //24A512 is 512Kibits = 64 Kibytes
    addr = 0;
  delay(1000);
  //Burst data to computer every 4 hours
  //if(secondsRun%(60*60*4)==0) {
  // send data only when you receive data:
  String txtMsg = "";  
  char s;
  int retrieve = 0;
  while (Serial.available() > 0) {
    s=(char)Serial.read();
    if (s == '\n') {
      retrieve = atoi(txtMsg.c_str());
      txtMsg = "";  
    } else {  
      txtMsg +=s; 
    }
  }
  if (retrieve > 0) {
    lcd.setCursor(0, 1);
    lcd.print(" *Sending Data* ");
    for (unsigned int theMemoryAddress = 0; theMemoryAddress < retrieve; ) {
      long timestamp = WireEepromReadLong(theDeviceAddress, theMemoryAddress);
      Serial.print(timestamp);
      theMemoryAddress +=4;     
      Serial.print(", ");
      Serial.print(WireEepromReadInt(theDeviceAddress, theMemoryAddress));
      theMemoryAddress +=2;
      Serial.println();
    }   
    addr = 0; //reset memory
    secondsRun=0; //reset hour count
  }
  ++secondsRun;   
}




void WireEepromRead(int theDeviceAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(theDeviceAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.endTransmission();
    delay(5);
    Wire.requestFrom(theDeviceAddress, sizeof(byte));
    theByteArray[theByteIndex] = Wire.read();
  }
}

byte WireEepromReadByte(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(byte)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(byte), theByteArray);
  return (byte)(((theByteArray[0] << 0)));
}

int WireEepromReadInt(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(int)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(int), theByteArray);
  return ((((int)theByteArray[0] << 8)) | (((int)theByteArray[1] << 0)));
}
long WireEepromReadLong(int theDeviceAddress, unsigned int theMemoryAddress) {
  byte theByteArray[sizeof(long)];
  WireEepromRead(theDeviceAddress, theMemoryAddress, sizeof(long), theByteArray);  
  return ((((long)theByteArray[0] << 24) |(((long)theByteArray[1] << 16) | ((long)theByteArray[2] << 8))) | ((long)theByteArray[3] << 0));
}
void WireEepromWrite(int theDeviceAddress, unsigned int theMemoryAddress, int theByteCount, byte* theByteArray) {
  for (int theByteIndex = 0; theByteIndex < theByteCount; theByteIndex++) {
    Wire.beginTransmission(theDeviceAddress);
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 8));
    Wire.write((byte)((theMemoryAddress + theByteIndex) >> 0));
    Wire.write(theByteArray[theByteIndex]);
    Wire.endTransmission();
    delay(5);
  }
}

void WireEepromWriteByte(int theDeviceAddress, unsigned int theMemoryAddress, byte theByte) {
  byte theByteArray[sizeof(byte)] = {(byte)(theByte >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(byte), theByteArray);
}

void WireEepromWriteInt(int theDeviceAddress, unsigned int theMemoryAddress, int theInt) {
  byte theByteArray[sizeof(int)] = {(byte)(theInt >> 8), (byte)(theInt >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(int), theByteArray);
}

void WireEepromWriteLong(int theDeviceAddress, unsigned int theMemoryAddress, long theLong) {
  byte theByteArray[sizeof(long)] = {(byte)(theLong >> 24), (byte)(theLong >> 16), (byte)(theLong >> 8), (byte)(theLong >> 0)};
  WireEepromWrite(theDeviceAddress, theMemoryAddress, sizeof(long), theByteArray);
}

