// Basic MD_AD9833 test file
//
// Initialises the device to default conditions
// 
// Connect a pot to A0 to change the frequency by turning the pot
//
#include <MD_AD9833.h>
#include <SPI.h>

// Pins for SPI comm with the AD9833 IC
#define DATA  11	///< SPI Data pin number
#define CLK   13	///< SPI Clock pin number
//#define FSYNC 10	///< SPI Load pin number (FSYNC in AD9833 usage)
#define CS_RES 9  ///< SPI Load pin number (FSYNC in AD9833 usage)
#define CS_DDS1 10
//MD_AD9833	AD(DATA,CLK, CS_DDS1);  // bit bang SPI
MD_AD9833  AD(CS_DDS1);  // Hardware SPI


String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

void setup() {
  // initialize serial:
  pinMode (CS_DDS1, OUTPUT);
  pinMode (CS_RES, OUTPUT);
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
//  ddsWrite(0x01c0); //reset
//  ddsWrite(0x00c0); //active
//  ddsWrite(0x20c0); //set freq 1kHz
//  ddsWrite(0x69f1);
//  ddsWrite(0x4000);
//  ddsWrite(0xc000); //set phase to 0
//  digitalPotWrite(128);  delay(500);
	AD.begin();
  //AD.SleepMode(false);

  //AD2.begin();
  //digitalPotWrite(128);
}

void loop(void)
{

//  delay(5000);
  float v;
  if (stringComplete) {
    
    v = inputString.toFloat();

    if(v==0 )
    {
      AD.setMode(MD_AD9833::MODE_OFF);
      Serial.println("Switching 1 Off");
    } else if(v==1) {
      AD.reset();
      Serial.println("Resetting");
    }
    else if(v<255) {
       //digitalPotWrite(v);
       //Serial.println("Setting amplitude  to : " + inputString);
       Serial.println("Setting phase  to : " + inputString);
       AD.setPhase(MD_AD9833::CHAN_0,v);
    } else {  
       AD.setMode(MD_AD9833::MODE_SINE);
       AD.setFrequency(MD_AD9833::CHAN_0,v);

       Serial.print("Set Frequency to : " );
       Serial.println(AD.getFrequency(MD_AD9833::CHAN_0));
    } 
    // clear the string:
    inputString = "";
    stringComplete = false;
  }

}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

int ddsWrite(unsigned int value)
{
  SPI.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE2));
  digitalWrite(CS_DDS1, LOW);
  delay(1);
  SPI.transfer((value >> 8 )& 0xFF );
  SPI.transfer(value & 0xFF);
  delay(1);
  digitalWrite(CS_DDS1, HIGH);
  SPI.endTransaction();
}
int digitalPotWrite(unsigned int value)
{
  SPI.beginTransaction(SPISettings(8000000, MSBFIRST, SPI_MODE2));
  digitalWrite(CS_RES, LOW);
  delay(1);
  SPI.transfer(0b00010001 );//XX C1 C0 XX P1 P0 write command channel 0
  SPI.transfer(value & 0xFF);
  delay(1);
  digitalWrite(CS_RES, HIGH);
  SPI.endTransaction();
}
