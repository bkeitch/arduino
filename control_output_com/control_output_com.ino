int int_array[] = {0,0,0,0
                   ,0,0,0,0,0};//D0-D8
String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete
const int analogWater = A2;  //
const int analogHeating = A3;  // 

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
  
  for (int ii = 4;ii<9;ii++){
    pinMode(ii,OUTPUT);
  }
  

}
void loop() {
  float sensorValue=0, outputValue =0;
  // print the string when a newline arrives:
  if (stringComplete) {
    // clear the string:
    Serial.println(inputString);
    int pin_num = inputString.toInt();
    if(pin_num > 3 && pin_num < 9) {
      int_array[pin_num] = !int_array[pin_num];
      digitalWrite(pin_num,int_array[pin_num]);
      
      
      Serial.print("Pin #");
      Serial.print(pin_num);
      
      if (int_array[pin_num]==0){
        Serial.println(" OFF");
      } else {
        Serial.println(" ON");
      }

    }
    if(pin_num ==9) {
      sensorValue = analogRead(analogWater);
      // map it to the range of the analog out:
      //outputValue = map(sensorValue, 0, 1023, 0, 5);

      // print the results to the Serial Monitor:
      Serial.print("Water = ");
      Serial.print(sensorValue);
      Serial.print("\t Heating = ");
      sensorValue = analogRead(analogHeating);
      // map it to the range of the analog out:
      //outputValue = map(sensorValue, 0, 1023, 0, 5);
      Serial.println(sensorValue);

    }
    inputString = "";
    stringComplete = false;
  }
}
/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
