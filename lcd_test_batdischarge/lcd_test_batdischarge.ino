#include <LiquidCrystal.h>


int lcdRS = 10;                               // Define output pins for LCD
int lcdE = 11;
int lcdD4 = 6;
int lcdD5 = 7;
int lcdD6 = 8;
int lcdD7 = 9;

LiquidCrystal lcd(lcdRS, lcdE, lcdD4, lcdD5, lcdD6, lcdD7); 
void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(16,2);
  // initialize the serial communications:
  Serial.begin(19200);
  for(int i= 0; i< 16; i++) {
    lcd.write(i+65);
    if (i==7) {
      lcd.setCursor(0,1);
    }
  }
}

void loop() {
  // when characters arrive over the serial port...
  if (Serial.available()) {
    // wait a bit for the entire message to arrive
    delay(100);
    // clear the screen
    lcd.clear();
    int ccount = 0;
    // read all the available characters
    while (Serial.available() > 0) {
      // display each character to the LCD
      char c = Serial.read();
      if (c != '\n') {
        lcd.write(c);
        ++ccount;
      }
      if (ccount == 8) { 
        lcd.setCursor(0,1);
      }
      if (ccount == 16) {
          lcd.setCursor(0,0);
          ccount = 0;
      }
    }
  }
}
