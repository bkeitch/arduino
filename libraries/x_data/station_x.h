#pragma once

#include <Arduino.h>
//#include "SdFat.h"
#ifdef USE_IRIDUM
#include <IridiumSBD.h>
#endif
#include <TimeLib.h>
// cd Arduino/libraries$
// git clone https://github.com/PaulStoffregen/Time.git
// git clone https://github.com/hideakitai/MsgPack.git
/*

  I2C bus (21 SCL 20 SDA)
  =======================
  RTC : DS3231  0x68
  Sensor: BME280
  FLASH1 0x53
  FLASH2

  Analogue
  ========
  Pressure: MPX4115 at 5V on A2
  External temp: A0
  Battery temp: A3
  Battery Voltage A4
  Solar voltage A6
  Battery current A8
  
  Serial
  ======
  GPS: SS Serial1 RX 19 TX 18 PWR D6
  Iridium Modem: Serial3 PWR D48

  SPI pins 10-13 (Purple on 13)
  =============================

  Digital:
  SD Card 1 SS : D53
  SD Card 2 SS : D49
  PSU 1 :  D5
  PSU 2 :  D6
  Interupt (Wake) : D2
  
   Ultrasonic Sensor HC-SR04
     VCC: +5VDC
     Trig : Trigger (INPUT) - Pin11
     Echo: Echo (OUTPUT) - Pin 12
     GND: GND
 */
 
 
  //Define inputs and outputs
  // pinMode(trigPin, OUTPUT);
  // pinMode(echoPin, INPUT);


#define DS3231_I2C_ADDRESS 0x68

//CHANGE ME!
#define IridiumSerial Serial1
#define GPSSerial Serial3

//24LC1025 I2C addresses
//24XX1026 control byte format: 1 0 1 0 A2 A1 B0
//24XX1025 control byte format: 1 0 1 0 B0 A2 A1

#define EEPROM_0_ADR_LOW_BLOCK  0x51  //0b.101.0001 
#define EEPROM_0_ADR_HIGH_BLOCK 0x55 //0b.101.0101 

#define EEPROM_1_ADR_LOW_BLOCK  0x52  //0b.101.0010 
#define EEPROM_1_ADR_HIGH_BLOCK 0x56 //0b.101.0110 

#define SD_1_CS_PIN   49
#define SD_2_CS_PIN   53
#define wake_up       2 //Pin from RTC (has to be 2 or 3)

static const int maxChars = 220; // limit of Iridium/Rock block

static const int months[] = { 281 ,269 ,288 ,291 ,295 ,301 ,299 ,285 ,296 ,294 ,307 ,268 };



/*


// 100 MiB file.
const uint32_t MAX_FILE_SIZE = (1 << 20);
#define ENABLE_DEDICATED_SPI 1
#define SD_CONFIG SdSpiConfig(SD_1_CS_PIN, DEDICATED_SPI)
#define SD_FAT_TYPE 1
#if SD_FAT_TYPE == 0
SdFat sd;
typedef File file_t;
#elif SD_FAT_TYPE == 1
SdFat32 sd;
typedef File32 file_t;
#elif SD_FAT_TYPE == 2
SdExFat sd;
typedef ExFile file_t;
#elif SD_FAT_TYPE == 3
SdFs sd;
typedef FsFile file_t;
#else  // SD_FAT_TYPE
#error Invalid SD_FAT_TYPE
#endif  // SD_FAT_TYPE

file_t binFile;

// Store error strings in flash to save RAM.
#define error(s) sd.errorHalt(&Serial, F(s))

void createBinFile();
void openBinFile();

void initialiseRTC();
void gotoSleep();
void wakeUp();
void wlog(char *message, bool sendOut);
void wlog(char *message, bool sendOut, data_t* binaryMsg);
*/
#ifdef USE_IRIDIUM
// Declare the IridiumSBD object
IridiumSBD modem(IridiumSerial);

#define debugprint(s) Serial.println(s)

const bool DEBUG = true; // debug. to do, use hardware
#define DIAGNOSTICS true // Iridiuim diagnostics

extern void displayTime(time_t t);

#include <x_data.h>

/**
 * Initialise Iridium connection on serial port
 * @returns error code as per Iridium modem
*/
int setupIridium() {
  int err;
  IridiumSerial.begin(19200);
  
// If we're powering the device by USB, tell the library to 
  // relax timing constraints waiting for the supercap to recharge.
  modem.setPowerProfile(IridiumSBD::USB_POWER_PROFILE);

  // Begin satellite modem operation
  debugprint("Starting Iridium");
  err = modem.begin();
  //modem.useMSSTMWorkaround(false);
  if (err != ISBD_SUCCESS)
  {
    debugprint("Iridium Setup failed: error ");
    debugprint(err);
    if (err == ISBD_NO_MODEM_DETECTED)
      debugprint("No modem detected: check wiring.");
   
  }
   return err;
}

/**
 * Get the current Iridium time and compare to current RTC. updates the difference in xd object.
 * @returns error code as per Iridium modem
*/

int getIridiumTime(time_t &t_iri) {
  int err = modem.getSystemTime(t_iri);
   if (err == ISBD_SUCCESS)
   {
 
     displayTime(t_iri);
   }
  time_t t_rtc = RTC.get();
  xd.updateIridiumOffset(t_rtc, t_gps);
  RTC.write(t_iri);
}
  return (err);
}

/**
  * Sends binary data over Iridium
  * @param packer Packed object to send
  * @param unpacker Received object in packed format
  * @returns error code as per Iridium modem
*/

int sendIridium(MsgPack::Packer &packer, MsgPack::Unpacker &unpacker) {

  if(DEBUG)
  {
    for(unsigned int i = 0 ; i < packer.size() ; i++) {
      Serial.print(packer.data()[i], HEX);
    }
    Serial.println();
  }

  
  int signalQuality = -1;
  int err;
  uint8_t buffer[200];
  bool messageSent = false;
 
  // Test the signal quality.
  // This returns a number between 0 and 5.
  // 2 or better is preferred.
  err = modem.getSignalQuality(signalQuality);
  if (err != ISBD_SUCCESS)
  {
    debugprint("SignalQuality failed: error ");
    debugprint(err);
    return err;
  }

  debugprint("Signal quality : ");
  debugprint(signalQuality);
  
  
  
  // Read/Write the first time or if there are any remaining messages
   // Send the message
  debugprint("Sending");
  
  size_t bufferSize = sizeof(buffer);
  
  if (signalQuality> 3  && (!messageSent || modem.getWaitingMessageCount() > 0))
  {

    // First time through send+receive; subsequent loops receive only
    if (!messageSent)
      err = modem.sendReceiveSBDBinary(packer.data(), packer.size(), buffer, bufferSize);
    else
      err = modem.sendReceiveSBDBinary(NULL, 0, buffer, bufferSize);
    if (err != ISBD_SUCCESS)
    {
      debugprint("sendReceiveSBD failed: error ");
      debugprint(err);
      if (err == ISBD_SENDRECEIVE_TIMEOUT)
        debugprint("TIMEDOUT");
    }
    else
    {
      debugprint("Iridium Sent OK");
      messageSent = true;
      debugprint("Inbound buffer size is ");
      debugprint(bufferSize);
      if(DEBUG) 
      {
        for (unsigned int i=0; i<bufferSize; ++i)
        {
          Serial.print(buffer[i], HEX);
        }
        Serial.println();
      }//end DEBUG      
      debugprint("Messages remaining to be retrieved: ");
      debugprint(modem.getWaitingMessageCount());
    }
  }  
  if(bufferSize > 0) {
    unpacker.feed(buffer, bufferSize);
    unpacker.deserialize(newConfig);
  }
  return err;
}


#if DIAGNOSTICS
void ISBDConsoleCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}

void ISBDDiagsCallback(IridiumSBD *device, char c)
{
  Serial.write(c);
}
#endif

#endif

#ifdef USE_GPS
/**
 * GPS module on (active LOW)
*/
void swOnGPS()
{
  digitalWrite(gpsCTRL, LOW);
}
/**
 * GPS module off (active LOW)
*/
void swOffGPS()
{
  digitalWrite(gpsCTRL, HIGH);
}
/**
  * Updates RTC from GPS data
  * @returns error codes from GPS
*/
int updateGPSTime() {
  bool timeNeeded = true;
  unsigned long start_time = millis();
  while (timeNeeded) {
    if (millis() - start_time > GPS_TIME_OUT) {
      timeNeeded = false;
      return GPS_TIMEOUT;
    }
    while (GPSSerial.available() > 0)
    {
      int c = GPSSerial.read();
      if (gps.encode(c)) {
        if (gps.time.isUpdated())
        {
          timeNeeded = false;
        }
      }
    }
  }
  time_t t_gps, t_rtc;
  t_rtc = RTC.get();
  //tmElements_t tm;
  setTime(month(t),year(t),day(t),gps.time.hour(),gps.time.minute(),gps.time.second());
  //setTime(tm.Month, tm.Year, tm.Day, tm.Hour, tm.Minute, tm.Second);
  t_gps = now();
  xd.updateGPSOffset(t_rtc, t_gps);
  RTC.write(t_gps);           // set the RTC from the tm structure
  displayTime();
  return GPS_OK;
}
/**
  * Update the GPS data - position and time
  * @returns error codes from GPS
*/
int updateGPSData()
{
  int toReturn = GPS_OK;
  swOnGPS();
  delay(30000); // warm up GPS 30 secs recommended

  bool posNeeded = true;
  unsigned long start_time = millis();
  while (posNeeded) {
    if (millis() - start_time > GPS_TIME_OUT) {
      posNeeded = false;
      return GPS_TIMEOUT;
    }
    while (GPSSerial.available() > 0)
    {
      int c = GPSSerial.read();
      if (gps.encode(c)) {
        if (gps.location.isUpdated())
        {
          posNeeded = false;
        }
      }
    }
  }
  
  toReturn = getGPSTime(); // as GPS is on, update time.
  swOffGPS();
  return (toReturn);
}
#endif
int updateXData() {
  unsigned int mem = 64;
  float bme_p, bme_t, bme_u;
  bme.read(bme_p, bme_t, bme_u, BME280::TempUnit_Celsius, BME280::PresUnit_Pa);
  time_t t = RTC.get();
 
  xd.update(bme_p, bme_t, bme_u, t, mem);
}
/* 
 * Ultrasonic Sensor HC-SR04
 *
        VCC: +5VDC
        Trig : Trigger (INPUT) - Pin11
        Echo: Echo (OUTPUT) - Pin 12
        GND: GND
 */

