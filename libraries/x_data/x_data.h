#ifndef XDATA_H
#define XDATA_H

//x_data.h
#include <stdint.h>
#include "config.h"

#ifdef ARDUINO
#include <MsgPack.h>
#include <TimeLib.h>
#else
#include <msgpack.hpp>
#include <time.h>
#endif  

/**
 * Data needed for SYNOP Message:
 * iii 	Station Number
 * TTT 	Dry-bulb temperature in tenths of a degree Celsius
 * TgTg 	Grass minimum temperature rounded to nearest whole degree
 * TxTxTx 	Maximum temperature 0600-1800 UTC in tenths of a degree
 * TnTnTn 	Minimum temperature 1800-0600 UTC in tenths of a degree
 * ff 	Wind speed in knots
 * dd 	Wind direction in tens of degrees
 * (f')f'f' 	Speed in knots of the maximum gust in the past hour (>33 kts)
 * PPPP 	Last four figs. of air pressure (reduced to mean sea level) in millibars and tenths
 * ppp 	Pressure change over last three hours in millibars and tenths
 * TdTdTd 	Dew-point temperature in tenths of a degree Celsius
 * sss 	Depth of snow
 * Also collects health data
*/
class x_data {
private:
  int16_t  Tair;
  int16_t  Tground;
  // int16_t  max_Tair;
  // int16_t  max_Tground;
  // int16_t  min_Tair;
  // int16_t  min_Tground;

  // uint16_t wind_speed;
  // uint16_t wind_dir;
  // uint16_t max_gust;
  uint16_t P;
  int8_t   P_trend;
  int16_t  T2;
  uint16_t P2;
  uint16_t U;
  uint8_t  depth;
  uint16_t status;
  uint32_t uptime;
  uint16_t v_batt;
  uint16_t v_solar;
  uint16_t i_charge;
  uint16_t memory;
  uint32_t rtc_time;
  int8_t   iridium_offset;
  int8_t   gps_offset;

public:
    bool operator!= (const x_data& x) const
  {
      return (
         (x.Tair               !=   Tair             )
      || (x.Tground            !=   Tground          )
      || (x.P                  !=   P                )
      || (x.P_trend            !=   P_trend          )
      || (x.T2                 !=   T2               )
      || (x.P2                 !=   P2               )
      || (x.U                  !=   U                )
      || (x.depth              !=   depth            )
      || (x.status             !=   status           )
      || (x.uptime             !=   uptime           )
      || (x.v_batt             !=   v_batt           )
      || (x.v_solar            !=   v_solar          )
      || (x.i_charge           !=   i_charge         )
      || (x.memory             !=   memory           )
      || (x.rtc_time           !=   rtc_time         )
      || (x.iridium_offset     !=   iridium_offset   )
      || (x.gps_offset         !=   gps_offset       )
    );
  }

  MSGPACK_DEFINE(
  Tair             , 
  Tground          , 
  P                , 
  P_trend          ,
  T2               , 
  P2               , 
  U                , 
  depth            , 
  status           , 
  uptime           , 
  v_batt           , 
  v_solar          , 
  i_charge         , 
  memory           , 
  rtc_time         ,  
  iridium_offset   , 
  gps_offset
  );
public:

	void getCSVData (char* csvdata);
	void getJSONData (char *message);
	int  updateGPSOffset(time_t rtc_time, time_t gps_time); 
	int  updateIridiumOffset(time_t rtc_time, time_t iridium_time);	
	void update(float T2, float P2, float U, int d, time_t t, unsigned int memory_count);

private:

	//If RTC is more than 10 minutes out ignore
	constexpr static long 	MAX_TIME_DIFF 	   = 600UL;
	constexpr static int    SAMPLE_NUMBER      = 10;
	constexpr static double BALANCE_RESISTOR   = 20000.0;
	constexpr static double MAX_ADC            = 1023.0;
	constexpr static double BETA               = 4013.0;
	constexpr static double ROOM_TEMP          = 298.15;   //in Kelvin
	constexpr static double RESISTOR_ROOM_TEMP = 20000.0;
	constexpr static double VREF = 5000; //mV 
	constexpr static double IREF = VREF/0.185;

	bool gpsPower = false;

	void        storeToMemory();
	void        updateStatus();


	uint32_t    readPressure();
	int32_t     readThermistor(int thermistorPin);
	double      readVoltage(int voltagePin);


};
#ifndef ARDUINO
  #define A0 1
  #define A1 1
  #define A2 1
  #define A3 1
  #define A4 1
  #define A5 1
  #define A6 1
  #define A7 1
  #define A8 1
  #define A9 1
#endif
/**
  Hardware            : Mega

  I2C bus (21 SCL 20 SDA)
  =======================
  RTC DS3231          : 0x68
  Sensor BMPE280      : 0x76
  EEPROM_1            : 0x50 (0,0)
  EEPROM_1            : N/C
   
*/

//If the EEPROM address is less than the 64k byte threshold we use I2C address 0x50
//If the address is above 65535 then we use 0x54 address
static const int  EEPROM_0_ADR_LOW_BLOCK_1 = 0x50;
//static const int  EEPROM_0_ADR_LOW_BLOCK_2 = 0x54;
static const int  DS3231_I2C_ADDRESS = 0x68;
   
/**
  Analogue
  ========
  Pressure(MPX4115 5V): A0
  Battery Voltage     : A1
  Solar voltage       : A2
  Battery current     : N/C
  External temp       : A4
  Battery temp        : A6


*/
//need "A" in front for Arduino Mega. remove for ESP
#define externalTPin  A4
#define batteryTPin   A6
#define pressurePin   A0 
#define vBatteryPin   A1 
#define vSolarPin     A2 
#define iBatteryPin   A8 

/**

  Serial
  ======
  GPS (Serial_1)      : RX 19 TX 18 PWR D29
  Modem (Serial_3)    : RX 15 TX 14 PWR D27 sleep D17

*/
#ifdef ARDUINO
static  HardwareSerial& NEO1 = Serial1;
static  HardwareSerial& MD1  = Serial; //serial 2 if using modem
static  HardwareSerial& IM1  = Serial3; //CHANGE to 2 FOR ESP
#endif

/**

  Digital
  -------
  
  SD Card CD1         : D48
  SD Card CD2         : D49
  SNOW_SIG            : D5
  SNOW_TRIG           : D4
  RTC Interrupt(Wake) : D2 

  POWER CONTROL
  =============

  EN_WIND             : D25
  EN_MODEM            : D27
  EN_GPS              : D29
  EN_SNOW             : D31
  EN_SD1              : D36
  EN_SD2              : D38
  EN_EEPROM           : D40
  EN_SENSE            : D42
  
*/

static constexpr int SD_1_CS   = 48;
static constexpr int SD_2_CS   = 49;
static constexpr int SNOW_SIG  = 5 ;// Echo
static constexpr int SNOW_TRIG = 4 ;// Trigger
static constexpr int WAKE_UP   = 2 ;
			
static constexpr int EN_WIND   = 25;
static constexpr int EN_MODEM  = 27;
static constexpr int EN_GPS    = 29;
static constexpr int EN_SNOW   = 31;
static constexpr int EN_SD1    = 36;
static constexpr int EN_SD2    = 38;
static constexpr int EN_EEPROM = 40;
static constexpr int EN_SENSE  = 42;
			
static constexpr int SLEEP     = 17;

#ifdef ARDUINO
static void setupPins() {
  //Define inputs and outputs

	pinMode(SNOW_SIG    , INPUT);
	pinMode(SD_1_CS     , OUTPUT);
	pinMode(SD_2_CS     , OUTPUT);
	pinMode(SNOW_TRIG   , OUTPUT);
	pinMode(EN_WIND     , OUTPUT);
	pinMode(EN_MODEM    , OUTPUT);
	pinMode(EN_GPS      , OUTPUT);
	pinMode(EN_SNOW     , OUTPUT);
	pinMode(EN_SD1      , OUTPUT);
	pinMode(EN_SD2      , OUTPUT);
	pinMode(EN_EEPROM   , OUTPUT);
	pinMode(EN_SENSE    , OUTPUT);
	pinMode(SLEEP       , OUTPUT);
	pinMode(WAKE_UP, INPUT_PULLUP); //Set pin input using the built-in pullup resistor
	
	//digitalWrite(SD_1_CS, HIGH);
	//digitalWrite(SD_2_CS, HIGH);
	digitalWrite(SLEEP  , LOW); // modem to sleep
}

#endif
#define SD1_INIT_FAIL 1025;
#define SD2_INIT_FAIL  1026;
#define SD_TEST_OK  0;
#define EEPROM_NOT_DETECTED 1027;
#define EEPROM_TEST_OK  0;
#define BME_NOT_FOUND 1028;
#define BME_SUCCESS  0;
#define SD_FILE_ERROR  1024;
#define SD_WRITE_OK  0;


#endif /* XDATA_H */

















