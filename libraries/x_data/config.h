#ifndef CONFIG_H
#define CONFIG_H
#ifdef ARDUINO
#include <stdint.h>
#include <MsgPack.h>
#else
#include <cstdint>
#include <msgpack.hpp>
#endif

class config {
  public:
    uint32_t f_version: 6,
    gps: 1,
    iridium: 2,
    data_count: 6,
    en_ptu: 1,
    en_sd: 1,
    en_gps: 1,
    en_depth: 1,
    en_wind: 1,
    en_iridium: 1,
    en_magno: 1,
    reset: 1;


  operator uint32_t() const {
    uint32_t b = 0;
    uint32_t a = f_version;
    b += a << 26;
    a = gps;
    b += a << 25;
    a = iridium;
    b += a << 23;
    a = data_count;
    b += a << 17;
    a = en_ptu;
    b += a << 16;
    a = en_sd;
    b += a << 15;
    a = en_gps;
    b += a << 14;
    a = en_depth;
    b += a << 13;
    a = en_wind;
    b += a << 12;
    a = en_iridium;
    b += a << 11;
    a = en_magno;
    b += a << 10;
    a = reset;
    b += a << 9;
    return b;
  }

  config & operator = (uint32_t i) {
    this->set(i);
    return *this;
  }

  void set(uint32_t i) {
    f_version  = (i & 0b11111100000000000000000000000000) >> 26;
    gps        = (i & 0b00000010000000000000000000000000) >> 25;
    iridium    = (i & 0b00000001100000000000000000000000) >> 23;
    data_count = (i & 0b00000000011111100000000000000000) >> 17;
    en_ptu     = (i & 0b00000000000000010000000000000000) >> 16;
    en_sd      = (i & 0b00000000000000001000000000000000) >> 15;
    en_gps     = (i & 0b00000000000000000100000000000000) >> 14;
    en_depth   = (i & 0b00000000000000000010000000000000) >> 13;
    en_wind    = (i & 0b00000000000000000001000000000000) >> 12;
    en_iridium = (i & 0b00000000000000000000100000000000) >> 11;
    en_magno   = (i & 0b00000000000000000000010000000000) >> 10;
    reset      = (i & 0b00000000000000000000001000000000) >> 9;
  }
  config() {
    f_version = 1;
    gps = 1;
    iridium = 2;
    data_count = 0;
    en_ptu = 1;
    en_sd = 1;
    en_gps = 1;
    en_depth = 1;
    en_wind = 1;
    en_iridium = 1;
    en_magno = 1;
    reset = 0;
  }
  config(uint32_t i) {
    this->set(i);
  }
};
/**
 * double  %lf // gps_x           in 1/1000 dec degree
 * double  %lf // gps_y           in 1/1000 dec degree
 */
class x_config {

  public:
  uint16_t station_id = 44;
  uint32_t x_packed;
  double location_x;
  double location_y;
  uint16_t checksum;
  public:
  MSGPACK_DEFINE(
    station_id,
    x_packed,
    location_x,
    location_y,
    checksum
  );

  private:
  
  const int time_interval = 2;
  config xs;

  public:
    
  x_config(uint32_t in ) {
    xs = in ;
  }
  void packConfig() {
    x_packed = xs;
  }
  config getConfig() {
    return xs;
  }
  void setLocation(double x, double y) {
    location_x = x;
    location_y = y;
  }
  void setConfig(uint32_t in ) {
    xs = in ;
  }
  void unpackConfig() {
    xs = x_packed;
  }
  void updateChecksum() {
    checksum = calculateChecksum();
  }
  bool verifyChecksum() {
    return (checksum == calculateChecksum());
  }
  private:
  /** function: calculateChecksum
   * calculates checksum of internal configuration object
   * to prevent mis/accidental configuration
   * Uses algorithm used by IP packets.
   * Last step folds a 32-bit partial checksum into 16 bits
   * @returns the inverted and folded checksum 
   * 
   */
  uint16_t calculateChecksum() {
    uint32_t checksum = 0;
    const uint32_t data = (this->xs);
    //sum bytes
    checksum = (data >> 16) + (data & 0xFFFF);
    //fold
    while (checksum > 0xFFFF) {
      checksum = (checksum >> 16) + (checksum & 0xFFFF);
    }
    //invert
    return ~checksum;
  }
};
#endif //CONFIG_H