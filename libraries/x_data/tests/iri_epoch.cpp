#include<iostream>
#include<time.h>
int main() {
 // Latest epoch began at May 11, 2014, at 14:23:55 UTC.
   struct tm  epoch_start;
   //tmElements_t epoch_start;
   epoch_start.tm_year = 2014 - 1900;
   epoch_start.tm_mon = 5 - 1;
   epoch_start.tm_mday = 11;
   epoch_start.tm_hour = 14;
   epoch_start.tm_min = 23;
   epoch_start.tm_sec = 55;

   unsigned long ticks_since_epoch = 0X8b01c597;

   /* Strategy: we'll convert to seconds by finding the largest number of integral
      seconds less than the equivalent ticks_since_epoch. Subtract that away and 
      we'll be left with a small number that won't overflow when we scale by 90/1000.

      Many thanks to Scott Weldon for this suggestion.
   */
   unsigned long secs_since_epoch = (ticks_since_epoch / 1000) * 90;
std::cout << "Tics from sat: " << secs_since_epoch << std::endl; 
  unsigned long small_ticks = ticks_since_epoch - (secs_since_epoch / 90) * 1000;
std::cout << "Offset tics: " << small_ticks << std::endl; 
   secs_since_epoch += small_ticks * 90 / 1000;
std::cout << "Calculated secs from sat: " << secs_since_epoch << std::endl; 

   time_t epoch_time_a = mktime(&epoch_start);
std::cout << "Epoch time in secs " << epoch_time_a << std::endl; 
   time_t adjusted_time = epoch_time_a + secs_since_epoch;
   //breakTime(adjusted_time, (tmElements_t&) tm);
std::cout << "Corrected time " << adjusted_time << std::endl; 
struct tm *atm = gmtime(&adjusted_time);
std::cout << (1900 + atm->tm_year) << "-" << (1+atm->tm_mon) << "-" << atm->tm_mday << " T " << atm->tm_hour << ":" <<atm->tm_min <<
	":" << atm->tm_sec << std::endl;


}
