#include<iostream>
#include "../config.h"
#include<msgpack.hpp>
void display(x_config* sx);
int main () {

  
  config xs;
  x_config sx(xs);
  sx.setLocation(-1.2577,51.7520);
  display(&sx);
  sx.updateChecksum();
  display(&sx);
  
  xs.f_version = 3;
  xs.gps = 1;
  xs.iridium  = 3; 
  xs.data_count = 24; 
  xs.en_ptu =1;
  xs.en_sd = 0;
  xs.en_gps= 0; 
  xs.en_depth = true; 
  xs.en_wind = false;
  xs.en_iridium = true;
  xs.en_magno = false;
  xs.reset = false;
  x_config sy(xs);
  sy.setLocation(68.1238,-67.5638);
  sy.updateChecksum();
  display(&sy);
  
  sy.packConfig();
  std::stringstream sbuf;
  msgpack::pack(sbuf, sy);

  
  for(std::string::size_type i = 0; i < sbuf.str().size(); ++i) {
    std::cout  << std::hex  << std::uppercase << (int16_t) sbuf.str()[i] ;
  } 
  //<< std::hex  << std::uppercase 
  std::cout << std::endl; 
  msgpack::object_handle oh =
      msgpack::unpack(sbuf.str().data(), sbuf.str().size());
      
      
  msgpack::object obj = oh.get();
  x_config x3(0);

  obj.convert(x3);
  x3.unpackConfig();
  display(&x3);
  
}
void display(x_config* sx) {
  config xs = (sx->getConfig());

#ifndef ARDUINO

  std::cout << "UNIT32: " << std::hex  << std::uppercase  << (uint32_t) sx->getConfig()<< std::endl;

  std::cout << std::hex
  << "station_id  " << sx->station_id  << "\n"
  << "lat  " << sx->location_y  << "\n"
  << "long  " << sx->location_x  << "\n"
  << std::dec
  << "f_version   " << xs.f_version   << "\n"
  << "gps         " << xs.gps         << "\n"
  << "iridium     " << xs.iridium     << "\n"
  << "data_count  " << xs.data_count  << "\n"
  << "en_ptu      " << xs.en_ptu      << "\n"
  << "en_sd       " << xs.en_sd       << "\n"
  << "en_gps      " << xs.en_gps      << "\n"
  << "en_depth    " << xs.en_depth    << "\n"
  << "en_wind     " << xs.en_wind     << "\n"
  << "en_iridium  " << xs.en_iridium  << "\n"
  << "en_magno    " << xs.en_magno    << "\n"
  << "reset       " << xs.reset       << "\n"
  << std::hex
  << "check_sum   " << sx->checksum    << "\n"
  << "csum_valid  " << sx->verifyChecksum() << "\n"  << std::endl;     

#else

  Serial.println(sx->station_id);
  Serial.println(sx->location_y);
  Serial.println(sx->location_x);
  Serial.println(xs.f_version    ,DEC);
  Serial.println(xs.gps          ,DEC);
  Serial.println(xs.iridium      ,DEC);
  Serial.println(xs.data_count   ,DEC);
  Serial.println(xs.en_ptu       ,DEC);
  Serial.println(xs.en_sd        ,DEC);
  Serial.println(xs.en_gps       ,DEC);
  Serial.println(xs.en_depth     ,DEC);
  Serial.println(xs.en_wind      ,DEC);
  Serial.println(xs.en_iridium   ,DEC);
  Serial.println(xs.en_magno     ,DEC);
  Serial.println(xs.reset        ,DEC);
  Serial.println(sx->checksum    ,HEX);
  Serial.println(sx->verifyChecksum());     
  
  
#endif                   
}
       
