#include<iostream>
#include "../x_data.h"
#include<msgpack.hpp>
#include<ctime>
#include<cstdlib>
#include <tgmath.h> //sqrt
//g++ ../x_data.cpp data_test.cpp -o data_test
void displayTime(time_t t)
{
  std::time_t now = std::time(NULL);
  std::tm * ptm = std::localtime(&now);
  char buffer[32];
  // Format: Mo, 15.06.2009 20:20:00
  std::strftime(buffer, 32, "%Y-%m-%d T %H:%M:%S", ptm);  
  std::cout << buffer << std::endl;
}
int main () {

  char buffer[200];
  srand (time(NULL));
  x_data xd;
  unsigned int mem = rand()%100;
  float P2, T2, U;
  P2 = rand();
  T2 = rand()/100-100;
  U = (rand()%10000)/100;
  time_t t = std::time(NULL);
 
  xd.update(T2, P2, U, t, mem);
  xd.updateGPSOffset(t, t+rand()%100);
  xd.updateIridiumOffset(t, t-rand()%100);
  /*
  xd.getJSONData(buffer);  
  //xd.getCSVData(buffer);  
  std::cout << "{\"before\":" << buffer << "," << std::endl;
  
  std::stringstream sbuf;
  msgpack::pack(sbuf, xd);

    std::cout << "\"tx_data\":\"" ;

  for(std::string::size_type i = 0; i < sbuf.str().size(); ++i) {
    std::cout  << std::hex  << std::uppercase << (int16_t) sbuf.str()[i] ;
  } 
  //<< std::hex  << std::uppercase 
    std::cout << "\"," << std::endl;
    */
  std::stringstream sbuf;
  msgpack::pack(sbuf, xd);
  std::cout << sbuf.str() << std::endl;
  /*
  msgpack::object_handle oh =
      msgpack::unpack(sbuf.str().data(), sbuf.str().size());
      
      
  msgpack::object obj = oh.get();
  x_data xd2;
  std::cerr << "Matches: " << (xd2 != xd) <<  std::endl;
  obj.convert(xd2);
  xd2.getJSONData(buffer);
  //xd2.getCSVData(buffer);  
  std::cout << "\"after\":"   << buffer << "}" << std::endl;
*/
}

       
