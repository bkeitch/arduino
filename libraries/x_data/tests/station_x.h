#pragma once
//station_x_h

#include <stdint.h>
//#include <msgpack.h>
//#include <assert.h> 
//#include <MsgPack.h>
#include <stdbool.h>

#define NUMBER_OF_VARIABLES 18
struct data {
  int16_t  Tair;
  int16_t  Tbat;
  uint32_t P;
  int16_t  bme_t;
  uint32_t bme_p;
  uint16_t bme_u;
  uint16_t depth;
  uint16_t status;
  uint16_t uptime;
  uint16_t v_batt;
  uint16_t v_solar;
  uint16_t i_charge;
  uint16_t memory;
  uint32_t rtc_time;
  
  int8_t   iridium_offset;
  int8_t   gps_offset;
  int32_t  gps_x;
  int32_t  gps_y;
 /* 
      bool operator!= (const data& x) const
    {
        return (
		     (x.Tair               !=   Tair             )
		  || (x.Tbat               !=   Tbat             )
		  || (x.P                  !=   P                )
		  || (x.bme_t              !=   bme_t            )
		  || (x.bme_p              !=   bme_p            )
		  || (x.bme_u              !=   bme_u            )
		  || (x.depth              !=   depth            )
		  || (x.status             !=   status           )
		  || (x.uptime             !=   uptime           )
		  || (x.v_batt             !=   v_batt           )
		  || (x.v_solar            !=   v_solar          )
		  || (x.i_charge           !=   i_charge         )
		  || (x.memory             !=   memory           )
		  || (x.rtc_time           !=   rtc_time         )
		  || (x.iridium_offset     !=   iridium_offset   )
		  || (x.gps_offset         !=   gps_offset       )
		  || (x.gps_x              !=   gps_x            )
		  || (x.gps_y              !=   gps_y            )
		);
    }

    MSGPACK_DEFINE(
		Tair             , 
		Tbat             , 
		P                , 
		bme_t            , 
		bme_p            , 
		bme_u            , 
		depth            , 
		status           , 
		uptime           , 
		v_batt           , 
		v_solar          , 
		i_charge         , 
		memory           , 
		rtc_time         ,  
		iridium_offset   , 
		gps_offset       , 
		gps_x            , 
		gps_y		
		);
 */
 };
 
typedef struct data data_t;
/*
static char *pack(const data_t *s, int num, int *size);
static data_t *unpack(const void *ptr, int size, int *num);

static char *pack(const data_t *s, int num, int *size) {
  //assert(num > 0);
  char *buf = NULL;
  msgpack_sbuffer sbuf;
  msgpack_sbuffer_init(&sbuf);
  msgpack_packer pck;
  msgpack_packer_init(&pck, &sbuf, msgpack_sbuffer_write);
  //The array will store `num` contiguous blocks 
  msgpack_pack_array(&pck, NUMBER_OF_VARIABLES * num);
  for (int i = 0; i < num; ++i) {
	msgpack_pack_int16(&pck, s[i].Tair);                
	msgpack_pack_int16(&pck, s[i].Tbat);                
	msgpack_pack_uint32(&pck, s[i].P);                
	msgpack_pack_int16(&pck, s[i].bme_t);            
	msgpack_pack_uint32(&pck, s[i].bme_p);            
	msgpack_pack_uint16(&pck, s[i].bme_u);            
	msgpack_pack_uint16(&pck, s[i].depth);            
	msgpack_pack_uint16(&pck, s[i].status);           
	msgpack_pack_uint16(&pck, s[i].uptime);           
	msgpack_pack_uint16(&pck, s[i].v_batt);           
	msgpack_pack_uint16(&pck, s[i].v_solar);          
	msgpack_pack_uint16(&pck, s[i].i_charge);         
	msgpack_pack_uint16(&pck, s[i].memory);           
	msgpack_pack_uint32(&pck, s[i].rtc_time);         
	msgpack_pack_int8(&pck, s[i].iridium_offset);   
	msgpack_pack_int8(&pck, s[i].gps_offset);       
	msgpack_pack_int32(&pck, s[i].gps_x);            
	msgpack_pack_int32(&pck, s[i].gps_y);            
  }
  *size = sbuf.size;
  buf = malloc(sbuf.size);
  memcpy(buf, sbuf.data, sbuf.size);
  msgpack_sbuffer_destroy(&sbuf);
  return buf;
}

static data_t *unpack(const void *ptr, int size, int *num) {
  data_t *s = NULL;
  msgpack_unpacked msg;
  msgpack_unpacked_init(&msg);
  if (msgpack_unpack_next(&msg, ptr, size, NULL)) {
    msgpack_object root = msg.data;
    if (root.type == MSGPACK_OBJECT_ARRAY) {
      assert(root.via.array.size % NUMBER_OF_VARIABLES == 0);
      *num = root.via.array.size / NUMBER_OF_VARIABLES;
      s = malloc(root.via.array.size*sizeof(*s));
      for (int i = 0, j = 0; i < root.via.array.size;  j++) {
		s[j].Tair =            root.via.array.ptr[i++].via.i64;
		s[j].Tbat =            root.via.array.ptr[i++].via.i64;
		s[j].P =               root.via.array.ptr[i++].via.u64;
		s[j].bme_t =           root.via.array.ptr[i++].via.i64;
		s[j].bme_p =           root.via.array.ptr[i++].via.u64;
		s[j].bme_u =           root.via.array.ptr[i++].via.u64;
		s[j].depth =           root.via.array.ptr[i++].via.u64;
		s[j].status    =       root.via.array.ptr[i++].via.u64;
		s[j].uptime    =       root.via.array.ptr[i++].via.u64;
		s[j].v_batt    =       root.via.array.ptr[i++].via.u64;
		s[j].v_solar   =       root.via.array.ptr[i++].via.u64;
		s[j].i_charge =        root.via.array.ptr[i++].via.u64;
		s[j].memory   =        root.via.array.ptr[i++].via.u64;
		s[j].rtc_time =        root.via.array.ptr[i++].via.u64;
		s[j].iridium_offset =  root.via.array.ptr[i++].via.i64;
		s[j].gps_offset     =  root.via.array.ptr[i++].via.i64;
		s[j].gps_x          =  root.via.array.ptr[i++].via.i64;
		s[j].gps_y          =  root.via.array.ptr[i++].via.i64;
      }  
    }    
  }      
  msgpack_unpacked_destroy(&msg);
  return s;
}
*/

