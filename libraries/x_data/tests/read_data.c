#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include "station_x.h"
//#include <msgpack.h>
#include <assert.h> 

int main (void) {
   
   FILE *fp;


	struct stat info;
	const char *filename = "test_data.bin";
	if (stat(filename, &info) != 0) {
		/* error handling */
	}   
	printf("FILE SIZE: %lu\n", (unsigned long)info.st_size);

	data_t *content = malloc(info.st_size);
	if (content == NULL) {
		/* error handling */
	}   
	fp = fopen(filename, "rb");
	if (fp == NULL) {
		/* error handling */
	}
	/* Try to read a single block of info.st_size bytes */
	size_t blocks_read = fread(content, info.st_size, 1, fp);
	if (blocks_read != 1) {
		/* error handling */
	}
	fclose(fp);

  
  
   /** UNPACK */
  int num;
 // data_t *s = unpack(content, info.st_size, &num);
  //data_t sa[]  = content;

  /** CHECK */
  //assert(num == (int) 1);
  printf("Tair C\tTbat C\tP HPa\tT2 C\tP2 HPa\tHumidity\%%\tDepth cm\tStatus\tUptime s\tVbat V\tVsolar V\tI mA\tMemory kB\tTime s\tIridium Offset s\tGPS Offset s\tLongitude\tLattitude\n");
  for (int ii = 0 ; ii < (info.st_size/(sizeof(data_t))); ii++) {
  data_t *s  = &content[ii];
	 // printf ("%02X", (uint8_t) &content[ii]);
  //printf("\n\tData unpacked (received)\n\n");
//  printf("Tair\t\t%.2f C\t\t\nTbat\t\t%.3f C\nP\t\t%.3f HPa\nT2\t\t%.2f C\nP2\t\t%.2f HPa\nHumidity\t%02.2f %%\nDepth\t\t%.2f cm\nStatus\t\t%0X\nUptime\t\t%d s\nVbat\t\t%.3f V\nVsolar\t\t%.3f V\nI\t\t%3f mA\nMemory\t\t%d kB\nTime\t\t%d s\nIridium Offset\t%d s\nGPS Offset\t%d s\nLongitude\t%d\nLattitude\t%d\n",
  printf("%.2f\t%.3f\t%.3f\t%.2f\t%.2f\t%02.2f\t%.2f\t%0X\t%d\t%.3f\t%.3f\t%3f\t%d\t%d\t%d\t%d\t%d\t%d\n",
	 s->Tair/1000.0,
	 s->Tbat/1000.0,
	 s->P/100.0,
	 s->bme_t/100.0,
	 s->bme_p/100.0,
	 s->bme_u/100.0,
	 s->depth/100.0,
	 s->status,
	 s->uptime,
	 s->v_batt/1000.0,
	 s->v_solar/1000.0,
	 s->i_charge/1.0,
	 s->memory,
	 s->rtc_time,                     
	 s->iridium_offset,
	 s->gps_offset,
	 s->gps_y,
	 s->gps_x
	 );
	
  } 
  
   return 0;
   
}
