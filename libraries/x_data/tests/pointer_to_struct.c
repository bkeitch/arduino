#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../station_x.h"
#include <msgpack.h>
#include <assert.h> 

void writeEEPROM(long eeAddress, byte data)
{
 
}

byte readEEPROM(long eeaddress)
{
  
  return 0;
}

void writeEEPROMPage(long eeAddress, byte * data)
{
  for (byte x = 0 ; x < MAX_I2C_WRITE ; x++)
	printf("%02X", data[x]); //Write the data
}

int main (void) {

	unsigned char* buffer = NULL;
	data_t *data1 = malloc( 1 * sizeof (data_t) );

	data1->Tair = -4071;
	data1->Tbat = -3010;
	data1->P = 105603;
	data1->bme_t = -5004;
	data1->bme_p =86821;
	data1->bme_u = 5032;
	data1->depth = 23416;
	data1->status = 64;
	data1->uptime = 50000;
	data1->memory = 6;
	data1->v_batt = 12120;
	data1->v_solar = 13450;
	data1->i_charge = 123;
	data1->rtc_time = 1232130048;
	data1->iridium_offset = -3;
	data1->gps_offset = 2;
	data1->gps_y = -5200234;
	data1->gps_x = 7339;
	buffer = malloc( 1 * sizeof (data_t) );
memcpy(buffer, data1, sizeof(data_t));

	buffer = (unsigned char*) (data1);
	// printf("T %04X %09d\tP %08X %09d\tTb %04X %09d\tPb %08X %09d\tH %04X %09d\tD %04X %09d\tS %04X\nU %04X %09d\tM %04X %09d\tVb %04X %09d\tVs %04X %09d\tI %04X %09d\tt%04X %09d\tti%04X %09d\ttg%04X %09d\tY %08X %09d\tX %08X %09d\n",
	// data1->T,                   data1->T,
	// data1->P,                   data1->P,
	// data1->bme_t,               data1->bme_t,
	// data1->bme_p,               data1->bme_p,
	// data1->bme_u,               data1->bme_u,
	// data1->depth,               data1->depth,
	// data1->status,              data1->status,
	// data1->uptime,              data1->uptime,
	// data1->v_batt,              data1->v_batt,
	// data1->v_solar,             data1->v_solar,
	// data1->i_charge,            data1->i_charge,
	// data1->memory,              data1->memory,
	// data1->rtc_time,            data1->rtc_time,                     
	// data1->iridium_offset,      data1->iridium_offset,
	// data1->gps_offset,          data1->gps_offset,
	// data1->gps_x,               data1->gps_x,
	// data1->gps_y,               data1->gps_y
	// );
printf("\n\n\tCurrent Data : \n");

printf("Tair %d\tTbat %d\tP  %d\tT2  %d\tP2  %d\tH  %d\tD  %d\tS %0X\nU  %d\tVb  %d\tVs  %d\tI %d\tM %d\tt %d\nti %d\ttg %d\tX  %d\tY  %d\n",
	 data1->Tair,
	 data1->Tbat,
	 data1->P,
	 data1->bme_t,
	 data1->bme_p,
	 data1->bme_u,
	 data1->depth,
	 data1->status,
	 data1->uptime,
	 data1->v_batt,
	 data1->v_solar,
	 data1->i_charge,
	 data1->memory,
	 data1->rtc_time,                     
	 data1->iridium_offset,
	 data1->gps_offset,
	 data1->gps_x,
	 data1->gps_y
	 );

long  address = 0;
byte * pointer_to_data = (byte*) &data1[0];
  printf("\n\tDumping Satellite Data... \n\n");

 for(int i = 0 ; i < (int) (sizeof(data_t)/(float) MAX_I2C_WRITE); i++) {
 //&buffer[i*MAX_I2C_WRITE]
	 writeEEPROMPage(address, pointer_to_data );
	pointer_to_data += MAX_I2C_WRITE;
    address += MAX_I2C_WRITE;
  }
  printf("\n\t..........\n");

printf("\nSize of Data: %d byte(s)\n", sizeof (data_t));
/*
	for (int i =0; i< sizeof (data_t) ; i++)
		printf("%04X ", buffer[i]);
	printf("\n");
*/
	


  /** PACK */
  int size;
  char *buf = pack(data1, 1, &size);
  printf("Packed Data for sending: %d byte(s)\n", size);

/** DUMP */
FILE *fptr;
fptr = fopen("test_data.bin","ab");

fwrite(buf, size, 1, fptr); 
fclose(fptr);

  /** UNPACK */
  int num;
  data_t *s = unpack(buf, size, &num);

  /** CHECK */
  assert(num == (int) 1);
  printf ("%s\n", buf);
  for (int ii = 0 ; ii < size; ii++) {
	  printf ("%02X", buf[ii]);
  }
  printf("\n\tData unpacked (received)\n\n");
  printf("Tair\t\t%.2f C\t\t\nTbat\t\t%.3f C\nP\t\t%.3f HPa\nT2\t\t%.2f C\nP2\t\t%.2f HPa\nHumidity\t%02.2f %%\nDepth\t\t%.2f cm\nStatus\t\t%0X\nUptime\t\t%d s\nVbat\t\t%.3f V\nVsolar\t\t%.3f V\nI\t\t%3f mA\nMemory\t\t%d kB\nTime\t\t%d s\nIridium Offset\t%d s\nGPS Offset\t%d s\nLongitude\t%d\nLattitude\t%d\n",
	 s->Tair/100.0,
	 s->Tbat/100.0,
	 s->P/100.0,
	 s->bme_t/100.0,
	 s->bme_p/100.0,
	 s->bme_u/100.0,
	 s->depth/100.0,
	 s->status,
	 s->uptime,
	 s->v_batt/1000.0,
	 s->v_solar/1000.0,
	 s->i_charge/1.0,
	 s->memory,
	 s->rtc_time,                     
	 s->iridium_offset,
	 s->gps_offset,
	 s->gps_y,
	 s->gps_x
	 );
	
	assert(data1->Tair             ==  s->Tair              );
	assert(data1->Tbat             ==  s->Tbat              );
	assert(data1->P                ==  s->P                 );
	assert(data1->bme_t            ==  s->bme_t             );
	assert(data1->bme_p            ==  s->bme_p             );
	assert(data1->bme_u            ==  s->bme_u             );
	assert(data1->depth            ==  s->depth             );
	assert(data1->status           ==  s->status            );
	assert(data1->uptime           ==  s->uptime            );
	assert(data1->v_batt           ==  s->v_batt            );
	assert(data1->v_solar          ==  s->v_solar           );
	assert(data1->i_charge         ==  s->i_charge          );
	assert(data1->memory           ==  s->memory            );
	assert(data1->rtc_time         ==  s->rtc_time          );         
	assert(data1->iridium_offset   ==  s->iridium_offset    );
	assert(data1->gps_offset       ==  s->gps_offset        );
	assert(data1->gps_x            ==  s->gps_x             );
	assert(data1->gps_y            ==  s->gps_y             );

  printf("Data matches ok. Exiting...\n");

  free(buf);
  free(s);

  return 0;
}
