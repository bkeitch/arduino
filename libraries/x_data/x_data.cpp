#ifdef ARDUINO
#include <Arduino.h>
#include <MsgPack.h>
#include <TimeLib.h>
#else
#include <iostream>
#include <msgpack.hpp>
#include <tgmath.h> //log
#include <time.h>
#define analogRead(i) 250
#define digitalRead(i) 1
#endif

#include "x_data.h"
#include "config.h"


int x_data::updateGPSOffset(time_t rtc_time, time_t gps_time) {
	long gps_offset = rtc_time - gps_time;
		
	if ( abs(gps_offset) < MAX_TIME_DIFF ) {
		this->gps_offset = gps_offset;
		return 0;
	} else {
		return -1;
		this->status &= 16;
	}
}
int x_data::updateIridiumOffset(time_t rtc_time, time_t iridium_time)	{
	long iridium_offset = rtc_time - iridium_time;
		
	if ( abs(iridium_offset) < MAX_TIME_DIFF ) {
		this->iridium_offset = iridium_offset;
		return 0;
	} else {
		return -1;
		this->status &= 32;
	}
}

/** 
* read from analogue pin and average over SAMPLE_NUMBER readings
*/
double x_data::readVoltage(int voltagePin) 
{
  double adcTotal = 0;
  
  for (int i = 0; i < SAMPLE_NUMBER; i++) 
  {
    adcTotal += analogRead(voltagePin);
    #ifdef ARDUINO 
    delay(10);
    #endif
  }

  adcTotal /= SAMPLE_NUMBER;

  return (adcTotal / MAX_ADC );   
}

/**
 * Read NTC thermistor and convert to centiCelsius
 * @return temperature in centiCelcius
*/
int32_t x_data::readThermistor(int thermistorPin) 
{
  double rThermistor = 0;
  double tKelvin     = 0; 
  double tCelsius    = 0;
  double adcAverage  = readVoltage(thermistorPin);

  rThermistor = BALANCE_RESISTOR * ( (1 / adcAverage) - 1);

  tKelvin = (BETA * ROOM_TEMP) / 
            (BETA + (ROOM_TEMP * log(rThermistor / RESISTOR_ROOM_TEMP)));

  tCelsius = tKelvin - 273.15;  // convert kelvin to celsius 

  return (int32_t) round(tCelsius*100.0);    // Return the temperature in 1/100 Celsius
}
/**
  * read from analogue pressure sensor MPX5100AP. 
  * @return pressure in deccaPa
*/
uint32_t x_data::readPressure() {
   // read the analogue in value:
  double pressureV = readVoltage (pressurePin);
  
  // convert to nearest Pa as per data sheet
  return (uint32_t) round(100*(pressureV+ 0.095) /0.009);
}

/**
  * Updates state of health about system i.e. battery 
  * charging status etc.
*/
void x_data::updateStatus() {
  //status = 0;
  //status  = digitalRead(ST_1_PIN);
  //status += digitalRead(ST_2_PIN) << 1;
}

/**
 * Return data in JSON format
 * @param message to write data to
*/
void x_data::getJSONData (char *message) {
  sprintf(message, "{\"T_air\":%d,\"T_ground\":%d,\"P\":%u,\"P_trend\":%hd,\"T2\":%d,\"P2\":%u,\"H\":%u,\"depth\":%hu,\"status\":\"%02X\",\"uptime\":%lu,\"Vb\":%u,\"Vs\":%u,\"I\":%u,\"Mem\":%u,\"time\":%lu,\"iri_offset\":%hd,\"gps_offset\":%hd}",
    Tair,                   // int16_t  %d     Tair            in 1/100 degree
    Tground,                // int16_t  %d     Tground         in 1/100 degree
    P,                      // uint16_t %u     P               in 1/10 Pa
    P_trend,                // int8_t   %hd    P_trend         in Pa / minute over 3 hours
    T2,                     // int16_t  %d     T2              in 1/100 degree
    P2,                     // uint16_t %u     P2              in 1/10 Pa
    U,                      // uint16_t %u     U               in 1/100 %
    depth,                  // uint8_t  %hu    depth           in cm
    status,                 // uint16_t %02X   status          
    uptime,                 // uint32_t %lu    uptime          in ms
    v_batt,                 // uint16_t %u     v_batt          in mV
    v_solar,                // uint16_t %u     v_solar         in mV
    i_charge,               // uint16_t %u     i_charge        in mA
    memory,                 // uint16_t %u     memory          in kB
    rtc_time,               // uint32_t %lu    rtc_time        in s since epoch
    iridium_offset,         // int8_t   %hd    iridium_offset  in s
    gps_offset              // int8_t   %hd    gps_offset      in s
   );
}
/**
 * Return data in CSV format
 * 
*/
void x_data::getCSVData (char *csvdata) {
  sprintf(csvdata, "%d ,%d ,%u ,%hd ,%d ,%u ,%u ,%hu ,%02X ,%lu ,%u ,%u ,%u ,%u ,%lu ,%hd ,%hd",
   Tair,
   Tground,
   P,
   P_trend,
   T2,
   P2,
   U,
   depth,
   status,
   uptime,
   v_batt,
   v_solar,
   i_charge,
   memory,
   rtc_time,
   iridium_offset,
   gps_offset
  );
}
/**
 * Uses home made serialiser to store current data 
 * into flash memory (saves power cf writing to SD card)
*/
/*
void x_data::storeToMemory() {

	uint8_t pointer_to_data[4*MAX_I2C_WRITE];
	memset (pointer_to_data, '\0', 4*MAX_I2C_WRITE);
	memcpy (pointer_to_data, this, sizeof(this));
	// uint8_t* pointer_to_data = (uint8_t*) this[0];

	for (int i = 0; i < 4 * MAX_I2C_WRITE; i++) {
		debugprint(pointer_to_data[i], HEX);
		debugprint(" ");
		#ifdef EEPROM 
		writeEEPROM(i, pointer_to_data[i]);
		#endif
	} 
	for (int i = 0; i < 16; i++) {
		#ifdef EEPROM 
		debugprint(writeEEPROMPage(address, pointer_to_data, i % 4));
		#endif
		address += MAX_I2C_WRITE;
		debugprint(address);
	} 

}
*/

#ifndef ARDUINO
#define millis() int(time(NULL))
#endif
/**
 * measure pressure, humidity temperature 
 * and system voltages and store these 
 * along state of health information in object
*/
void x_data::update(float T2, float P2, float U, int d, time_t t, unsigned int memory_count) {
  this->T2 = (int16_t) round(T2 *100.0);
  this->P2 = (uint32_t) round(P2/10.0);
  this->U = (uint16_t) round(U *100.0);
  Tair = readThermistor(externalTPin);
  Tground = readThermistor(batteryTPin);
  uint16_t old_P = this->P;
  uint32_t old_t = this->rtc_time;
  
  P = readPressure();
  
  updateStatus();
  if (d < 0) {
		this->depth = 0;
    this->status += (d *-16);
  } else {
		this->depth = d;
	}
  uptime = millis();
  v_batt = round(readVoltage(vBatteryPin)*VREF);
  v_solar = round(readVoltage(vSolarPin)*VREF);
  i_charge = round(readVoltage(iBatteryPin)*IREF);
  memory = memory_count;
  
  rtc_time  = t;
//Pressure change over last three hours in millibars and tenths (int8_t)
  if(t - old_t > 3600) { //only valid if one hour at least went by
	  float pt = (P - old_P)*3*3600 / (t - old_t) ;
	  if(pt > -100 && pt < 100) {
		  this->P_trend = (int8_t) pt;
	  } else {
		  this->P_trend = 99;
	  } 
  } 
	
}
