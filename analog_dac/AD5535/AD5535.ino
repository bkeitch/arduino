#include <SPI.h>
#include <string.h>

#define firmwareVerNum 2.0  //BCK 2012.07.09
#define MAX_CHANNEL 32 //BCK 2012.07.09

/* AD5535 -- An Arduino based firmware for Analog Devices AD5535 DACs.
 * The Arduino listens for text based commands over the USB-serial bus and reads/writes appropriate commands to/from the SPI bus to control the DAC.
 * Commands should be terminated with a single <LF> character. Any excess whitespace is ignored.
 *
 * Commands are:
 * Write Voltage -- "V <Channel> <Volts><LF>" ("Channel" is an unsigned int, "Volts" is a double. DAC value will not update until UPDATE command sent)
 * Write Voltage and Update -- "VU <Channel> <Volts><LF>
 * Query Voltage -- "V? <Channel><LF>" (returns the voltage set on channel in the form "V <Voltage><LF>")
 *
 * Set gain -- "M <Channel> <gain><LF> ("gain" is an unsigned short specifying a value for the gain register)
 * Set gain and update "MU <Channel><LF>
 * Query gain -- "M? <Channel><LF> (returns "M <gain><LF>")
 *
 * Set offset -- "C <Channel> <offset><LF> ("offset" is an unsigned short specifying a value for the offset register)
 * Set offset and Update -- "CU <Channel> <offset><LF>"
 * Query offset -- "C? <Channel><LF>" (returns "C <offset><LF>")
 *
 * Set offset DAC -- "O <DAC> <offset><LF>" (set offset DAC (DAC must be 0 or 1))
 * Set offset DAC and update -- "OU <DAC> <offset><LF>" 
 * Query offset DAC -- "O? <DAC><LF>" (returns "O <offset><LF>")
 *
 * Update DAC -- "UPDATE<LF>" (Pulse LDAC line to update DAC value)
 * Identity -- "*IDN?<LF>" -- Prints a version string to the USB-serial bus
 *
 * N.B. Query commands return the value in the DAC register, not the value set on the DAC. Thus, if a value is waiting to be updated then this value will be returned by the query, not the current DAC value...
 * N.B. Channel is 0->31
 *
 * This firmware aims to support all the functions of the DAC that we are likely to want. However, it's worth noting a few DAC functions which are currently unsupported:
 * Grouping -- the DAC supports writing to groups of channels (either to all channels in a group or the same channel in each group)
 * The DAC has a reset function which performs a power on reset.
 * The DAC has a clear function, which shorts the outupts to signal ground. N.B. the datasheet recommends using this to avoid glitches when changing the offset so we should probaby be careful about this...
 * The DAC has two registers which you can write data to, this is useful if you want to switch between two states.
 * The DAC can be programmed to have a thermal shutdown.
 *
 * Implementation -- The DAC uses the following settings:
 * Bit ordering: big-endian (MSB first)
 * Clock phase: Data read on falling edge, propagated on rising edge (CPHA=1)
 * Clock polarity: Clock low when idle (CPOL=0)
 * Read/write frame while SS LOW
 * Clock speed: 125kHz (kept low because of reflections from poor wiring...)
 * To get a symmetric voltage range, we set the offset DACs to 8192, so that when the voltage register is at MAX_CHANNEL768 (half way through its range) we get 0V.
 *
 * USB-serial baud rate is 115200
 */



// Pins used on Arduino Mega
//#define SCK  52
//#define MISO 50
//#define MOSI 51 
//#define SS   53                           // Defines the DAC read-frame
//#define LDAC 39                           // Used to update the DAC (DACs updated when LDAC is taken low)
// Pins used on Arduino Duemilanove/UNO
#define MOSI 11                          // pin 1 on eval board. Blue/White cable
#define MISO 12                          // NC
#define SCK  13                          // pin 3 on eval board. White/Blue cable
#define SS   10                          // pin 5 on eval board. Orange/White cable. Defines the DAC read-frame (SYNC)
// GROUND : pin 7 on eval board Blue/White cable
#define LDAC 9                           // pin 8 on eval board White/Orange. Used to update the DAC (DACs updated when LDAC is taken low)

// DAC commands
//#define AD5372_DAC_WRITE               0xC0
//#define AD5372_OFFSET_WRITE            0x80
//#define AD5372_GAIN_WRITE              0x40
//#define AD5372_OFFSETDAC0_WRITE        0x2
//#define AD5372_OFFSETDAC1_WRITE        0x3
//#define AD5372_READOUT                 0x5
//#define AD5372_READOUT_GAIN_MASK       0x6000
//#define AD5372_READOUT_VOLTAGE_MASK    0x0    // By default the DACs use the A register, so this is the one we query
//#define AD5372_READOUT_OFFSET_MASK     0x4000
//#define AD5372_READOUT_OFFSETDAC0_MASK 0x8100
//#define AD5372_READOUT_OFFSETDAC1_MASK 0x8180


// DAC commands
#define AD5371_DAC_WRITE               0xC0
#define AD5371_OFFSET_WRITE            0x80
#define AD5371_GAIN_WRITE              0x40
#define AD5371_OFFSETDAC0_WRITE        0x02
#define AD5371_OFFSETDAC1_WRITE        0x03
#define AD5371_OFFSETDAC2_WRITE        0x04
#define AD5371_READOUT                 0x05
#define AD5371_READOUT_GAIN_MASK       0x6000
#define AD5371_READOUT_VOLTAGE_MASK    0x0000    // By default the DACs use the A register, so this is the one we query
#define AD5371_READOUT_OFFSET_MASK     0x4000
#define AD5371_READOUT_OFFSETDAC0_MASK 0x8100
#define AD5371_READOUT_OFFSETDAC1_MASK 0x8180
#define AD5371_READOUT_OFFSETDAC2_MASK 0x8200
#define AD5371_FULL_RANGE 0x4000
#define DIVIDER 4 // FOR AD5372 THIS IS 4
// DAC settings
#define vRef 5
//#define AD5372_OFFSET_CODE  8192            // Default loaded into DAC on power-up (used to convert DAC codes <=> voltages)


#define AD5371_OFFSET_CODE 8192           // used to convert DAC codes <=> voltages AND max for 5V - see datasheet page 17

// Buffering settings (don't use too large a buffer or you'll run out of RAM!)
#define maxNumChars 254                     // Size of buffer to use when reading commands from USB serial bus (don't make too large or we run out of RAM!)
#define readTimeout 100                    // Timeout (ms) to use for reading from serial port in ms N.B. this is the maximum time to wait between chars (not a limit on the total read time)

unsigned long lastReadTime;                // Used to check for timeout when reading commands from USB serial
unsigned int charsRead;
char readBuffer [maxNumChars + 1];         // Used to read commands into (N.B. room for NULL terminator)
char tempBuffer [maxNumChars + 1];
const char terminators []  = " \t\f\v\r\n";

// helper function used to check how much RAM we have left
// REMEMBER TO CHECK HOW MUCH RAM YOU'RE USING -- PARTICULARLY IF YOU HAVE LOTS OF STATIC TEXT STRINGS...
//int availableMemory()
//{
//  int size = 8192;
//  byte *buf;
//  while ((buf = (byte *) malloc(--size)) == NULL);
//  free(buf);
//  return size;
//}
void error(int why, char *where) {
  if(0==why) return;
  char *reasons[] = {
    "","channel out of range", "no set parameter specified", 
    "unrecognised argument", "unexpected garbage at EOF", "parameter is outside DAC range", "read timeout", "clock overflow during read", 
    "buffer overflow during read", "no command found", "unrecognised command" , "unrecognised channel"  };
  String error = String("Error : ");
  error += where;
  error += " : ";
  error += reasons[why];
  error += " \n";
  //error += what;
  char buffer[100];
  error.toCharArray(buffer,100);
  Serial.write(buffer); 
}
void setup()                              // Code run at board power-up
{
  Serial.begin(115200);                   // Open USB-serial port to communicate with computer

  SPI.begin();                            // This sets SCK, MOSI, and SS to outputs and pulls SCK and MOSI low and SS high. 
  SPI.setDataMode(SPI_MODE1);             // (CPOL=0, CPHA=1)
  SPI.setBitOrder(MSBFIRST);              // Set bit order to MSB first
  SPI.setClockDivider(SPI_CLOCK_DIV128);  // Nice slow clock (Arduino Uno runs at 16MHz, so this is 125kHz)
  pinMode(MISO, INPUT);
  pinMode(LDAC, OUTPUT);                  // Make the LDAC pin an output
  digitalWrite(LDAC, HIGH);               // Set LDAC high (i.e. don't update DAC until we say so)

  charsRead = 0;                          // Used to keep track of where we are in our read buffer
  initOffset();                // Write default code to offset DAC

  /*
  Serial.print("free memory = ");        // Used to check we haven't filled up our RAM...
   Serial.print(availableMemory());
   Serial.print(" - memory used = ");
   Serial.println(8192-availableMemory());
   */

}

void loop()                                                // Program main loop
{
  char* here = "main loop";
  if(!Serial.available())                                  // No data in serial buffer, so check for timeout and return
  {
    if(!charsRead)                                         // No chars in buffer so nothing to do
      return;

    if( (millis()-lastReadTime) > readTimeout )            // We've reached a timeout before getting a full line of data, so discard current buffered data and begin read again
    {
      error(6, here);
      charsRead = 0;
      return;
    }

    if(millis() < lastReadTime)                          // This clock overflows once every ~50 days! (not a very likely error)
    {
      error(7, here);
      charsRead = 0;
      return;
    }
    return;
  }

  lastReadTime = millis();                              // Update timeout
  char inByte = Serial.read(); 

  if('\n' != inByte)                                    // We've read a character so check for buffer overruns and save the char
  {
    if(charsRead == maxNumChars)
    {
      error(8, here);

      charsRead = 0;
      return;
    }
    readBuffer[charsRead++] = inByte;
    return;
  }

  readBuffer[charsRead] = '\0';                          // Full command read, so null terminate string and process
  charsRead = 0;

  char *pos = strtok(readBuffer, terminators);
  if('\0' == pos)
  {
    error(9, here);

    return;
  }

  if(0 == strcmp(pos, "V"))
  {
    setVoltage();
    return;

  }

  if(0 == strcmp(pos, "VU"))
  {
    setVoltage();
    pulseLDAC();
    return;    
  }

  else if(0 == strcmp(pos, "V?"))
  {
    getVoltage();
    return;
  }

  else if (0 == strcmp(pos, "C"))
  {
    setOffset();
    return;
  }


  else if (0 == strcmp(pos, "CU"))
  {
    setOffset();
    pulseLDAC();
    return;
  }
  else if (0 == strcmp(pos, "C?"))
  {
    getOffset();
    return;
  }

  else if (0 == strcmp(pos, "M"))
  {
    setGain();
    return;
  }

  else if (0 == strcmp(pos, "MU"))
  {
    setGain();
    pulseLDAC();
    return;
  }

  else if (0 == strcmp(pos, "M?"))
  {
    getGain();
    return;
  }

  else if (0 == strcmp(pos, "O"))
  {
    setOffsetDAC();
    return;
  }

  else if (0 == strcmp(pos, "OU"))
  {
    setOffsetDAC();
    pulseLDAC();
    return;
  }

  else if (0 == strcmp(pos, "O?"))
  {
    getOffsetDAC();
    return;
  }

  else if(0 == strcmp(pos, "UPDATE"))
  {
    pulseLDAC();
    return;
  }

  else if(0 == strcmp(pos, "*IDN?"))
  {
    identity();
    return;
  }

  error(10, here);
  return;
}
/*
* parses our command and returns 0 if no error, or an error code otherwise
 ** BCK
 */
int parseCommand(unsigned int *channel, double *parameter) {
  char *pos = strtok(NULL, terminators);
  if ('\0' == pos)
  {
    return(10);
  }
  if(1 != sscanf(pos, "%u%s", channel, tempBuffer))    // Extra argument checks for remaining characters (e.g. will give an error if we pass "2a" as a voltage)
  {
    return(11);
  }
  if(MAX_CHANNEL <= *channel)                                    // N.B. First DAC is 0
  {
    return(1);
  }

  if (parameter != 0) {

    pos = strtok(NULL, terminators);
    if('\0' == pos)
    {
      return (2);
    }
    char *endOfDouble;
    *parameter = strtod(pos, &endOfDouble);
    if(NULL == endOfDouble || (strlen(pos) != (sizeof(char)*(endOfDouble - pos))) )    // N.B. arduino sscanf implementation does not support floats!
    {
      return(3);
    }

    pos = strtok(NULL, terminators);
    if('\0' != pos)
    {   
      return(4);
    }
  }
  return(0);
}
void setVoltage()  
{
  unsigned int channel;
  double voltage;
  int errorCode = parseCommand(&channel, &voltage);
  if(errorCode !=0) {
    error(errorCode, "set voltage");
    return;
  }

  byte group = (channel/8);                            // DAC divides channels into groups
  byte channelInGroup = (channel%8);                   // Calculate address of channel based on its group
  byte channelMask = ((group+1) << 3) | channelInGroup;
  byte mask = channelMask | AD5371_DAC_WRITE;

  double dacSetting = (double) (voltage /(vRef * DIVIDER) * AD5371_FULL_RANGE + AD5371_OFFSET_CODE);

  if(16384 < dacSetting || 0 > dacSetting) //2^14
  {
    error(5, "set voltage");

    return;
  }

  unsigned short INPUT_CODE = (unsigned short)dacSetting;
  unsigned short SHIFTED_CODE = INPUT_CODE << 2;
  #ifdef DEBUG 
  Serial.print(mask, HEX);
  Serial.println(SHIFTED_CODE, HEX); 
  #endif 
  digitalWrite(SS, LOW);
  SPI.transfer(mask);
  SPI.transfer(((byte*)&SHIFTED_CODE)[1]);  // MSB first
  SPI.transfer(((byte*)&SHIFTED_CODE)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame
}

void getVoltage()  
{
  unsigned int channel;
  int errorCode = parseCommand(&channel, (double*) 0);
  if(errorCode !=0) {
    error(errorCode, "get voltage");
    return;
  }

  unsigned short channelMask = (8 +  channel) << 7;
  unsigned short command = channelMask | AD5371_READOUT_VOLTAGE_MASK;     

  digitalWrite(SS, LOW);
  SPI.transfer((byte)AD5371_READOUT);
  SPI.transfer(((byte*)&command)[1]);
  SPI.transfer(((byte*)&command)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  delayMicroseconds(10);

  digitalWrite(SS, LOW);
  byte bytes[3];
  bytes[0] = SPI.transfer(0);
  bytes[2] = SPI.transfer(0);
  bytes[1] = SPI.transfer(0);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  unsigned short DAC_CODE = *((unsigned short*)&bytes[1]);                    // Ignore first byte and read second two as an unsigned short
  double voltage = (4*vRef) * (((double)DAC_CODE - AD5371_OFFSET_CODE)/AD5371_FULL_RANGE);   // Convert into a Voltage
  Serial.write("V ");
  Serial.print(voltage,5);
  Serial.write("\n");

}

void setOffset()  
{
  unsigned int channel;
  double offsetD;
  int errorCode = parseCommand(&channel, &offsetD);
  if(errorCode !=0) {
    error(errorCode, "set offset");
    return;
  }
  unsigned short offset = (unsigned short) offsetD;
  byte group = (channel/8);                            // DAC divides channels into groups
  byte channelInGroup = (channel%8);                   // Calculate address of channel based on its group
  byte channelMask = ((group+1) << 3) | channelInGroup;
  byte mask = channelMask | AD5371_OFFSET_WRITE;
  #ifdef DEBUG
  Serial.print(mask, HEX);
  Serial.println(offset, HEX); 
  #endif 

  digitalWrite(SS, LOW);
  SPI.transfer(mask);
  SPI.transfer(((byte*)&offset)[1]);
  SPI.transfer(((byte*)&offset)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame
}

void getOffset()  
{
  unsigned int channel;
  int errorCode = parseCommand(&channel, (double*) 0);
  if(errorCode !=0) {
    error(errorCode, "get offset");
    return;
  }

  unsigned short channelMask = (8 +  channel) << 7;
  unsigned short command = channelMask | AD5371_READOUT_OFFSET_MASK;     

  digitalWrite(SS, LOW);
  SPI.transfer((byte)AD5371_READOUT);
  SPI.transfer(((byte*)&command)[1]);
  SPI.transfer(((byte*)&command)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  delayMicroseconds(10);

  digitalWrite(SS, LOW);
  byte bytes[3];
  bytes[0] = SPI.transfer(0);
  bytes[2] = SPI.transfer(0);
  bytes[1] = SPI.transfer(0);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  unsigned short offset = *((unsigned short*)&bytes[1]);                    // Ignore first byte and read second two as an unsigned short
  sprintf(tempBuffer,"C %hu\n", offset);
  Serial.write(tempBuffer);
}

void setGain()  
{
  unsigned int channel;
  double gaind;
  int errorCode = parseCommand(&channel, &gaind);
  if(errorCode !=0) {
    error(errorCode, "set gain");
    return;
  }
  unsigned short gain = (unsigned short) gaind;

  byte group = (channel/8);                            // DAC divides channels into groups
  byte channelInGroup = (channel%8);                   // Calculate address of channel based on its group
  byte channelMask = ((group+1) << 3) | channelInGroup;
  byte mask = channelMask | AD5371_GAIN_WRITE;

  #ifdef DEBUG
  Serial.print(mask, HEX);
  Serial.println(gain, HEX); 
  #endif 

  digitalWrite(SS, LOW);
  SPI.transfer(mask);
  SPI.transfer(((byte*)&gain)[1]);
  SPI.transfer(((byte*)&gain)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame
}

void getGain()  
{
  unsigned int channel;
  int errorCode = parseCommand(&channel, (double*) 0);
  if(errorCode !=0) {
    error(errorCode, "get gain");
    return;
  }

  unsigned short channelMask = (8 +  channel) << 7;
  unsigned short command = channelMask | AD5371_READOUT_GAIN_MASK;     

  digitalWrite(SS, LOW);
  SPI.transfer((byte)AD5371_READOUT);
  SPI.transfer(((byte*)&command)[1]);
  SPI.transfer(((byte*)&command)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  delayMicroseconds(10);

  digitalWrite(SS, LOW);
  byte bytes[3];
  bytes[0] = SPI.transfer(0);
  bytes[2] = SPI.transfer(0);
  bytes[1] = SPI.transfer(0);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  unsigned short gain = *((unsigned short*)&bytes[1]);                    // Ignore first byte and read second two as an unsigned short
  sprintf(tempBuffer,"M %hu\n", gain);
  Serial.write(tempBuffer);
}

void setOffsetDAC()  
{
  unsigned int DACNumber;
  double offsetd;
  int errorCode = parseCommand(&DACNumber, &offsetd);
  if(errorCode !=0) {
    error(errorCode, "set offset DAC");
    return;
  }
  unsigned short offset = (unsigned short) offsetd;

  digitalWrite(SS, LOW);
  if(0 == DACNumber)
    SPI.transfer((byte)AD5371_OFFSETDAC0_WRITE);
  else if(1 == DACNumber)
    SPI.transfer((byte)AD5371_OFFSETDAC1_WRITE);
  else if(2 == DACNumber)
    SPI.transfer((byte)AD5371_OFFSETDAC2_WRITE);
  #ifdef DEBUG
  Serial.println(offset, HEX); 
  #endif

  SPI.transfer(((byte*)&offset)[1]);
  SPI.transfer(((byte*)&offset)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  delayMicroseconds(10);
}

void getOffsetDAC()  
{

  unsigned int DACNumber;
  int errorCode = parseCommand(&DACNumber, (double*) 0);
  if(errorCode !=0) {
    error(errorCode, "get offset DAC");
    return;
  }

  unsigned short command;

  if (0 == DACNumber)
    command = AD5371_READOUT_OFFSETDAC0_MASK;
  else if(1 == DACNumber)
    command = AD5371_READOUT_OFFSETDAC1_MASK;
  else if(2 == DACNumber)
    command = AD5371_READOUT_OFFSETDAC2_MASK;

  digitalWrite(SS, LOW);
  SPI.transfer((byte)AD5371_READOUT);
  SPI.transfer(((byte*)&command)[1]);
  SPI.transfer(((byte*)&command)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  delayMicroseconds(10);

  digitalWrite(SS, LOW);
  byte bytes[3];
  bytes[0] = SPI.transfer(0);
  bytes[2] = SPI.transfer(0);
  bytes[1] = SPI.transfer(0);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame

  unsigned short gain = *((unsigned short*)&bytes[1]);                    // Ignore first byte and read second two as an unsigned short
  sprintf(tempBuffer,"O %hu\n", gain);
  Serial.write(tempBuffer);
}

void pulseLDAC()
{
  char *pos = strtok(NULL, terminators);
  if('\0' != pos)
  {
    error(4, "pulse ldac");
    return;
  }

  digitalWrite(LDAC, LOW);                   // Pulse the LDAC channel to update the DAC
  delayMicroseconds(10);
  digitalWrite(LDAC, HIGH);
}

void identity()
{
  char *pos = strtok(NULL, terminators);
  if('\0' != pos)
  {
    error(4, "pulse identity");
    return;
  }

  Serial.write("Arduino powered AD5MAX_CHANNEL7. Firmware version V");
  Serial.println(firmwareVerNum, 2);
}

void initOffset()
{
  unsigned short offset = AD5371_OFFSET_CODE;
  digitalWrite(SS, LOW);
  SPI.transfer((byte)AD5371_OFFSETDAC0_WRITE);
  SPI.transfer(((byte*)&offset)[1]);
  SPI.transfer(((byte*)&offset)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame
  delayMicroseconds(10);

  digitalWrite(SS, LOW);
  SPI.transfer((byte)AD5371_OFFSETDAC1_WRITE);
  SPI.transfer(((byte*)&offset)[1]);
  SPI.transfer(((byte*)&offset)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame
  delayMicroseconds(10);

  digitalWrite(SS, LOW);
  SPI.transfer((byte)AD5371_OFFSETDAC2_WRITE);
  SPI.transfer(((byte*)&offset)[1]);
  SPI.transfer(((byte*)&offset)[0]);
  delayMicroseconds(10);
  digitalWrite(SS, HIGH);     // End write frame
  delayMicroseconds(10);
}

