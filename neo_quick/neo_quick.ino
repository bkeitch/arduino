#include <string.h>
#include <Arduino.h>
#include <SPI.h>
#include <Adafruit_NeoPixel.h>
#if SOFTWARE_SERIAL_AVAILABLE
  #include <SoftwareSerial.h>
#endif
#define FACTORYRESET_ENABLE     0
#define NEOPIXEL_VERSION_STRING "Neopixel v2.0"
#define PIN                     6   /* Pin used to drive the NeoPixels */

#define MAXCOMPONENTS  4

//Adafruit_NeoPixel neopixel(50, 6, NEO_GRB + NEO_KHZ400);
  int red  ;
    int green ;
    int blue;
Adafruit_NeoPixel neopixel = Adafruit_NeoPixel(40, PIN, NEO_RGB + NEO_KHZ800);
void serial_printf(const char * format, ...) {
  char buffer [48];
  va_list args;
  va_start(args, format);
  vsnprintf(buffer, sizeof(buffer), format, args);
  va_end(args);
  Serial.print(buffer);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Adafruit Bluefruit Neopixel Test");
  Serial.println("--------------------------------");

  Serial.println();
  Serial.println("Please connect using the Bluefruit Connect LE application");

  // Config Neopixels
  neopixel.begin();
  neopixel.clear();

  
}

void loop() {
  // put your main code here, to run repeatedly:
  uint32_t color;

  if (Serial.available() > 0)
  {
    red = Serial.parseInt();
    green = Serial.parseInt();
    blue = Serial.parseInt();
    if ((red !=0) || (blue |=0) || (green !=0) ) {
      color = neopixel.Color( red,green,blue );
      serial_printf("\tcolor (%d, %d, %d, %d)\n", red, green, blue);    
      for (int i=0; i<2; i++) {
        neopixel.setPixelColor(i, color);
        neopixel.show();
      }
    }
  }
}
