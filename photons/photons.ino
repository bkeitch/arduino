int time_h[] = {44, 14, 17, 11, 34, 43, 29, 42, 10, 44, 20, 46, 16, 5, 25, 
10, 11, 9, 6, 14, 4, 25, 19, 45, 8, 44, 23, 2, 46, 6, 24, 16, 31, 42, 11, 
12, 45, 26, 39, 8, 8, 12, 31, 46, 18, 48, 19, 45, 39, 21, 34, 44, 32, 0, 
41, 32, 19, 31, 34, 6, 45, 7, 35, 0, 23, 26, 33, 44, 12, 32, 0, 32, 20, 
38, 17, 12, 22, 14, 36, 3, 21, 10, 33, 5, 6, 1, 3, 23, 7, 21, 45, 45, 37, 17, 1, 33, 9, 23, 36, 18};
int time_l[] = {4, 41, 2, 14, 42, 13, 44, 22, 18, 34, 13, 43, 1, 46, 12, 
42, 14, 8, 8, 42, 11, 30, 40, 14, 40, 15, 33, 39, 38, 39, 10, 19, 7, 41, 
11, 11, 4, 49, 38, 9, 40, 21, 25, 48, 37, 1, 30, 27, 41, 15, 27, 30, 12, 
4, 35, 12, 43, 0, 34, 25, 32, 46, 43, 17, 15, 11, 9, 29, 36, 15, 17, 30, 
14, 30, 24, 33, 29, 15, 40, 12, 36, 8, 26, 45, 16, 35, 44, 7, 31, 35, 21, 
21, 34, 45, 27, 11, 26, 21, 12, 41};
int photon = 13;
int i;
void setup(){
  //Serial.begin(9600);
  pinMode(photon, OUTPUT);

  // if analog input pin 0 is unconnected, random analog
  // noise will cause the call to randomSeed() to generate
  // different seed numbers each time the sketch runs.
  // randomSeed() will then shuffle the random function.
  randomSeed(analogRead(0));
  i=0;
}

void loop() {
  if(i>100) {
    i=0;
  }

  digitalWrite(photon, HIGH);
  delayMicroseconds(time_h[i]);
  digitalWrite(photon, LOW);
  delayMicroseconds(time_l[i]);
  ++i;
}
