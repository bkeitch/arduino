/*
BME280 I2C Test.ino

This code shows how to record data from the BME280 environmental sensor
using I2C interface. This file is an example file, part of the Arduino
BME280 library.

GNU General Public License

Written: Dec 30 2015.
Last Updated: Oct 07 2017.

Connecting the BME280 Sensor:
Sensor              ->  Board
-----------------------------
Vin (Voltage In)    ->  3.3V
Gnd (Ground)        ->  Gnd
SDA (Serial Data)   ->  A4 on Uno/Pro-Mini, 20 on Mega2560/Due, 2 Leonardo/Pro-Micro
SCK (Serial Clock)  ->  A5 on Uno/Pro-Mini, 21 on Mega2560/Due, 3 Leonardo/Pro-Micro

 */

#include <BME280I2C.h> //https://github.com/finitespace/BME280.git
#include <Wire.h>
#include <U8g2lib.h>




/*
  U8g2lib Example Overview:
    Frame Buffer Examples: clearBuffer/sendBuffer. Fast, but may not work with all Arduino boards because of RAM consumption
    Page Buffer Examples: firstPage/nextPage. Less RAM usage, should work with all Arduino boards.
    U8x8 Text Only Example: No RAM usage, direct communication with display controller. No graphics, 8x8 Text only.
    
*/

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif



//U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, /* E(SCLK) =*/ 25, /* RW(SID) data=*/ 23, /* RS(CS)=*/ 32, /* reset=*/ 33); // ESP32
//U8G2_ST7920_128X64_F_HW_SPI u8g2(U8G2_R0, /* CS=*/ 15, /* reset=*/ 16); // Feather HUZZAH ESP8266, E=clock=14, RW=data=13, RS=CS
U8G2_ST7920_128X64_F_SW_SPI u8g2(U8G2_R0, 14, 12, 15, 13);

#define SERIAL_BAUD 115200

BME280I2C bme;    // Default : forced mode, standby time = 1000 ms
                  // Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,

//////////////////////////////////////////////////////////////////
void setup()
{
  Serial.begin(SERIAL_BAUD);

  while(!Serial) {} // Wait
  u8g2.begin();
  u8g2.setFont(u8g2_font_6x10_tf);
  u8g2.setFontRefHeightExtendedText();
  u8g2.setDrawColor(1);
  u8g2.setFontPosTop();
  u8g2.setFontDirection(0);
  Wire.begin();

  //while(!bme.begin())
  {
    Serial.println("Could not find BME280 sensor!");
    delay(1000);
  }

  switch(bme.chipModel())
  {
     case BME280::ChipModel_BME280:
       Serial.println("Found BME280 sensor! Success.");
       break;
     case BME280::ChipModel_BMP280:
       Serial.println("Found BMP280 sensor! No Humidity available.");
       break;
     default:
       Serial.println("Found UNKNOWN sensor! Error!");
  }
  pinMode(33, OUTPUT);
  digitalWrite(33, LOW);
  
}

//////////////////////////////////////////////////////////////////
void loop()
{
   printBME280Data(&Serial);
   delay(500);
}

//////////////////////////////////////////////////////////////////
void printBME280Data
(
   Stream* client
)
{
   float temp(NAN), hum(NAN), pres(NAN);

   BME280::TempUnit tempUnit(BME280::TempUnit_Celsius);
   BME280::PresUnit presUnit(BME280::PresUnit_mbar);
   u8g2.clearBuffer();
  
   bme.read(pres, temp, hum, tempUnit, presUnit);
   char buff[10] = "hello";
   u8g2.drawStr( 0, 0, "Temp: ");
   client->print("Temp: ");
   snprintf (buff, sizeof(buff), "%04.02f", temp);
   u8g2.drawStr(28, 10, buff);
   client->print(buff);
   client->print("°"+ String(tempUnit == BME280::TempUnit_Celsius ? 'C' :'F'));
   u8g2.drawUTF8(77, 10, "°");
   u8g2.drawStr(84, 10, "C");
   
   client->print("\t\tHumidity: ");
   u8g2.drawStr(0, 20, "Humidity: ");
   snprintf (buff, sizeof(buff), "%04.02f", hum);
   client->print(buff);
   u8g2.drawStr(28, 30, buff);
   client->print("% RH");
   u8g2.drawStr(77, 30, "% RH");
   
   client->print("\t\tPressure: ");
   u8g2.drawStr(0, 40, "Pressure: ");
   snprintf (buff, sizeof(buff), "%06.02f", pres);
   client->print(buff);
   u8g2.drawStr(14, 50, buff);
   client->println("Pa");
   u8g2.drawStr(77, 50, "Pa");
   u8g2.sendBuffer();
   delay(1000);
}
